﻿namespace Dureforce.BlogSystem.PostService.Core.Shared.Enums
{/// <summary>
/// Cache keys for redis cache
/// </summary>
    public enum CacheKeys
    { 
        /// <summary>
        /// All  keys related blog post 
        /// </summary>
        Post,
        /// <summary>
        /// all keys related to tags 
        /// </summary>
        Tag,
        /// <summary>
        /// all keys related to categories
        /// </summary>
        Category
    }

}
