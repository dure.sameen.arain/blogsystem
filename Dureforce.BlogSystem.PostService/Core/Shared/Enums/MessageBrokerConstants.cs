﻿namespace Dureforce.BlogSystem.PostService.Core.Shared.Enums
{/// <summary>
/// constants
/// </summary>
    public class MessageBrokerConstants
    {/// <summary>
    /// a topic to send request of comments
    /// </summary>
        public const string RequestCommentsQueue= "post-request-comments-queue";
        /// <summary>
        ///  a topic to received response of comments
        /// </summary>
        public const string ResponseCommentsTopic = "ct";
        /// <summary>
        /// subscription
        /// </summary>
        public const string ResponseCommentsTopicSubscription = "s";
    }
}
 
