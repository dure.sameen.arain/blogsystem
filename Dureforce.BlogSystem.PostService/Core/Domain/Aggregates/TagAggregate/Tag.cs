﻿using Dureforce.Cqrs.Shared.Domain;
using Dureforce.Database.Rdbms.SqlServer.Model;

namespace Dureforce.BlogSystem.PostService.Core.Domain.Aggregates.TagAggregate
{
    [DataTable]
    public class Tag : BaseEntity<Guid>
    {
        public required string  Name { get; set; }

        public static Tag Create(
              string name,
              string userCLaimsId)
        {
            Tag tag = new()
            {
                Id = Guid.NewGuid(),
                Name = name, 
                CreatedBy = userCLaimsId,
                CreatedOn = DateTime.Now
            };

            return tag;
        }


        public void Update(
             string name,
             string userCLaimsId )
        {
            Name = name; 
            ModifiedBy = userCLaimsId;
            ModifiedOn = DateTime.Now;
        }


    }
}
