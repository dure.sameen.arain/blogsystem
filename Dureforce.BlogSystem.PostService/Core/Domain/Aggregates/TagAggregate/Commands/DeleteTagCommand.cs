﻿using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Command;
using Dureforce.Cqrs.Shared.Domain;

namespace Dureforce.BlogSystem.PostService.Core.Domain.Aggregates.TagAggregate.Commands
{
    public class DeleteTagCommand : BaseCommand, ICommand<CommandExecutionResult>
    {
        public required Guid Id { get; set; }
    }
}
