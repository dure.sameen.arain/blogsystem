﻿using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Domain;
  
namespace Dureforce.BlogSystem.PostService.Core.Domain.Aggregates.TagAggregate.Commands
{
    public class ChangeTagCommand : CreateTagCommand, ICommand<CommandExecutionResult>
    { 
        public required Guid Id { get; set; }  
        
    }
     
}
