﻿using Dureforce.Cqrs.Shared.Domain;
using Dureforce.Database.Rdbms.SqlServer.Model;
 

namespace Dureforce.BlogSystem.PostService.Core.Domain.Aggregates.CategoryAggregate
{
    [DataTable]
    public class Category:BaseEntity<Guid>
    {
        public required string Name { get; set; }

        public static Category Create(
              string name, 
              string userCLaimsId)
        {
            Category category = new()
            {
                Id = Guid.NewGuid(),
                Name= name, 
                CreatedBy = userCLaimsId,
                CreatedOn = DateTime.Now
            };

            return category;
        }


        public void Update(
             string name,
             string userCLaimsId  
             )
        {
            Name = name; 
            ModifiedBy = userCLaimsId;
            ModifiedOn = DateTime.Now;
        }

      
    }
}
