﻿using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Command;
using Dureforce.Cqrs.Shared.Domain;

namespace Dureforce.BlogSystem.PostService.Core.Domain.Aggregates.CategoryAggregate.Commands
{
    public class CreateCategoryCommand : BaseCommand, ICommand<CommandExecutionResult> 
    { 
        public required string Name { get; set; }
    }
}
