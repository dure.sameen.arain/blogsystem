﻿using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Domain;
  
namespace Dureforce.BlogSystem.PostService.Core.Domain.Aggregates.CategoryAggregate.Commands
{
    public class ChangeCategoryCommand : CreateCategoryCommand, ICommand<CommandExecutionResult>
    { 
        public required Guid Id { get; set; }  
         
    }
     
}
