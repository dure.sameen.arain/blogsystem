﻿using Dureforce.Cqrs.Shared.Domain;
using Dureforce.Database.Rdbms.SqlServer.Model;

namespace Dureforce.BlogSystem.PostService.Core.Domain.Aggregates.TagAggregate
{
    [DataTable]
    public class PostTag : BaseEntity<Guid>
    {
        public required  Guid  PostId { get; set; }
        public required  Guid  TagId { get; set; }


        public static PostTag Create(
              Guid postId,
              Guid tagId,
              string userCLaimsId)
        {
            PostTag postTag = new()
            {
                Id = Guid.NewGuid(),
                PostId = postId ,
                TagId= tagId , 
                CreatedBy = userCLaimsId,
                CreatedOn = DateTime.Now
            };

            return postTag;
        }


         


    }
}
