﻿using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Domain;
  
namespace Dureforce.BlogSystem.PostService.Core.Domain.Aggregates.PostAggregate.Commands
{
    public class ChangePostCommand : CreatePostCommand, ICommand<CommandExecutionResult>
    { 
        public required Guid Id { get; set; }  
         
    }
     
}
