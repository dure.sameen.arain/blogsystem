﻿using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Command;
using Dureforce.Cqrs.Shared.Domain;

namespace Dureforce.BlogSystem.PostService.Core.Domain.Aggregates.PostAggregate.Commands
{
    public class CreatePostCommand : BaseCommand, ICommand<CommandExecutionResult> 
    { 
        public required string Title { get; set; }
        public required string Body { get; set; }
        public required Guid CategoryId { get; set; }
        public  DateTime? PublishedOn { get; set; }
    }
}
