﻿using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Command;
using Dureforce.Cqrs.Shared.Domain;

namespace Dureforce.BlogSystem.PostService.Core.Domain.Aggregates.PostAggregate.Commands
{
    public class DeletePostCommand : BaseCommand, ICommand<CommandExecutionResult>
    {
        public required Guid Id { get; set; }
    }
}
