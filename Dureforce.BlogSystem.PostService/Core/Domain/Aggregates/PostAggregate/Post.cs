﻿using Dureforce.Cqrs.Shared.Domain;
using Dureforce.Cqrs.Shared.Extension;
using Dureforce.Database.Rdbms.SqlServer.Model;

namespace Dureforce.BlogSystem.PostService.Core.Domain.Aggregates.PostAggregate
{
    [DataTable]
    public class Post : BaseEntity<Guid>
    {
        public required string  Title { get; set; }
        public  string?  Slug { get; set; }
        public required string  Body { get; set; }
        public required Guid   CategoryId { get; set; }
        public DateTime? PublishedOn { get; set; }


        public static Post Create(
              string title,
              string body,
              Guid   categoryId,
              string userCLaimsId,
              DateTime? publishedOn)
        {
            Post post = new()
            {
                Id = Guid.NewGuid(),
                Title = title,
                Slug = title.Slugify(),
                Body=  body,
                CategoryId =categoryId ,
                CreatedBy = userCLaimsId,
                CreatedOn = DateTime.Now,
                PublishedOn=publishedOn
            };

            return post;
        }


        public void Update(
             string title, 
             string body,
             Guid categoryId,
             string userCLaimsId,
              DateTime? publishedOn)
        {
            Title = title;  
            Slug = title.Slugify();
            Body = body;
            CategoryId = categoryId;
            ModifiedBy = userCLaimsId;
            ModifiedOn = DateTime.Now;
            PublishedOn = publishedOn;
        }


    }
}
