﻿using Dureforce.Cqrs.Shared.Application.Execution;
using Dureforce.Cqrs.Shared.Extension;

namespace Dureforce.BlogSystem.PostService.Core.Startup
{
    public class CoreModule : IModule
    {
        public void Load(IServiceCollection services)
        {
            services.AddScoped<ICommandExecutor, CommandExecutor>();
            services.AddScoped<IQueryExecutor, QueryExecutor>();
            
        }
    }
}
