﻿using Dureforce.BlogSystem.PostService.Core.Application.ReadModels.Post;
using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Models;
 

namespace Dureforce.BlogSystem.PostService.Core.Application.Queries.Post
{
    public class GetPostByIdQuery : IQuery<ResponseModel<PostReadModel>>
    { 
        public required Guid Id { get; set; }
    }
}
