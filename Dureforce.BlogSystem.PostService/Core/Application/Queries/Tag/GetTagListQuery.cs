﻿using Dureforce.BlogSystem.PostService.Core.Application.ReadModels.Tag;
using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Models;

namespace Dureforce.BlogSystem.PostService.Core.Application.Queries.Tag
{
    public class GetTagListQuery : IQuery<ResponseModel<IList<TagReadModel>>>
    {
    }
}
