﻿using Dureforce.BlogSystem.PostService.Core.Application.ReadModels.Tag;
using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Models;
 

namespace Dureforce.BlogSystem.PostService.Core.Application.Queries.Tag
{
    public class GetTagByIdQuery : IQuery<ResponseModel<TagReadModel>>
    { 
        public required Guid Id { get; set; }
    }
}
