﻿using Dureforce.BlogSystem.PostService.Core.Application.ReadModels.Category;
using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Models;
 

namespace Dureforce.BlogSystem.PostService.Core.Application.Queries.Category
{
    public class GetCategoryByIdQuery : IQuery<ResponseModel<CategoryReadModel>>
    { 
        public required Guid Id { get; set; }
    }
}
