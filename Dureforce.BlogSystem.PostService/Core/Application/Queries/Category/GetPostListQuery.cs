﻿using Dureforce.BlogSystem.PostService.Core.Application.ReadModels.Category;
using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Models;

namespace Dureforce.BlogSystem.PostService.Core.Application.Queries.Category
{
    public class GetCategoryListQuery : IQuery<ResponseModel<IList<CategoryReadModel>>>
    {
    }
}
