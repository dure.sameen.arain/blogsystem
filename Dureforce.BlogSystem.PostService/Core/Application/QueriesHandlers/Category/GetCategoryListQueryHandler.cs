﻿using Dureforce.BlogSystem.PostService.Core.Application.Queries.Category;
using Dureforce.BlogSystem.PostService.Core.Application.QueriesHandlers.Category.Extenstions;
using Dureforce.BlogSystem.PostService.Core.Application.ReadModels.Category;
using Dureforce.Cqrs.Shared.Application.Handlers;
using Dureforce.Cqrs.Shared.Enums;
using Dureforce.Cqrs.Shared.Models;
using Dureforce.Cqrs.Shared.Response;
using Dureforce.Database.Rdbms;
using System.Net;
 

namespace Dureforce.BlogSystem.PostService.Core.Application.QueriesHandlers.Category
{
    public class GetCategoryListQueryHandler : QueryHandler<GetCategoryListQuery, IList<CategoryReadModel>>
    {
        private readonly IRdbmsDatabase _database;

        public GetCategoryListQueryHandler(IRdbmsDatabase database)
        {
            _database = database;
        }

        public async override Task<ResponseModel<IList<CategoryReadModel>>> Handle(GetCategoryListQuery query, CancellationToken cancellationToken)
        {
            HttpStatusCode httpStatusCode = HttpStatusCode.InternalServerError;
            MessageType messageType = MessageType.Error;
            IList<CategoryReadModel> listReadModel = default!;
            string errorMessage = "";
            var Categorys = _database.Query<Domain.Aggregates.CategoryAggregate.Category>().Take(10).ToList ();
             
            if (Categorys == null)
            {
                httpStatusCode = HttpStatusCode.NotFound;
                messageType = MessageType.Info;
                errorMessage = $"Records not found!";
            }
            else
            {
                listReadModel = Categorys.Select(p => p.ToModel()).ToList(); 
                httpStatusCode = HttpStatusCode.OK;
                messageType = MessageType.Success;
            }

            await Task.CompletedTask;

            return ResponseManager.GenerateResponseModel(httpStatusCode, messageType, messageType.ToString(), null, listReadModel, errorMessage);
        }

        
    }
}
