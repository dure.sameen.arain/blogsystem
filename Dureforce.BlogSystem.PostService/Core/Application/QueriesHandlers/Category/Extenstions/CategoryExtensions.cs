﻿using Dureforce.BlogSystem.PostService.Core.Application.ReadModels.Category;
using Dureforce.BlogSystem.PostService.Core.Domain.Aggregates.PostAggregate;

namespace Dureforce.BlogSystem.PostService.Core.Application.QueriesHandlers.Category.Extenstions
{/// <summary>
/// category extensions
/// </summary>
    public static class CategoryExtensions
    {

         /// <summary>
         /// to single readmodel record
         /// </summary>
         /// <param name="category"></param>
         /// <returns></returns>
        public static CategoryReadModel ToModel(this Domain.Aggregates.CategoryAggregate.Category category)
        {
            if (category == null) return default!;
            return new CategoryReadModel(category.Id, category.Name );

        }
    }
}
