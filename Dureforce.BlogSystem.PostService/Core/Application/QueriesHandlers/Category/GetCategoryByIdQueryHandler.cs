﻿using Dureforce.BlogSystem.PostService.Core.Application.Queries.Category;
using Dureforce.BlogSystem.PostService.Core.Application.QueriesHandlers.Category.Extenstions;
using Dureforce.BlogSystem.PostService.Core.Application.ReadModels.Category;
using Dureforce.Cqrs.Shared.Application.Handlers;
using Dureforce.Cqrs.Shared.Enums;
using Dureforce.Cqrs.Shared.Models;
using Dureforce.Cqrs.Shared.Response;
using Dureforce.Database.Rdbms;
using System.Net;
 

namespace Dureforce.BlogSystem.PostService.Core.Application.QueriesHandlers.Category
{
    public class GetCategoryByIdQueryHandler : QueryHandler<GetCategoryByIdQuery,CategoryReadModel>
    {
        private readonly IRdbmsDatabase _database;

        public GetCategoryByIdQueryHandler(IRdbmsDatabase database)
        {
            _database = database;
        }

        public async override Task<ResponseModel<CategoryReadModel>> Handle(GetCategoryByIdQuery query, CancellationToken cancellationToken)
        {
            HttpStatusCode httpStatusCode = HttpStatusCode.InternalServerError;
            MessageType messageType = MessageType.Error;
            CategoryReadModel readModel = default!;
            string errorMessage = "";
            var Category = _database.Query<Domain.Aggregates.CategoryAggregate.Category>(p=>p.Id== query.Id).FirstOrDefault  ();
             
            if (Category == null)
            {
                httpStatusCode = HttpStatusCode.NotFound;
                messageType = MessageType.Info;
                errorMessage = $"Records not found!";
            }
            else
            {
                readModel = Category.ToModel(); 
                httpStatusCode = HttpStatusCode.OK;
                messageType = MessageType.Success;
            }

            await Task.CompletedTask;

            return ResponseManager.GenerateResponseModel(httpStatusCode, messageType, messageType.ToString(), null, readModel, errorMessage);
        }

        
    }
}
