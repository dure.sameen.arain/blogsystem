﻿using Dureforce.BlogSystem.PostService.Core.Application.ReadModels.Tag;
using Dureforce.BlogSystem.PostService.Core.Domain.Aggregates.PostAggregate;

namespace Dureforce.BlogSystem.PostService.Core.Application.QueriesHandlers.Tag.Extenstions
{/// <summary>
/// tag extensions
/// </summary>
    public static class TagExtensions
    { 
        /// <summary>
        /// convert to model instance
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        public static TagReadModel ToModel(this Domain.Aggregates.TagAggregate.Tag tag)
        {
            if (tag == null) return default!;
            return new TagReadModel(tag.Id, tag.Name);
        }
    }
}
