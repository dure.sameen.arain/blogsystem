﻿using Dureforce.BlogSystem.PostService.Core.Application.Queries.Tag;
using Dureforce.BlogSystem.PostService.Core.Application.QueriesHandlers.Tag.Extenstions;
using Dureforce.BlogSystem.PostService.Core.Application.ReadModels.Tag;
using Dureforce.Cqrs.Shared.Application.Handlers;
using Dureforce.Cqrs.Shared.Enums;
using Dureforce.Cqrs.Shared.Models;
using Dureforce.Cqrs.Shared.Response;
using Dureforce.Database.Rdbms;
using System.Net;
 

namespace Dureforce.BlogSystem.PostService.Core.Application.QueriesHandlers.Tag
{
    public class GetTagListQueryHandler : QueryHandler<GetTagListQuery, IList<TagReadModel>>
    {
        private readonly IRdbmsDatabase _database;

        public GetTagListQueryHandler(IRdbmsDatabase database)
        {
            _database = database;
        }

        public async override Task<ResponseModel<IList<TagReadModel>>> Handle(GetTagListQuery query, CancellationToken cancellationToken)
        {
            HttpStatusCode httpStatusCode = HttpStatusCode.InternalServerError;
            MessageType messageType = MessageType.Error;
            IList<TagReadModel> listReadModel = default!;
            string errorMessage = "";
            var Tags = _database.Query<Domain.Aggregates.TagAggregate.Tag>().Take(10).ToList ();
             
            if (Tags == null)
            {
                httpStatusCode = HttpStatusCode.NotFound;
                messageType = MessageType.Info;
                errorMessage = $"Records not found!";
            }
            else
            {
                listReadModel = Tags.Select(p => p.ToModel()).ToList(); 
                httpStatusCode = HttpStatusCode.OK;
                messageType = MessageType.Success;
            }

            await Task.CompletedTask;

            return ResponseManager.GenerateResponseModel(httpStatusCode, messageType, messageType.ToString(), null, listReadModel, errorMessage);
        }

        
    }
}
