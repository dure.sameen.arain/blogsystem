﻿using Dureforce.BlogSystem.PostService.Core.Application.Queries.Tag;
using Dureforce.BlogSystem.PostService.Core.Application.QueriesHandlers.Tag.Extenstions;
using Dureforce.BlogSystem.PostService.Core.Application.ReadModels.Tag;
using Dureforce.Cqrs.Shared.Application.Handlers;
using Dureforce.Cqrs.Shared.Enums;
using Dureforce.Cqrs.Shared.Models;
using Dureforce.Cqrs.Shared.Response;
using Dureforce.Database.Rdbms;
using System.Net;
 

namespace Dureforce.BlogSystem.PostService.Core.Application.QueriesHandlers.Tag
{
    public class GetTagByIdQueryHandler : QueryHandler<GetTagByIdQuery,TagReadModel>
    {
        private readonly IRdbmsDatabase _database;

        public GetTagByIdQueryHandler(IRdbmsDatabase database)
        {
            _database = database;
        }

        public async override Task<ResponseModel<TagReadModel>> Handle(GetTagByIdQuery query, CancellationToken cancellationToken)
        {
            HttpStatusCode httpStatusCode = HttpStatusCode.InternalServerError;
            MessageType messageType = MessageType.Error;
            TagReadModel readModel = default!;
            string errorMessage = "";
            var Tag = _database.Query<Domain.Aggregates.TagAggregate.Tag>(p=>p.Id== query.Id).FirstOrDefault  ();
             
            if (Tag == null)
            {
                httpStatusCode = HttpStatusCode.NotFound;
                messageType = MessageType.Info;
                errorMessage = $"Records not found!";
            }
            else
            {
                readModel = Tag.ToModel(); 
                httpStatusCode = HttpStatusCode.OK;
                messageType = MessageType.Success;
            }

            await Task.CompletedTask;

            return ResponseManager.GenerateResponseModel(httpStatusCode, messageType, messageType.ToString(), null, readModel, errorMessage);
        }

        
    }
}
