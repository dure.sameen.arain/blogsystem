﻿using Dureforce.BlogSystem.PostService.Core.Application.ReadModels.Post;

namespace Dureforce.BlogSystem.PostService.Core.Application.QueriesHandlers.Post.Extenstions
{/// <summary>
/// posts extensions
/// </summary>
    public static class PostExtensions
    {
        /// <summary>
        /// convert to read model list
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        public static PostReadModelMinimum ToMinimumModel(this Domain.Aggregates.PostAggregate.Post post)
        {
            return new PostReadModelMinimum (post.Id, post.Title, post.CreatedOn, post.CreatedBy);

        }
        /// <summary>
        /// convert to a single read model
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        public static PostReadModel ToModel(this Domain.Aggregates.PostAggregate.Post post)
        { 
            var tags = new List<string>() { "hi" };
            return new PostReadModel(post.Id, post.Title, post.Slug,post.Body , post.CategoryId, post.CreatedOn, post.CreatedBy, tags);

        }
    }
}
