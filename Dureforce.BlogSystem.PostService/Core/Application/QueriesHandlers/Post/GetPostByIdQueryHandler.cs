﻿using Dureforce.BlogSystem.PostService.Core.Application.Queries.Post;
using Dureforce.BlogSystem.PostService.Core.Application.QueriesHandlers.Post.Extenstions;
using Dureforce.BlogSystem.PostService.Core.Application.ReadModels.Comment;
using Dureforce.BlogSystem.PostService.Core.Application.ReadModels.Post;
using Dureforce.BlogSystem.PostService.Core.Shared.Enums;
using Dureforce.Cache.Handler;
using Dureforce.Cqrs.Shared.Application.Handlers;
using Dureforce.Cqrs.Shared.Enums;
using Dureforce.Cqrs.Shared.Models;
using Dureforce.Cqrs.Shared.Response;
using Dureforce.Database.Rdbms;
using Dureforce.MessageBroker;
using Dureforce.MessageBroker.Models;
using Microsoft.EntityFrameworkCore;
using System.Net;


namespace Dureforce.BlogSystem.PostService.Core.Application.QueriesHandlers.Post
{/// <summary>
/// get a post by id
/// </summary>
    public class GetPostByIdQueryHandler : QueryHandler<GetPostByIdQuery,PostReadModel>
    {
        private readonly IRdbmsDatabase _database; 
        private readonly ICacheHandler _cache;
        private readonly IMessageBroker _messageBroker;
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="database"></param>
        /// <param name="cache"></param>
        /// <param name="messageBroker"></param>
        public GetPostByIdQueryHandler(IRdbmsDatabase database, ICacheHandler cache, IMessageBroker messageBroker)
        {
            _cache = cache;
            _database = database;
            _messageBroker =  messageBroker;
        }
/// <summary>
/// handler
/// </summary>
/// <param name="query"></param>
/// <param name="cancellationToken"></param>
/// <returns></returns>
        public async override Task<ResponseModel<PostReadModel>> Handle(GetPostByIdQuery query, CancellationToken cancellationToken)
        {
            HttpStatusCode httpStatusCode = HttpStatusCode.InternalServerError;
            MessageType messageType = MessageType.Error; 
            string errorMessage = "";
            var cacheKey = $"id-{query.Id}";
            PostReadModel post = await _cache.GetSetCache(CacheKeys.Post.ToString(), cacheKey, async () => await GetData(query.Id));
            await CommentsSendRequest(post);
            var comments = await WaitForMessageAsync<ResponseModel<CommentReadModelList>>(MessageBrokerConstants.ResponseCommentsTopic, MessageBrokerConstants.ResponseCommentsTopicSubscription, $"{post.Id}", TimeSpan.FromSeconds(3));
            if (comments!=null && comments.StatusCode == HttpStatusCode.OK)
                post.Comments = comments.Result;
            if (post == null)
            {
                httpStatusCode = HttpStatusCode.NotFound;
                messageType = MessageType.Info;
                errorMessage = $"Records not found!";
            }
            else
            {

                httpStatusCode = HttpStatusCode.OK;
                messageType = MessageType.Success;
            }

            await Task.CompletedTask;

            return ResponseManager.GenerateResponseModel(httpStatusCode, messageType, messageType.ToString(), null, post, errorMessage);
        }

        private async Task CommentsSendRequest(PostReadModel post)
        {
            GeneralQueueMessage<PostReadModel> distributedMessage = new()
            {
                Body = post,
                Queue = new GeneralQueue
                {
                    Exchange = MessageBrokerConstants.RequestCommentsQueue,
                    Label = $"postservice-post.{post.Id}"
                }
            };

            await _messageBroker.PublishObject(distributedMessage, CancellationToken.None);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<PostReadModel> GetData(Guid id)
        {
            var post = await _database.Query<Domain.Aggregates.PostAggregate.Post>(p => p.Id == id).FirstOrDefaultAsync();
            if (post == null) return default!;
            var readModel = post.ToModel();
            return readModel;
        }

        private async Task<T> WaitForMessageAsync<T>(string topicname, string subscription,string operationId, TimeSpan timeout )
        {
            var simpleOperationId = operationId.ToLower().Replace("-", "");

            string subscriptionName = $"{subscription}{simpleOperationId}";
            string query = $"sys.label LIKE '%{simpleOperationId}%'";
            try
            {
                await _messageBroker.CreateSubscriber(topicname, subscriptionName, query);
                
            }
            catch(Exception exp)
            { } 
            string entityPath = $"{topicname}/subscriptions/{subscriptionName}";
            ISingleMessageConsumer consumer = new GeneralSingleMessageConsumer(entityPath, ReceiveMode.ReceiveAndDelete, timeout, default!) ;
            T result = await _messageBroker.SpecificReceiver<T>(consumer);

            
            try
            {
                await _messageBroker.DeleteSubscriber(MessageBrokerConstants.ResponseCommentsTopic, subscriptionName);
                 
            }
            catch
            {
            }
            return result;
        }

    }
}
