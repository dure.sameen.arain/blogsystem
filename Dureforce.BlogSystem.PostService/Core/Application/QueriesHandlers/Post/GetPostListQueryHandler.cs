﻿using Dureforce.BlogSystem.PostService.Core.Application.Queries.Post;
using Dureforce.BlogSystem.PostService.Core.Application.QueriesHandlers.Post.Extenstions;
using Dureforce.BlogSystem.PostService.Core.Application.ReadModels.Post;
using Dureforce.BlogSystem.PostService.Core.Shared.Enums;
using Dureforce.Cache.Handler;
using Dureforce.Cqrs.Shared.Application.Handlers;
using Dureforce.Cqrs.Shared.Enums;
using Dureforce.Cqrs.Shared.Models;
using Dureforce.Cqrs.Shared.Response;
using Dureforce.Database.Rdbms;
using Microsoft.EntityFrameworkCore;
using System.Net;
 

namespace Dureforce.BlogSystem.PostService.Core.Application.QueriesHandlers.Post
{/// <summary>
/// List of all posts
/// </summary>
    public class GetPostListQueryHandler : QueryHandler<GetPostListQuery, PostReadModelList>
    {
        private readonly IRdbmsDatabase _database;
        private readonly ICacheHandler _cache;
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="database"></param>
        /// <param name="cache"></param>
        public GetPostListQueryHandler(IRdbmsDatabase database, ICacheHandler  cache)
        {
            _database = database;
            _cache = cache;
        }
        /// <summary>
        /// Handler for list of posts
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async override Task<ResponseModel<PostReadModelList>> Handle(GetPostListQuery query, CancellationToken cancellationToken)
        {
            HttpStatusCode httpStatusCode = HttpStatusCode.InternalServerError;
            MessageType messageType = MessageType.Error; 
            string errorMessage = "";

            var cacheKey = "List";
            PostReadModelList posts =await  _cache.GetSetCache(CacheKeys.Post.ToString (), cacheKey,  async () => await GetData()  );
             
             
            if (posts.List == null || posts.List.Count ()==0)
            {
                httpStatusCode = HttpStatusCode.NotFound;
                messageType = MessageType.Info;
                errorMessage = $"Records not found!";
            }
            else
            {
                
                httpStatusCode = HttpStatusCode.OK;
                messageType = MessageType.Success;
            } 

            return ResponseManager.GenerateResponseModel(httpStatusCode, messageType, messageType.ToString(), null, posts, errorMessage);
        }

        /// <summary>
        /// get data from database and change to dto/readmodel
        /// </summary>
        /// <returns></returns>
        public async Task<PostReadModelList> GetData()
        {
            var posts =await  _database.Query<Domain.Aggregates.PostAggregate.Post>().Take(10).ToListAsync();
            var listReadModel = posts.Select(p => p.ToMinimumModel()).ToList();
            return   new PostReadModelList() { List = listReadModel };
        }

    }
}
