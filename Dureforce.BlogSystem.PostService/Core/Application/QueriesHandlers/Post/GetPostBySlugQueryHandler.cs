﻿using Dureforce.BlogSystem.PostService.Core.Application.Queries.Post;
using Dureforce.BlogSystem.PostService.Core.Application.QueriesHandlers.Post.Extenstions;
using Dureforce.BlogSystem.PostService.Core.Application.ReadModels.Post;
using Dureforce.BlogSystem.PostService.Core.Shared.Enums;
using Dureforce.Cache.Handler;
using Dureforce.Cqrs.Shared.Application.Handlers;
using Dureforce.Cqrs.Shared.Enums;
using Dureforce.Cqrs.Shared.Models;
using Dureforce.Cqrs.Shared.Response;
using Dureforce.Database.Rdbms;
using Microsoft.EntityFrameworkCore;
using System.Net;
 

namespace Dureforce.BlogSystem.PostService.Core.Application.QueriesHandlers.Post
{/// <summary>
/// get a post record by a slug title.
/// </summary>
    public class GetPostBySlugQueryHandler : QueryHandler<GetPostBySlugQuery, PostReadModel>
    {
        private readonly IRdbmsDatabase _database;
        private readonly ICacheHandler _cache;
         
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="database"></param>
        /// <param name="cache"></param>
        public GetPostBySlugQueryHandler(IRdbmsDatabase database, ICacheHandler cache)
        {
            _cache = cache;
            _database = database;
        }
        /// <summary>
        /// Handler
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async override Task<ResponseModel<PostReadModel>> Handle(GetPostBySlugQuery query, CancellationToken cancellationToken)
        {
            HttpStatusCode httpStatusCode = HttpStatusCode.InternalServerError;
            MessageType messageType = MessageType.Error;
            PostReadModel readModel = default!;
            string errorMessage = "";
            var cacheKey = $"slug-{query.Slug}";
            PostReadModel post =await _cache.GetSetCache(CacheKeys.Post.ToString (), cacheKey,async () => await GetData(query.Slug));

            if (post == null)
            {
                httpStatusCode = HttpStatusCode.NotFound;
                messageType = MessageType.Info;
                errorMessage = $"Records not found!";
            }
            else
            {
               
                httpStatusCode = HttpStatusCode.OK;
                messageType = MessageType.Success;
            }

           
            return ResponseManager.GenerateResponseModel(httpStatusCode, messageType, messageType.ToString(), null, readModel, errorMessage);
        }
        /// <summary>
        /// get record by a slug
        /// </summary>
        /// <param name="slug"></param>
        /// <returns></returns>
        public async Task<PostReadModel> GetData(string slug)
        {
            var post = await _database.Query<Domain.Aggregates.PostAggregate.Post>(p => p.Slug == slug).FirstOrDefaultAsync();
            if (post == null) return default!;
            var readModel = post.ToModel();
            return  readModel;
        }
    }
}
