﻿using Dureforce.BlogSystem.PostService.Core.Domain.Aggregates.PostAggregate.Commands;
using Dureforce.Cqrs.Shared.Application.Handlers;
using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Exception;
using Dureforce.Database.Rdbms;
using Dureforce.Cache.Handler;
using Dureforce.Cache.Group;
using Dureforce.BlogSystem.PostService.Core.Shared.Enums;

namespace Dureforce.BlogSystem.PostService.Core.Application.CommandHandlers.Post
{
    public class DeletePostCommandHandler : CommandHandler<DeletePostCommand>
    {
        private readonly IRdbmsDatabase _database;
        private readonly ICacheGroup _cache; 

        public DeletePostCommandHandler(IRdbmsDatabase database, ICacheGroup cache)
        {
            _cache = cache;
            _database = database;
        }

        public async override Task<CommandExecutionResult> Handle(DeletePostCommand command, CancellationToken cancellationToken)
        {

            if (command == null)
            {
                return await BadRequest($"{typeof(DeletePostCommand).Name}");
            }

            Domain.Aggregates.PostAggregate.Post post = await _database.Find<Domain.Aggregates.PostAggregate.Post>(x => x.Id == command.Id && !x.Deleted) ?? throw new RecordNotFoundErrorException(404, "Post not found!");

            post.Delete(command.GetUserClaimsId()); 

            await _database.Delete(post);

            await _database.SaveChanges();
            _cache.RemoveGroup(CacheKeys.Post.ToString(), "");

            return await Ok(DomainOperationResult.Success(post));
        }
    }
}
