﻿using Dureforce.Cqrs.Shared.Application.Handlers;
using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Exception;
using Dureforce.Database.Rdbms;
using Dureforce.BlogSystem.PostService.Core.Domain.Aggregates.PostAggregate.Commands;
using Dureforce.Cache.Group;
using Dureforce.BlogSystem.PostService.Core.Shared.Enums;

namespace Dureforce.BlogSystem.PostService.Core.Application.CommandHandlers.Post
{
    public class CreatePostCommandHandler : CommandHandler<CreatePostCommand>
    {
        private readonly IRdbmsDatabase _database;
        private readonly ICacheGroup _cache;
         
        public CreatePostCommandHandler(IRdbmsDatabase database, ICacheGroup cache)
        {
            _cache = cache;
            _database = database;
        }

        public async override Task<CommandExecutionResult> Handle(CreatePostCommand command, CancellationToken cancellationToken)
        {

            if (command == null)
            {
                return await BadRequest($"{typeof(CreatePostCommand).Name}");
            }

            Domain.Aggregates.PostAggregate.Post existingPost = await _database.Find<Domain.Aggregates.PostAggregate.Post>(x => x.Title.ToLower() == command.Title.ToLower() && !x.Deleted);
            if (existingPost != null)
                throw new RecordExistErrorException(409, "Post already exist!");

            var post = Domain.Aggregates.PostAggregate.Post.Create(command.Title, command.Body, command.CategoryId, command.GetUserClaimsId(), command.PublishedOn);

            await _database.Create(post); 
            await _database.SaveChanges();

            _cache.RemoveGroup(CacheKeys.Post.ToString(), "");

            return await Ok(DomainOperationResult.Create(post));
        }
    }
}

