﻿using Dureforce.Cqrs.Shared.Application.Handlers;
using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Exception;
using Dureforce.Database.Rdbms;
using Dureforce.BlogSystem.PostService.Core.Domain.Aggregates.PostAggregate.Commands;
using Dureforce.Cache.Handler;
using Dureforce.BlogSystem.PostService.Core.Shared.Enums;
using Dureforce.Cache.Group;

namespace Dureforce.BlogSystem.PostService.Core.Application.CommandHandlers.Post
{
    public class ChangePostCommandHandler : CommandHandler<ChangePostCommand>
    {
        private readonly IRdbmsDatabase _database;
        private readonly ICacheGroup _cache;
        
        public ChangePostCommandHandler(IRdbmsDatabase database, ICacheGroup cache)
        {
            _cache = cache;
            _database = database;
        }

        public async override Task<CommandExecutionResult> Handle(ChangePostCommand command, CancellationToken cancellationToken)
        {

            if (command == null)
            {
                return await BadRequest($"{typeof(ChangePostCommand).Name}");
            }

            Domain.Aggregates.PostAggregate.Post post = await _database.Find<Domain.Aggregates.PostAggregate.Post>(x => x.Id == command.Id && !x.Deleted) ?? throw new RecordNotFoundErrorException(404, "Post not found!");

            post.Update(command.Title, command.Body, command.CategoryId, command.GetUserClaimsId() , command.PublishedOn );

            await _database.Update(post); 
            await _database.SaveChanges();

            _cache.RemoveGroup(CacheKeys.Post.ToString(), "");
            return await Ok(DomainOperationResult.Update(post));
        }
    }
}


