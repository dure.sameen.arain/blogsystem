﻿using Dureforce.Cqrs.Shared.Application.Handlers;
using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Exception;
using Dureforce.Database.Rdbms;
using Dureforce.BlogSystem.PostService.Core.Domain.Aggregates.TagAggregate.Commands;

namespace Dureforce.BlogSystem.PostService.Core.Application.CommandHandlers.Tag
{
    public class ChangeTagCommandHandler : CommandHandler<ChangeTagCommand>
    {
        private readonly IRdbmsDatabase _database;

        public ChangeTagCommandHandler(IRdbmsDatabase database)
        {
            _database = database;
        }

        public async override Task<CommandExecutionResult> Handle(ChangeTagCommand command, CancellationToken cancellationToken)
        {

            if (command == null)
            {
                return await BadRequest($"{typeof(ChangeTagCommand).Name}");
            }

            Domain.Aggregates.TagAggregate.Tag Tag = await _database.Find<Domain.Aggregates.TagAggregate.Tag>(x => x.Id == command.Id && !x.Deleted) ?? throw new RecordNotFoundErrorException(404, "Tag not found!");

            Tag.Update(command.Name, command.GetUserClaimsId() );

            await _database.Update(Tag);

            await _database.SaveChanges();

            return await Ok(DomainOperationResult.Update(Tag));
        }
    }
}


