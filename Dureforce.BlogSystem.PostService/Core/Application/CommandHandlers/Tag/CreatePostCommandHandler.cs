﻿using Dureforce.Cqrs.Shared.Application.Handlers;
using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Exception;
using Dureforce.Database.Rdbms;
using Dureforce.BlogSystem.PostService.Core.Domain.Aggregates.TagAggregate.Commands;

namespace Dureforce.BlogSystem.PostService.Core.Application.CommandHandlers.Tag
{
    public class CreateTagCommandHandler : CommandHandler<CreateTagCommand>
    {
        private readonly IRdbmsDatabase _database;

        public CreateTagCommandHandler(IRdbmsDatabase database)
        {
            _database = database;
        }

        public async override Task<CommandExecutionResult> Handle(CreateTagCommand command, CancellationToken cancellationToken)
        {

            if (command == null)
            {
                return await BadRequest($"{typeof(CreateTagCommand).Name}");
            }

            Domain.Aggregates.TagAggregate.Tag existingTag = await _database.Find<Domain.Aggregates.TagAggregate.Tag>(x => x.Name.ToLower() == command.Name.ToLower() && !x.Deleted);
            if (existingTag != null)
                throw new RecordExistErrorException(409, "Tag already exist!");

            var Tag = Domain.Aggregates.TagAggregate.Tag.Create(command.Name, command.GetUserClaimsId());

            await _database.Create(Tag);

            await _database.SaveChanges();

            return await Ok(DomainOperationResult.Create(Tag));
        }
    }
}

