﻿using Dureforce.BlogSystem.PostService.Core.Domain.Aggregates.TagAggregate.Commands;
using Dureforce.Cqrs.Shared.Application.Handlers;
using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Exception;
using Dureforce.Database.Rdbms;

namespace Dureforce.BlogSystem.PostService.Core.Application.CommandHandlers.Tag
{
    public class DeleteTagCommandHandler : CommandHandler<DeleteTagCommand>
    {
        private readonly IRdbmsDatabase _database;

        public DeleteTagCommandHandler(IRdbmsDatabase database)
        {
            _database = database;
        }

        public async override Task<CommandExecutionResult> Handle(DeleteTagCommand command, CancellationToken cancellationToken)
        {

            if (command == null)
            {
                return await BadRequest($"{typeof(DeleteTagCommand).Name}");
            }

            Domain.Aggregates.TagAggregate.Tag Tag = await _database.Find<Domain.Aggregates.TagAggregate.Tag>(x => x.Id == command.Id && !x.Deleted) ?? throw new RecordNotFoundErrorException(404, "Tag not found!");

            Tag.Delete(command.GetUserClaimsId());

            await _database.Delete(Tag);

            await _database.SaveChanges();

            return await Ok(DomainOperationResult.Success(Tag));
        }
    }
}
