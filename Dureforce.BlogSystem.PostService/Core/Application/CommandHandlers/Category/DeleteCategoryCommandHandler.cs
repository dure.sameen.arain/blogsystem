﻿using Dureforce.BlogSystem.PostService.Core.Domain.Aggregates.CategoryAggregate.Commands;
using Dureforce.Cqrs.Shared.Application.Handlers;
using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Exception;
using Dureforce.Database.Rdbms;

namespace Dureforce.BlogSystem.PostService.Core.Application.CommandHandlers.Category
{
    public class DeleteCategoryCommandHandler : CommandHandler<DeleteCategoryCommand>
    {
        private readonly IRdbmsDatabase _database;

        public DeleteCategoryCommandHandler(IRdbmsDatabase database)
        {
            _database = database;
        }

        public async override Task<CommandExecutionResult> Handle(DeleteCategoryCommand command, CancellationToken cancellationToken)
        {

            if (command == null)
            {
                return await BadRequest($"{typeof(DeleteCategoryCommand).Name}");
            }

            Domain.Aggregates.CategoryAggregate.Category Category = await _database.Find<Domain.Aggregates.CategoryAggregate.Category>(x => x.Id == command.Id && !x.Deleted) ?? throw new RecordNotFoundErrorException(404, "Category not found!");

            Category.Delete(command.GetUserClaimsId());

            await _database.Delete(Category);

            await _database.SaveChanges();

            return await Ok(DomainOperationResult.Success(Category));
        }
    }
}
