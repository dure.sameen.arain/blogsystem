﻿using Dureforce.Cqrs.Shared.Application.Handlers;
using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Exception;
using Dureforce.Database.Rdbms;
using Dureforce.BlogSystem.PostService.Core.Domain.Aggregates.CategoryAggregate.Commands;

namespace Dureforce.BlogSystem.PostService.Core.Application.CommandHandlers.Category
{
    public class CreateCategoryCommandHandler : CommandHandler<CreateCategoryCommand>
    {
        private readonly IRdbmsDatabase _database;

        public CreateCategoryCommandHandler(IRdbmsDatabase database)
        {
            _database = database;
        }

        public async override Task<CommandExecutionResult> Handle(CreateCategoryCommand command, CancellationToken cancellationToken)
        {

            if (command == null)
            {
                return await BadRequest($"{typeof(CreateCategoryCommand).Name}");
            }

            Domain.Aggregates.CategoryAggregate.Category existingCategory = await _database.Find<Domain.Aggregates.CategoryAggregate.Category>(x => x.Name.ToLower() == command.Name.ToLower() && !x.Deleted);
            if (existingCategory != null)
                throw new RecordExistErrorException(409, "Category already exist!");

            var Category = Domain.Aggregates.CategoryAggregate.Category.Create(command.Name , command.GetUserClaimsId());

            await _database.Create(Category);

            await _database.SaveChanges();

            return await Ok(DomainOperationResult.Create(Category));
        }
    }
}

