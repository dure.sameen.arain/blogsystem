﻿using Dureforce.Cqrs.Shared.Application.Handlers;
using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Exception;
using Dureforce.Database.Rdbms;
using Dureforce.BlogSystem.PostService.Core.Domain.Aggregates.CategoryAggregate.Commands;

namespace Dureforce.BlogSystem.PostService.Core.Application.CommandHandlers.Category
{
    public class ChangeCategoryCommandHandler : CommandHandler<ChangeCategoryCommand>
    {
        private readonly IRdbmsDatabase _database;

        public ChangeCategoryCommandHandler(IRdbmsDatabase database)
        {
            _database = database;
        }

        public async override Task<CommandExecutionResult> Handle(ChangeCategoryCommand command, CancellationToken cancellationToken)
        {

            if (command == null)
            {
                return await BadRequest($"{typeof(ChangeCategoryCommand).Name}");
            }

            Domain.Aggregates.CategoryAggregate.Category Category = await _database.Find<Domain.Aggregates.CategoryAggregate.Category>(x => x.Id == command.Id && !x.Deleted) ?? throw new RecordNotFoundErrorException(404, "Category not found!");

            Category.Update(command.Name,   command.GetUserClaimsId() );

            await _database.Update(Category);

            await _database.SaveChanges();

            return await Ok(DomainOperationResult.Update(Category));
        }
    }
}


