﻿namespace Dureforce.BlogSystem.PostService.Core.Application.ReadModels.Tag
{
    public class TagReadModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }


        public TagReadModel(Guid id, string name)
        {
            Id = id;
            Name = name;

        }
    }
}
