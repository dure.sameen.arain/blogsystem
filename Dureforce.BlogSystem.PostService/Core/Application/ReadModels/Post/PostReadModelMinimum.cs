﻿namespace Dureforce.BlogSystem.PostService.Core.Application.ReadModels.Post
{
    public class PostReadModelMinimum
    {
        public Guid Id { get; set; }
        public string Title { get; set; } 
        public DateTime? CreateOn { get; set; }
        public string CreateBy { get; set; } 

        public PostReadModelMinimum()
        { }
        public PostReadModelMinimum(Guid id, string title, DateTime? createOn, string createBy )
        {
            Id = id;
            Title = title; 
            CreateOn = createOn;
            CreateBy = createBy;
            
        }
    }
}
