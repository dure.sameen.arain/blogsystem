﻿namespace Dureforce.BlogSystem.PostService.Core.Application.ReadModels.Post
{
    public class PostReadModelList 
    {
        public IList<PostReadModelMinimum> List { get; set; }
        public PostReadModelList()
        { }
    }
}
