﻿namespace Dureforce.BlogSystem.PostService.Core.Application.ReadModels.Comment
{
    public class CommentReadModel
    {
        public Guid Id { get; set; } 
        public string Body { get; set; }
        public Guid PostId { get; set; }
        public Guid? ParentCommentId { get; set; }
        public DateTime? CreateOn { get; set; }
        public string CreateBy { get; set; } 
        public IList<CommentReadModel> Replies { get; set; }

        public CommentReadModel()
        { }

        public CommentReadModel(Guid id,  string body,Guid postId, Guid? parentCommentId, DateTime? createOn, string createBy)
        {
            Id = id; 
            Body = body;
            PostId = postId;
            ParentCommentId = parentCommentId;
            CreateOn = createOn;
            CreateBy = createBy;
             
        }


    }
}
