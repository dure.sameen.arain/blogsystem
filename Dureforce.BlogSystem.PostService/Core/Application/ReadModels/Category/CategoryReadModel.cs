﻿namespace Dureforce.BlogSystem.PostService.Core.Application.ReadModels.Category
{
    public class CategoryReadModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }


        public CategoryReadModel(Guid id, string name)
        {
            Id = id;
            Name = name;

        }
    }
}
