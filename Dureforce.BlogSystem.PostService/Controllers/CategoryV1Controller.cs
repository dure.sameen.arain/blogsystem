using Dureforce.BlogSystem.PostService.Core.Application.Queries.Category;
using Dureforce.BlogSystem.PostService.Core.Application.ReadModels.Category;
using Dureforce.BlogSystem.PostService.Core.Domain.Aggregates.CategoryAggregate.Commands;
using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Application.Execution;
using Dureforce.Cqrs.Shared.Extension;
using Dureforce.Cqrs.Shared.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Identity.Web;
 


namespace Dureforce.BlogSystem.Controllers
{

    [ApiController]
    [Route("api/v{ver:apiVersion}/category")]
    [AuthorizeForScopes(Scopes =new[]{ "api://81a6a99e-12d0-4ec9-9655-9674a12d59e8/ManageCategories" })]
    public class CategoryV1Controller : ControllerBase
    {

        private readonly ILogger<CategoryV1Controller> _logger;
        private readonly ICommandExecutor _commandExecutor;
        private readonly IQueryExecutor _queryExecutor;
  
        public CategoryV1Controller(ILogger<CategoryV1Controller> logger, ICommandExecutor commandExecutor, IQueryExecutor queryExecutor)
        {
            _commandExecutor = commandExecutor;
            _queryExecutor = queryExecutor;
            _logger = logger;
        }

        /// <summary>
        /// Get list of all Categorys
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet("list")]
        public async Task<ResponseModel<IList<CategoryReadModel>>> GetList( CancellationToken cancellationToken)
        {
            GetCategoryListQuery query = new GetCategoryListQuery();
            return await _queryExecutor.Execute(query, cancellationToken);
        }

        
        /// <summary>
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns>CategoryReadModel</returns>

        [HttpGet("by-id")]
        public async Task<ResponseModel<CategoryReadModel>> GetById([FromQuery] GetCategoryByIdQuery query, CancellationToken cancellationToken)
        {
            return await _queryExecutor.Execute(query, cancellationToken);
        }
        /// <summary>
        /// Create
        /// </summary>
        /// <param name="command"></param>
        /// <returns>CommandExecutionResult</returns>
        [HttpPost]
        public async Task<CommandExecutionResult> Create([FromBody] CreateCategoryCommand command)
        {
            command.SetUserClaimsId(HttpContext.User.UserName());

            return await _commandExecutor.Execute(command, CancellationToken.None);
        }

        
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="command"></param>
        /// <returns>CommandExecutionResult</returns>
        [HttpDelete]
        public async Task<CommandExecutionResult> Delete([FromQuery] DeleteCategoryCommand command)
        {
            command.SetUserClaimsId(HttpContext.User.UserName());

            return await _commandExecutor.Execute(command, CancellationToken.None);
        }

    }    
}