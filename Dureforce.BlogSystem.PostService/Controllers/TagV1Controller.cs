using Dureforce.BlogSystem.PostService.Core.Application.Queries.Tag;
using Dureforce.BlogSystem.PostService.Core.Application.ReadModels.Tag;
using Dureforce.BlogSystem.PostService.Core.Domain.Aggregates.TagAggregate.Commands;
using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Application.Execution;
using Dureforce.Cqrs.Shared.Extension;
using Dureforce.Cqrs.Shared.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Identity.Web;

namespace Dureforce.BlogSystem.Controllers
{

    [ApiController]
    [Route("api/v{ver:apiVersion}/tag")]
    [AuthorizeForScopes(Scopes = new[] {"api://81a6a99e-12d0-4ec9-9655-9674a12d59e8/ManageTags"})]
    public class TagV1Controller : ControllerBase
    {

        private readonly ILogger<TagV1Controller> _logger;
        private readonly ICommandExecutor _commandExecutor;
        private readonly IQueryExecutor _queryExecutor;
  
        public TagV1Controller(ILogger<TagV1Controller> logger, ICommandExecutor commandExecutor, IQueryExecutor queryExecutor)
        {
            _commandExecutor = commandExecutor;
            _queryExecutor = queryExecutor;
            _logger = logger;
        }

        /// <summary>
        /// Get list of all Tags
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet("list")]
        public async Task<ResponseModel<IList<TagReadModel>>> GetList([FromQuery] GetTagListQuery query, CancellationToken cancellationToken)
        {
            return await _queryExecutor.Execute(query, cancellationToken);
        }

        
        /// <summary>
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns>TagReadModel</returns>

        [HttpGet("by-id")]
        public async Task<ResponseModel<TagReadModel>> GetById([FromQuery] GetTagByIdQuery query, CancellationToken cancellationToken)
        {
            return await _queryExecutor.Execute(query, cancellationToken);
        }
        /// <summary>
        /// Create
        /// </summary>
        /// <param name="command"></param>
        /// <returns>CommandExecutionResult</returns>
        [HttpPost]
        public async Task<CommandExecutionResult> Create([FromBody] CreateTagCommand command)
        {
            command.SetUserClaimsId(HttpContext.User.UserName());

            return await _commandExecutor.Execute(command, CancellationToken.None);
        }

        
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="command"></param>
        /// <returns>CommandExecutionResult</returns>
        [HttpDelete]
        public async Task<CommandExecutionResult> Delete([FromQuery] DeleteTagCommand command)
        {
            command.SetUserClaimsId(HttpContext.User.UserName());

            return await _commandExecutor.Execute(command, CancellationToken.None);
        }

    }    
}