using Dureforce.BlogSystem.PostService.Core.Application.Queries.Post;
using Dureforce.BlogSystem.PostService.Core.Application.ReadModels.Post;
using Dureforce.BlogSystem.PostService.Core.Domain.Aggregates.PostAggregate.Commands;
using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Application.Execution;
using Dureforce.Cqrs.Shared.Extension;
using Dureforce.Cqrs.Shared.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Identity.Web;

namespace Dureforce.BlogSystem.Controllers
{

    [ApiController]
    [Route("api/v{ver:apiVersion}/post")]
    [AuthorizeForScopes(Scopes = new[] { "api://81a6a99e-12d0-4ec9-9655-9674a12d59e8/ManagePosts" })]
    public class PostV1Controller : ControllerBase
    {

        private readonly ILogger<PostV1Controller> _logger;
        private readonly ICommandExecutor _commandExecutor;
        private readonly IQueryExecutor _queryExecutor;
  
        public PostV1Controller(ILogger<PostV1Controller> logger, ICommandExecutor commandExecutor, IQueryExecutor queryExecutor)
        {
            _commandExecutor = commandExecutor;
            _queryExecutor = queryExecutor;
            _logger = logger;
        }

        /// <summary>
        /// Get list of all posts
        /// </summary> 
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet("list")]
        public async Task<ResponseModel<PostReadModelList>> GetList(CancellationToken cancellationToken)
        {
            GetPostListQuery query = new GetPostListQuery();
            return await _queryExecutor.Execute(query, cancellationToken);
        }

        /// <summary>
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns>PostReadModel</returns>

        [HttpGet("by-slug")]
        public async Task<ResponseModel<PostReadModel>> GetBySlug([FromQuery] GetPostBySlugQuery query, CancellationToken cancellationToken)
        {
            return await _queryExecutor.Execute(query, cancellationToken);
        }

        /// <summary>
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns>PostReadModel</returns>

        [HttpGet("by-id")]
        public async Task<ResponseModel<PostReadModel>> GetById([FromQuery] GetPostByIdQuery query, CancellationToken cancellationToken)
        {
            return await _queryExecutor.Execute(query, cancellationToken);
        }
        /// <summary>
        /// Create
        /// </summary>
        /// <param name="command"></param>
        /// <returns>CommandExecutionResult</returns>
        [HttpPost]
        public async Task<CommandExecutionResult> Create([FromBody] CreatePostCommand command)
        {
            command.SetUserClaimsId(HttpContext.User.UserName());

            return await _commandExecutor.Execute(command, CancellationToken.None);
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="command"></param>
        /// <returns>CommandExecutionResult</returns>
        [HttpPut]
        public async Task<CommandExecutionResult> Update([FromBody] ChangePostCommand command)
        {
            command.SetUserClaimsId(HttpContext.User.UserName());

            return await _commandExecutor.Execute(command, CancellationToken.None);
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="command"></param>
        /// <returns>CommandExecutionResult</returns>
        [HttpDelete]
        public async Task<CommandExecutionResult> Delete([FromQuery] DeletePostCommand command)
        {
            command.SetUserClaimsId(HttpContext.User.UserName());

            return await _commandExecutor.Execute(command, CancellationToken.None);
        }

    }    
}