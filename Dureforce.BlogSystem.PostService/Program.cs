using Dureforce.BlogSystem.PostService.Core.Startup;
using Dureforce.BlogSystem.PostService.Core.Startup.Attribute;
using Dureforce.Cqrs.Shared.Enums;
using Dureforce.Security.B2C;
using Dureforce.Swagger.B2C.Extensions;
using Dureforce.Swagger.B2C.ViewModels;
using Microsoft.AspNetCore.RateLimiting;
using System.Reflection;
using System.Threading.RateLimiting;

var builder = WebApplication.CreateBuilder(args);
//API Versioning
 builder.Services.AddApiVersioning(config=> config.DefaultApiVersion=  new Asp.Versioning.ApiVersion (majorVersion:0, 0,"Alpha") );

// Add services to the container.
builder.Services.AddAuthentication(builder.Configuration)
    .AddCustomAuthorization(new Dictionary<string, string> { { "Manage Posts", "api://81a6a99e-12d0-4ec9-9655-9674a12d59e8/ManagePosts" }, { "Manage Categories", "api://81a6a99e-12d0-4ec9-9655-9674a12d59e8/ManageCategories" }, { "Manage Tags", "api://81a6a99e-12d0-4ec9-9655-9674a12d59e8/ManageTags" } });

builder.Services.AddControllers(options => { options.Filters.Add<SetStatusCode>(); });
 
var apiDetails = new ApiDetails();
builder.Configuration.GetSection("AzureAd").Bind(apiDetails);


var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
builder.Services.AddSwagger(apiDetails, xmlPath);

// Cache
builder.Services.AddRedisCache(builder.Configuration);

// ServiceBus
 
builder.Services.AddAzureServiceBus(builder.Configuration.GetValue<string>("defaultAppSettings:AzureServiceBusConnectionString"));

builder.Services.StartupConfigureServices(builder.Configuration, ProjectType.Api);
builder.Services.AddRateLimiter(options =>
{
    options.RejectionStatusCode = StatusCodes.Status429TooManyRequests;
    options.AddFixedWindowLimiter("fixed", options =>
    {
        options.PermitLimit = 10;
        options.Window = TimeSpan.FromSeconds(10);
        options.QueueProcessingOrder = QueueProcessingOrder.OldestFirst;
        options.QueueLimit = 5;
    });


});
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwaggerEndpoint(apiDetails.Title, apiDetails.Title, apiDetails.ClientId);

  
}

app.UseHttpsRedirection();
app.UseRateLimiter();
app.UseAuthorization();
app.UseAuthentication();
app.MapControllers();

app.Run();
