﻿using Dureforce.BlogSystem.CommentService.Core.Domain.Aggregates.CommentAggregate.Commands;
using Dureforce.Cqrs.Shared.Application.Handlers;
using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Exception;
using Dureforce.Database.Rdbms;
using Dureforce.Cache.Handler;
using Dureforce.Cache.Group;
using Dureforce.BlogSystem.CommentService.Core.Shared.Enums;

namespace Dureforce.BlogSystem.CommentService.Core.Application.CommandHandlers.Comment
{
    public class DeleteCommentCommandHandler : CommandHandler<DeleteCommentCommand>
    {
        private readonly IRdbmsDatabase _database;
        private readonly ICacheGroup _cache; 

        public DeleteCommentCommandHandler(IRdbmsDatabase database, ICacheGroup cache)
        {
            _cache = cache;
            _database = database;
        }

        public async override Task<CommandExecutionResult> Handle(DeleteCommentCommand command, CancellationToken cancellationToken)
        {

            if (command == null)
            {
                return await BadRequest($"{typeof(DeleteCommentCommand).Name}");
            }

            Domain.Aggregates.CommentAggregate.Comment comment = await _database.Find<Domain.Aggregates.CommentAggregate.Comment>(x => x.Id == command.Id && !x.Deleted) ?? throw new RecordNotFoundErrorException(404, "Comment not found!");

            comment.Delete(command.GetUserClaimsId()); 

            await _database.Delete(comment);

            await _database.SaveChanges();
            _cache.RemoveGroup(CacheKeys.Comment.ToString(), "");

            return await Ok(DomainOperationResult.Success(comment));
        }
    }
}
