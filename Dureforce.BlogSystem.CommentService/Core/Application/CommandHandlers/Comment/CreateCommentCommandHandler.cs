﻿using Dureforce.Cqrs.Shared.Application.Handlers;
using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Exception;
using Dureforce.Database.Rdbms;
using Dureforce.BlogSystem.CommentService.Core.Domain.Aggregates.CommentAggregate.Commands;
using Dureforce.Cache.Group;
using Dureforce.BlogSystem.CommentService.Core.Shared.Enums;

namespace Dureforce.BlogSystem.CommentService.Core.Application.CommandHandlers.Comment
{
    public class CreateCommentCommandHandler : CommandHandler<CreateCommentCommand>
    {
        private readonly IRdbmsDatabase _database;
        private readonly ICacheGroup _cache;
         
        public CreateCommentCommandHandler(IRdbmsDatabase database, ICacheGroup cache)
        {
            _cache = cache;
            _database = database;
        }

        public async override Task<CommandExecutionResult> Handle(CreateCommentCommand command, CancellationToken cancellationToken)
        {

            if (command == null)
            {
                return await BadRequest($"{typeof(CreateCommentCommand).Name}");
            }

            
            var comment = Domain.Aggregates.CommentAggregate.Comment.Create(command.Body, command.PostId, command.ParentCommentId, command.GetUserClaimsId());


            await _database.Create(comment); 
            await _database.SaveChanges();

            _cache.RemoveGroup(CacheKeys.Comment.ToString(), "");

            return await Ok(DomainOperationResult.Create(comment));
        }
    }
}

