﻿using Dureforce.BlogSystem.CommentService.Core.Application.ReadModels.Comment;


namespace Dureforce.BlogSystem.CommentService.Core.Application.ReadModels.Post
{
    public class PostReadModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Slug { get; set; }
        public string Body { get; set; }
        public Guid CategoryId { get; set; }
        public DateTime? CreateOn { get; set; }
        public string CreateBy { get; set; } 
        public IList<string> Tags { get; set; }
        public IList<CommentReadModel> Comments  { get; set; }
        public PostReadModel()
        { }

        public PostReadModel(Guid id, string title, string slug, string body,Guid categoryId, DateTime? createOn, string createBy, IList<string> tags )
        {
            Id = id;
            Title = title;
            Slug = slug;
            Body = body;
            CategoryId = categoryId;
            CreateOn = createOn;
            CreateBy = createBy;
            Tags = tags;
        }
    }
}
