﻿namespace Dureforce.BlogSystem.CommentService.Core.Application.ReadModels.Comment
{
    public class CommentReadModelList 
    {
        public IList<CommentReadModel> List { get; set; }
        public CommentReadModelList()
        { }
    }
}
