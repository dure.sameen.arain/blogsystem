﻿using Dureforce.BlogSystem.CommentService.Core.Application.ReadModels.Comment;
using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Models;

namespace Dureforce.BlogSystem.CommentService.Core.Application.Queries.Comment
{
    public class GetCommentListQuery : IQuery<ResponseModel<CommentReadModelList>>
    {
        public required Guid PostId { get; set; }
    }
}
