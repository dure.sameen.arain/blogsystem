﻿using Dureforce.BlogSystem.CommentService.Core.Application.ReadModels.Comment;

namespace Dureforce.BlogSystem.CommentService.Core.Application.QueriesHandlers.Comment.Extenstions
{/// <summary>
/// comments extensions
/// </summary>
    public static class CommentExtensions
    { /// <summary>
      /// convert to a single read model
      /// </summary>
      /// <param name="comment"></param>
      /// <returns></returns>
        public static CommentReadModel ToModel(this Domain.Aggregates.CommentAggregate.Comment comment)
        {  
              var commentRealModel= new CommentReadModel(comment.Id, comment.Body , comment.PostId, comment.ParentCommentId, comment.CreatedOn, comment.CreatedBy);
           
            return commentRealModel;
        }
        /// <summary>
        /// Set replies
        /// </summary>
        /// <param name="commentRealModel"></param>
        /// <param name="comments"></param>
        /// <returns></returns>
        public static CommentReadModel SetReplies(this CommentReadModel commentRealModel, IList<CommentReadModel> comments)
        {
            commentRealModel.Replies = commentRealModel.GetReplies(comments);
            return commentRealModel;
        }
        /// <summary>
        /// //recursive call to find all replies
        /// </summary>
        /// <param name="comment"></param>
        /// <param name="comments"></param>
        /// <returns></returns>
        public static List<CommentReadModel> GetReplies(this CommentReadModel comment,IList<CommentReadModel> comments )
        {
            return comments
                    .Where(c => c.ParentCommentId == comment.ParentCommentId)
                    .Select(c => new CommentReadModel
                    {
                        Id = c.Id,
                        Body = c.Body,
                        ParentCommentId = c.ParentCommentId,
                        Replies = c.GetReplies(comments )
                    })
                    .ToList();
        }
    }
}
