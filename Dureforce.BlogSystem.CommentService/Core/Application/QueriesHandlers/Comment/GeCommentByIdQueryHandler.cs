﻿using Dureforce.BlogSystem.CommentService.Core.Application.Queries.Comment;
using Dureforce.BlogSystem.CommentService.Core.Application.QueriesHandlers.Comment.Extenstions;
using Dureforce.BlogSystem.CommentService.Core.Application.ReadModels.Comment;
using Dureforce.BlogSystem.CommentService.Core.Shared.Enums;
using Dureforce.Cache.Handler;
using Dureforce.Cqrs.Shared.Application.Handlers;
using Dureforce.Cqrs.Shared.Enums;
using Dureforce.Cqrs.Shared.Models;
using Dureforce.Cqrs.Shared.Response;
using Dureforce.Database.Rdbms;
using Microsoft.EntityFrameworkCore;
using System.Net;
 

namespace Dureforce.BlogSystem.CommentService.Core.Application.QueriesHandlers.Comment
{/// <summary>
/// get a comment by id
/// </summary>
    public class GetCommentByIdQueryHandler : QueryHandler<GetCommentByIdQuery,CommentReadModel>
    {
        private readonly IRdbmsDatabase _database; 
        private readonly ICacheHandler _cache;
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="database"></param>
        /// <param name="cache"></param>
        public GetCommentByIdQueryHandler(IRdbmsDatabase database, ICacheHandler cache)
        {
            _cache = cache;
            _database = database;
        }
/// <summary>
/// handler
/// </summary>
/// <param name="query"></param>
/// <param name="cancellationToken"></param>
/// <returns></returns>
        public async override Task<ResponseModel<CommentReadModel>> Handle(GetCommentByIdQuery query, CancellationToken cancellationToken)
        {
            HttpStatusCode httpStatusCode = HttpStatusCode.InternalServerError;
            MessageType messageType = MessageType.Error;
            CommentReadModel readModel = default!;
            string errorMessage = "";
            var cacheKey = $"id-{query.Id}";
            CommentReadModel comment = await _cache.GetSetCache(CacheKeys.Comment.ToString(), cacheKey, async () => await GetData(query.Id));

            if (comment == null)
            {
                httpStatusCode = HttpStatusCode.NotFound;
                messageType = MessageType.Info;
                errorMessage = $"Records not found!";
            }
            else
            {
                
                httpStatusCode = HttpStatusCode.OK;
                messageType = MessageType.Success;
            }

            await Task.CompletedTask;

            return ResponseManager.GenerateResponseModel(httpStatusCode, messageType, messageType.ToString(), null, readModel, errorMessage);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<CommentReadModel> GetData(Guid id)
        {
            var comment = await _database.Query<Domain.Aggregates.CommentAggregate.Comment>(p => p.Id == id).FirstOrDefaultAsync();
            if (comment != null)
            {
                var relatedComments = await _database.Query<Domain.Aggregates.CommentAggregate.Comment>(p => p.PostId == comment.PostId).Select (comment=>comment.ToModel()).ToListAsync();
                if (comment == null) return default!;
                var readModel = comment.ToModel();
                readModel.SetReplies(relatedComments);
                return readModel;
            }
            return default!;
        }

    }
}
