﻿using Dureforce.BlogSystem.CommentService.Core.Application.Queries.Comment;
using Dureforce.BlogSystem.CommentService.Core.Application.QueriesHandlers.Comment.Extenstions;
using Dureforce.BlogSystem.CommentService.Core.Application.ReadModels.Comment;
using Dureforce.BlogSystem.CommentService.Core.Shared.Enums;
using Dureforce.Cache.Handler;
using Dureforce.Cqrs.Shared.Application.Handlers;
using Dureforce.Cqrs.Shared.Enums;
using Dureforce.Cqrs.Shared.Models;
using Dureforce.Cqrs.Shared.Response;
using Dureforce.Database.Rdbms;
using Microsoft.EntityFrameworkCore;
using System.Net;

namespace Dureforce.BlogSystem.CommentService.Core.Application.QueriesHandlers.Comment
{/// <summary>
 /// List of all comments
 /// </summary>
    public class GetCommentListQueryHandler : QueryHandler<GetCommentListQuery, CommentReadModelList>
    {
        private readonly IRdbmsDatabase _database;
        private readonly ICacheHandler _cache;
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="database"></param>
        /// <param name="cache"></param>
        public GetCommentListQueryHandler(IRdbmsDatabase database, ICacheHandler cache)
        {
            _database = database;
            _cache = cache;
        }
        /// <summary>
        /// Handler for list of comments
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async override Task<ResponseModel<CommentReadModelList>> Handle(GetCommentListQuery query, CancellationToken cancellationToken)
        {
            HttpStatusCode httpStatusCode = HttpStatusCode.InternalServerError;
            MessageType messageType = MessageType.Error;
            string errorMessage = "";

            var cacheKey = "List";
            CommentReadModelList comments = await _cache.GetSetCache(CacheKeys.Comment.ToString(), cacheKey, async () => await GetData(query.PostId));


            if (comments.List == null || comments.List.Count() == 0)
            {
                httpStatusCode = HttpStatusCode.NotFound;
                messageType = MessageType.Info;
                errorMessage = $"Records not found!";
            }
            else
            {

                httpStatusCode = HttpStatusCode.OK;
                messageType = MessageType.Success;
            }

            return ResponseManager.GenerateResponseModel(httpStatusCode, messageType, messageType.ToString(), null, comments, errorMessage);
        }

        /// <summary>
        /// get data from database and change to dto/readmodel
        /// </summary>
        /// <returns></returns>
        public async Task<CommentReadModelList> GetData(Guid postId)
        {
            var comments = await _database.Query<Domain.Aggregates.CommentAggregate.Comment>(comment => comment.PostId == postId && comment.ParentCommentId == null).Take(10).ToListAsync();
            var listReadModel = comments.Select(p => p.ToModel()).ToList();
            return new CommentReadModelList() { List = listReadModel };
        }

    }


}
 
