﻿using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Command;
using Dureforce.Cqrs.Shared.Domain;

namespace Dureforce.BlogSystem.CommentService.Core.Domain.Aggregates.CommentAggregate.Commands
{
    public class DeleteCommentCommand : BaseCommand, ICommand<CommandExecutionResult>
    {
        public required Guid Id { get; set; }
    }
}
