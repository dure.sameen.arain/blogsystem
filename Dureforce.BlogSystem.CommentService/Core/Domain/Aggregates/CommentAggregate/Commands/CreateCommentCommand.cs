﻿using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Command;
using Dureforce.Cqrs.Shared.Domain;

namespace Dureforce.BlogSystem.CommentService.Core.Domain.Aggregates.CommentAggregate.Commands
{
    public class CreateCommentCommand : BaseCommand, ICommand<CommandExecutionResult> 
    {  
        public required string Body { get; set; }
        public required Guid PostId { get; set; }
        public Guid? ParentCommentId { get; set; }
    }
}
