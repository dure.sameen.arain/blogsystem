﻿using Dureforce.Cqrs.Shared.Domain;
using Dureforce.Database.Rdbms.SqlServer.Model;

namespace Dureforce.BlogSystem.CommentService.Core.Domain.Aggregates.CommentAggregate
{
    [DataTable]
    public class Comment : BaseEntity<Guid>
    { 
        public required string  Body { get; set; }
        public required Guid PostId { get; set; }
        public   Guid?   ParentCommentId { get; set; } 

        public static Comment Create( 
              string body,
              Guid postId,
              Guid? parentCommentId,
              string userCLaimsId 
               )
        {
            Comment comment = new()
            {
                Id = Guid.NewGuid(), 
                Body=  body,
                ParentCommentId= parentCommentId,
                PostId=postId,
                CreatedBy = userCLaimsId,
                CreatedOn = DateTime.Now,
                 
            };

            return comment;
        }

         


    }
}
