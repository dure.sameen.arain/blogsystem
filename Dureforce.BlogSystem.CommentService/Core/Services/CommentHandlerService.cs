﻿using Dureforce.BlogSystem.CommentService.Core.Application.QueriesHandlers;
using Dureforce.BlogSystem.CommentService.Core.Application.ReadModels.Post;
using Dureforce.BlogSystem.CommentService.Core.Shared.Enums;
using Microsoft.Azure.ServiceBus;
using Newtonsoft.Json;
using System.Text;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using System.Threading;
using Dureforce.Cqrs.Shared.Application.Execution;
using Dureforce.BlogSystem.CommentService.Core.Application.Queries.Comment;

namespace Dureforce.BlogSystem.CommentService.Core.Services
{/// <summary>
/// Background service
/// </summary>
    public class CommentHandlerService : BackgroundService
    { 
        private readonly string? _connectionstring;
        private IQueueClient _requestCommentsQueueClient;
        private ITopicClient _responseCommentsTopicClient;
        private readonly IServiceProvider _serviceProvider;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="serviceProvider"></param>
        public CommentHandlerService(IConfiguration configuration, IServiceProvider serviceProvider)
        {
            _connectionstring = configuration.GetValue<string>("defaultAppSettings:AzureServiceBusConnectionString");
            _serviceProvider = serviceProvider;
            _requestCommentsQueueClient = new QueueClient(_connectionstring, MessageBrokerConstants.RequestCommentsQueue);
            _responseCommentsTopicClient = new TopicClient(_connectionstring, MessageBrokerConstants.ResponseCommentsTopic);
            
        }
        /// <summary>
        /// Handle
        /// </summary>
        /// <param name="message"></param>
        /// <param name="cancelToken"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public async Task Handle(Message message, CancellationToken cancelToken)
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));

            var post = Encoding.UTF8.GetString(message.Body);
            Console.WriteLine($"reuqested post is: {post}");
            var postObject = JsonConvert.DeserializeObject<PostReadModel>(post);
            if (postObject != null)
            {
                using (IServiceScope scope = _serviceProvider.CreateScope())
                {
                    IQueryExecutor queryExecutor =
                    scope.ServiceProvider.GetRequiredService<IQueryExecutor>();
                    GetCommentListQuery query = new GetCommentListQuery() { PostId = postObject.Id };
                    var comments = await queryExecutor.Execute(query,  CancellationToken.None);
                    var json = JsonConvert.SerializeObject(comments);
                    byte[] bytes = Encoding.ASCII.GetBytes(json);
                    Message serviceBusMessage = new(bytes)
                    {
                        Label = $"comments-{postObject.Id.ToString().ToLower().Replace("-", "")}"
                    };

                    await _responseCommentsTopicClient.SendAsync(serviceBusMessage);
                }
            }
            await _requestCommentsQueueClient.CompleteAsync(message.SystemProperties.LockToken).ConfigureAwait(false);
        }
        /// <summary>
        /// Failure
        /// </summary>
        /// <param name="exceptionReceivedEventArgs"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public virtual Task HandleFailureMessage(ExceptionReceivedEventArgs exceptionReceivedEventArgs)
        {
            if (exceptionReceivedEventArgs == null)
                throw new ArgumentNullException(nameof(exceptionReceivedEventArgs));
            return Task.CompletedTask;
        }
        /// <summary>
        /// exec
        /// </summary>
        /// <param name="stoppingToken"></param>
        /// <returns></returns>
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var messageHandlerOptions = new MessageHandlerOptions(HandleFailureMessage)
            {
                MaxConcurrentCalls = 5,
                AutoComplete = false,
                MaxAutoRenewDuration = TimeSpan.FromMinutes(10)
            };

            _requestCommentsQueueClient.RegisterMessageHandler(Handle, messageHandlerOptions);
            Console.WriteLine($"{nameof(CommentHandlerService)} service has started.");
            return Task.CompletedTask;
        }
        /// <summary>
        /// stop
        /// </summary>
        /// <param name="stoppingToken"></param>
        /// <returns></returns>
        public override async Task StopAsync(CancellationToken stoppingToken)
        {
            Console.WriteLine($"{nameof(CommentHandlerService)} service has stopped.");
            await _requestCommentsQueueClient.CloseAsync().ConfigureAwait(false);
            await _responseCommentsTopicClient.CloseAsync().ConfigureAwait(false);
        }

    }
}
