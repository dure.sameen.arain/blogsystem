﻿namespace Dureforce.BlogSystem.CommentService.Core.Shared.Enums
{/// <summary>
/// Cache keys for redis cache
/// </summary>
    public enum CacheKeys
    { 
        /// <summary>
        /// All  keys related comment
        /// </summary>
        Comment,
        
    }

}
