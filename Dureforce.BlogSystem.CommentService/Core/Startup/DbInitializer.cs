﻿using Dureforce.Database.Rdbms.SqlServer.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Dureforce.BlogSystem.CommentService.Core.Startup
{
    public class DbInitializer: IDbInitializer
    {
        public DbInitializer (string schema)
        {
            Schema = schema;
        }

        public string Schema { get; set; } 
        public void Initialize(DbContext dbContext)
        {
            ArgumentNullException.ThrowIfNull(dbContext, nameof(dbContext));
            dbContext.Database.EnsureCreated();
            dbContext.Database.GetConnectionString();
            ///create text schema if schema is not found
            ///create three tables and delete if exists before
            
            dbContext.SaveChanges();
        }
    }
}
