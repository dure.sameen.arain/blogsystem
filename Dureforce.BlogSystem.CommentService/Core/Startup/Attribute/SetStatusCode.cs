﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Dureforce.BlogSystem.CommentService.Core.Startup.Attribute
{
    public class SetStatusCode : ActionFilterAttribute

    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {

        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {

            if (filterContext.Result is ObjectResult)
            {
                ObjectResult result = (ObjectResult)filterContext.Result;
                var response = JsonConvert.SerializeObject(result.Value);

                if (!string.IsNullOrEmpty(response))
                {
                    var jsonObject = JObject.Parse(response);
                    if (jsonObject != null && jsonObject["StatusCode"] != null)
                        filterContext.HttpContext.Response.StatusCode = (int)jsonObject["StatusCode"];

                }
            }

        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {

        }




    }
}
