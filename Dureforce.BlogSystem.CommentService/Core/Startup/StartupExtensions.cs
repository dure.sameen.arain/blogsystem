﻿using Dureforce.BlogSystem.CommentService.Core.Domain.Aggregates.CommentAggregate;
using Dureforce.Cache.Configuration;
using Dureforce.Cache.Redis;
using Dureforce.Cache.Redis.Configuration;
using Dureforce.Cqrs.Shared.Enums;
using Dureforce.Cqrs.Shared.Extension;
using Dureforce.Database.Rdbms.SqlServer.Extensions;
using Dureforce.Database.Rdbms.SqlServer.Interfaces;
using FluentValidation.AspNetCore;
 

namespace Dureforce.BlogSystem.CommentService.Core.Startup
{
    public static class StartupExtensions
    {
        public static IServiceCollection StartupConfigureServices(this IServiceCollection services, IConfiguration configuration, ProjectType project)
        {
            string schema = (project == ProjectType.Api) ? "dbo" : "test";
            IDbInitializer dbInitializer=default!;
            if (project == ProjectType.UnitTest)
                dbInitializer= new DbInitializer(schema);

            services.UseSqlServerDatabase(configuration.GetSection("defaultAppSettings:ConnectionString").Value, typeof(Comment), schema, dbInitializer);
           
            services.AddModule(new CoreModule());
            services.AddMediatR(config=> config.RegisterServicesFromAssembly(typeof(Comment).Assembly));
            services.AddFluentValidationAutoValidation();
            services.AddFluentValidationClientsideAdapters();
           
            return services;
        }
        public static IServiceCollection AddModule(this IServiceCollection services, IModule module)
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));
            if (module == null)
                throw new ArgumentNullException(nameof(module));
            module.Load(services);
            return services;
        }
        public static IServiceCollection AddRedisCache(this IServiceCollection services, IConfiguration options)
            {
                var cacheOptions = options.GetSection("CacheOptions").Get<CacheOptions>();
                var config = options.GetSection("RedisConfiguration").Get<RedisConfiguration>();
                if (cacheOptions !=null && config !=null)
                    services.AddRedisCaching(config, cacheOptions);

                return services;
            }
        
    }

}
