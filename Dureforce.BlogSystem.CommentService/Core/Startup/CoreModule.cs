﻿using Dureforce.BlogSystem.CommentService.Core.Application.QueriesHandlers;
using Dureforce.Cqrs.Shared.Application.Execution;
using Dureforce.Cqrs.Shared.Extension;

namespace Dureforce.BlogSystem.CommentService.Core.Startup
{/// <summary>
/// core
/// </summary>
    public class CoreModule : IModule
    {/// <summary>
    /// load
    /// </summary>
    /// <param name="services"></param>
        public void Load(IServiceCollection services)
        {
            services.AddScoped<ICommandExecutor, CommandExecutor>();
            services.AddScoped<IQueryExecutor, QueryExecutor>();
            
        }
    }
}
