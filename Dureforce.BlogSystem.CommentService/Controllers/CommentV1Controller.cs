using Dureforce.BlogSystem.CommentService.Core.Application.Queries.Comment;
using Dureforce.BlogSystem.CommentService.Core.Application.ReadModels.Comment;
using Dureforce.BlogSystem.CommentService.Core.Domain.Aggregates.CommentAggregate.Commands;
using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Application.Execution;
using Dureforce.Cqrs.Shared.Extension;
using Dureforce.Cqrs.Shared.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Identity.Web;
 

namespace Dureforce.BlogSystem.Controllers
{

    [ApiController]
    [Route("api/v{ver:apiVersion}/comment")]
    [AuthorizeForScopes(Scopes = new[] { "api://179abee1-6f82-4ebb-ad52-08874a8bcc0d/ManageComments" })]
    public class CommentV1Controller : ControllerBase
    {

        private readonly ILogger<CommentV1Controller> _logger;
        private readonly ICommandExecutor _commandExecutor;
        private readonly IQueryExecutor _queryExecutor;
  
        public CommentV1Controller(ILogger<CommentV1Controller> logger, ICommandExecutor commandExecutor, IQueryExecutor queryExecutor)
        {
            _commandExecutor = commandExecutor;
            _queryExecutor = queryExecutor;
            _logger = logger;
        }

        /// <summary>
        /// Get list of all comments
        /// </summary> 
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet("list")]
        public async Task<ResponseModel<CommentReadModelList>> GetList([FromQuery] GetCommentListQuery query,CancellationToken cancellationToken)
        {
            
            return await _queryExecutor.Execute(query, cancellationToken);
        } 
         
        /// <summary>
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns>CommentReadModel</returns>

        [HttpGet("by-id")]
        public async Task<ResponseModel<CommentReadModel>> GetById([FromQuery] GetCommentByIdQuery query, CancellationToken cancellationToken)
        {
            return await _queryExecutor.Execute(query, cancellationToken);
        }
        /// <summary>
        /// Create
        /// </summary>
        /// <param name="command"></param>
        /// <returns>CommandExecutionResult</returns>
        [HttpPost]
        public async Task<CommandExecutionResult> Create([FromBody] CreateCommentCommand command)
        {
            command.SetUserClaimsId(HttpContext.User.UserName());

            return await _commandExecutor.Execute(command, CancellationToken.None);
        } 

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="command"></param>
        /// <returns>CommandExecutionResult</returns>
        [HttpDelete]
        public async Task<CommandExecutionResult> Delete([FromQuery] DeleteCommentCommand command)
        {
            command.SetUserClaimsId(HttpContext.User.UserName());

            return await _commandExecutor.Execute(command, CancellationToken.None);
        }

    }    
}