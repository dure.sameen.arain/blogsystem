﻿using Bogus;
using Dureforce.BlogSystem.CommentService.Core.Application.CommandHandlers.Comment;
using Dureforce.BlogSystem.CommentService.Core.Domain.Aggregates.CommentAggregate;
using Dureforce.BlogSystem.CommentService.Core.Domain.Aggregates.CommentAggregate.Commands;
using Dureforce.Cache.Group;
using Dureforce.Cqrs.Shared.Exception;
using Dureforce.Database.Rdbms;
using Moq;
using System;
using System.Linq.Expressions;
using System.Net;
using Xunit;

namespace Get.Caa.IntegrationSetting.Test.Unit
{
    [Trait("Category", "Unit")]
    public class CommentTest
    {
        public Mock<IRdbmsDatabase> _databaseMock;
        public CreateCommentCommandHandler _sut; 
        public DeleteCommentCommandHandler _deleteSut; 
        public Mock<ICacheGroup> _cacheMock;
        private readonly Randomizer randomizer;
        public CommentTest()
        {
            _databaseMock = new Mock<IRdbmsDatabase>();
            _cacheMock = new Mock<ICacheGroup>();
            _cacheMock.Setup(_ => _.RemoveGroup(It.IsAny<string>(),null)
                 );
            _sut = new CreateCommentCommandHandler(_databaseMock.Object,   _cacheMock.Object); 
            _deleteSut = new DeleteCommentCommandHandler(_databaseMock.Object,  _cacheMock.Object);
            randomizer = new Randomizer();
        }

        [Fact]
        public async void CreateCommentCommandHandler_ValidComment_ReturnSuccessResult()
        {
            //Arrange
            var command = new Faker<CreateCommentCommand>()
                .RuleFor(x => x.Body, e => randomizer.String2(8))
                .Generate();
            var entity = new Faker<Comment>().Generate();

            _databaseMock.Setup(x => x.Find(It.IsAny<Expression<Func<Comment, bool>>>()));
            _databaseMock.Setup(x => x.Create(It.IsAny<Comment>()));
            _databaseMock.Setup(x => x.SaveChanges());

            //Act
            var result = await _sut.Handle(command, default);

            //Assert
            Assert.Null(result.Errors);
            Assert.NotNull(result.Result);
            Assert.NotNull(result.Message);
            Assert.True(result.StatusCode == HttpStatusCode.OK);

            _databaseMock.Verify(x => x.Find(It.IsAny<Expression<Func<Comment, bool>>>()), Times.Once);
            _databaseMock.Verify(x => x.Create(It.IsAny<Comment>()), Times.Once);
            _databaseMock.Verify(x => x.SaveChanges(), Times.Once);
             
        }

        [Fact]
        public async void CreateCommentCommandHandler_NullComment_ReturnFailResult()
        {
            //Arrange
            _databaseMock.Setup(x => x.Create(It.IsAny<Comment>()));

            //Act
            var result = await _sut.Handle(null, default);

            //Assert
            Assert.NotNull(result.Errors);
            Assert.Null(result.Result);
            Assert.Equal("Error", result.Message);
            Assert.True(result.StatusCode == HttpStatusCode.BadRequest);
            _databaseMock.Verify(x => x.Create(It.IsAny<Comment>()), Times.Never);
            
        }

        
         

        [Fact]
        public async void DeleteCommentCommandHandler_CommentFound_ReturnSuccessResult()
        {
            //Arange
            var command = new Faker<DeleteCommentCommand>()
                .RuleFor(x => x.Id, g => randomizer.Guid())
                .Generate();
            var entity = new Faker<Comment>().Generate();
            _databaseMock.Setup(x => x.Find(It.IsAny<Expression<Func<Comment, bool>>>())).ReturnsAsync(entity);
            
            _databaseMock.Setup(x => x.SaveChanges());

            //Act
            var result = await _deleteSut.Handle(command, default);

            //Asert
            Assert.Null(result.Errors);
            Assert.NotNull(result.Result);
            Assert.Equal("Success", result.Message);
            Assert.True(HttpStatusCode.OK == result.StatusCode);

            _databaseMock.Verify(x => x.Find(It.IsAny<Expression<Func<Comment, bool>>>()), Times.Once);
            _databaseMock.Verify(x => x.SaveChanges(), Times.Once);
            
        }

        [Fact]
        public async void DeleteCommentCommandHandler_CommenttNotFound_ReturnFailResult()
        {
            //Arange
            var command = new Faker<DeleteCommentCommand>()
                .RuleFor(x => x.Id, g => randomizer.Guid())
                .Generate();

            _databaseMock.Setup(x => x.Find(It.IsAny<Expression<Func<Comment, bool>>>()));

             
            //Asert 
            await Assert.ThrowsAsync<RecordNotFoundErrorException>(async () => await _deleteSut.Handle(command, default));
             
            _databaseMock.Verify(x => x.Find(It.IsAny<Expression<Func<Comment, bool>>>()), Times.Once);
            _databaseMock.Verify(x => x.SaveChanges(), Times.Never);
            
        }

         
    }
}
