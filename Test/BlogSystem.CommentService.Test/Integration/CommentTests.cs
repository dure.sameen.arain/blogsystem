﻿using Bogus;
using Dureforce.BlogSystem.CommentService.Core.Application.ReadModels.Comment;
using Dureforce.BlogSystem.CommentService.Core.Domain.Aggregates.CommentAggregate.Commands;
using Dureforce.Cqrs.Shared.Exception;
using Get.Caa.IntegrationSetting.Test.Integration.Clients;
using Get.Caa.IntegrationSetting.Test.Integration.Fixtures;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Refit;
using System;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace Get.Caa.IntegrationSetting.Test.Integration
{
    [Collection(WebCollection.Collection)]
    [Trait("Category", "Integration")]
    public class CommentTests
    {
        private readonly ICommentClient _httpClient;
        private readonly Randomizer randomizer;

        public CommentTests(WebFixture fixture)
        {
            _httpClient = RestService.For<ICommentClient>(fixture.Client, new RefitSettings
            {
                ContentSerializer = new NewtonsoftJsonContentSerializer(
                    new JsonSerializerSettings
                    {
                        ContractResolver = new CamelCasePropertyNamesContractResolver()
                    })
            });
            randomizer = new Randomizer();
        }

        [Fact]
        public async Task GetAsync_ExistData_ReturnsOk()
        { //Arrange
            var command = CreateCommentCommand(randomizer.String2(7), Guid.NewGuid(), Guid.NewGuid());
            //Act
            await _httpClient.Post(command); 
             var apiResponse = await _httpClient.Get();
            var result = apiResponse.Result;

            //Assert
            Assert.Equal(HttpStatusCode.OK, apiResponse.StatusCode);
            Assert.NotNull(result);
            Assert.Null(apiResponse.Errors);
        }

        [Fact]
        public async Task PostAsync_ValidComment_ReturnsOk()
        {
            //Arrange
            var command = CreateCommentCommand(randomizer.String2(7), Guid.NewGuid(), Guid.NewGuid());

            //Act
            var apiResponse = await _httpClient.Post(command);

            string serializerResponse = JsonConvert.SerializeObject(apiResponse.Result);
            var objectResponse = JsonConvert.DeserializeObject<CommentReadModel>(serializerResponse);

            var deleteResponse = await _httpClient.Delete(DeleteCommentCommand(objectResponse.Id));

            //Assert
            Assert.Equal(HttpStatusCode.OK, apiResponse.StatusCode);
            Assert.Equal(HttpStatusCode.OK, deleteResponse.StatusCode);
            Assert.Null(apiResponse.Errors);
            Assert.NotNull(objectResponse);
            Assert.Equal(command.Body, objectResponse.Body);
        }


        [Fact]
        public async Task PostAsync_CommentExist_ReturnsConflict()
        {
            //Arrange
            var command = CreateCommentCommand(randomizer.String2(7), Guid.NewGuid(), Guid.NewGuid());

            //Act
            var apiResponse = await _httpClient.Post(command);
            //Assert
            await Assert.ThrowsAsync<RecordExistErrorException>(async () => await  _httpClient.Post(command));
            
        }

        

        [Fact]

        public async Task DeleteAsync_CommentNotFound_ReturnsNotFound()
        {

            //Arrange
            var command = new DeleteCommentCommand { Id = randomizer.Guid() };

            //Assert
            await Assert.ThrowsAsync<RecordNotFoundErrorException > (async () => await _httpClient.Delete(command));



        }

        private CreateCommentCommand CreateCommentCommand( string body, Guid postId,Guid? parentCommentId)
        {
            return new CreateCommentCommand() {   Body = body, PostId=postId, ParentCommentId= parentCommentId }   ;
        }
        
        private DeleteCommentCommand DeleteCommentCommand(Guid id)
        {
            return new DeleteCommentCommand { Id = id };
        }
    }
}
