﻿using Dureforce.BlogSystem.Controllers;
using Dureforce.Cache.Configuration;
using Dureforce.Cache.Distributed;
using Get.Caa.IntegrationSetting.Test.Integration.Fake;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Dureforce.Cache;
using Dureforce.BlogSystem.CommentService.Core.Startup;
using Dureforce.Cqrs.Shared.Enums;
using Dureforce.Cache.Group;
using Dureforce.BlogSystem.CommentService.Core.Startup.Attribute;

namespace Get.Caa.IntegrationSetting.Test.Integration.Internal
{
    internal class Startup
    {
        public Startup(IConfiguration configuration) => Configuration = configuration;

        private IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {   
            services.StartupConfigureServices( Configuration, ProjectType.UnitTest);

            services.AddControllers(options => { options.Filters.Add<SetStatusCode>(); }) 
                    .AddApplicationPart(typeof(CommentV1Controller).Assembly);



            services.AddApiVersioning();
            CacheOptions cacheOptions=new() {  Enabled  = false };
            services.AddCaching(cacheOptions);
            services.AddSingleton<IDistributedCache, FakeRedisCache>( );
             

        }


        public void Configure(IApplicationBuilder app)
        {
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers().AllowAnonymous();
            });
        }
    }
}
