﻿using Dureforce.BlogSystem.CommentService.Core.Application.Queries.Comment;
using Dureforce.BlogSystem.CommentService.Core.Application.ReadModels.Comment;
using Dureforce.BlogSystem.CommentService.Core.Domain.Aggregates.CommentAggregate.Commands;
using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Models;
using Refit;
using System.Threading.Tasks;

namespace Get.Caa.IntegrationSetting.Test.Integration.Clients;

public interface ICommentClient
{
    [Get("/api/v1/comment/list")]
    Task<ResponseModel<CommentReadModelList>> Get();

    [Get("/api/v1/comment/by-id")]
    Task<ResponseModel<CommentReadModel>> GetById(GetCommentByIdQuery query);

    [Post("/api/v1/comment/")]
    Task<CommandExecutionResult> Post(CreateCommentCommand command); 
    
    [Delete("/api/v1/comment/")]
    Task<CommandExecutionResult> Delete(DeleteCommentCommand command);
}
