﻿using Bogus;
using Bogus.Bson;
using Dureforce.BlogSystem.PostService.Core.Application.CommandHandlers.Post;
using Dureforce.BlogSystem.PostService.Core.Domain.Aggregates.PostAggregate;
using Dureforce.BlogSystem.PostService.Core.Domain.Aggregates.PostAggregate.Commands;
using Dureforce.Cache;
using Dureforce.Cache.Group;
using Dureforce.Cqrs.Shared.Exception;
using Dureforce.Database.Rdbms;
using Moq;
using System;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using Xunit;

namespace Get.Caa.IntegrationSetting.Test.Unit
{
    [Trait("Category", "Unit")]
    public class PostTest
    {
        public Mock<IRdbmsDatabase> _databaseMock;
        public CreatePostCommandHandler _sut;
        public ChangePostCommandHandler _changeSut;
        public DeletePostCommandHandler _deleteSut; 
        public Mock<ICacheGroup> _cacheMock;
        private readonly Randomizer randomizer;
        public PostTest()
        {
            _databaseMock = new Mock<IRdbmsDatabase>();
            _cacheMock = new Mock<ICacheGroup>();
            _cacheMock.Setup(_ => _.RemoveGroup(It.IsAny<string>(),null)
                 );
            _sut = new CreatePostCommandHandler(_databaseMock.Object,   _cacheMock.Object);
            _changeSut = new ChangePostCommandHandler(_databaseMock.Object,  _cacheMock.Object);
            _deleteSut = new DeletePostCommandHandler(_databaseMock.Object,  _cacheMock.Object);
            randomizer = new Randomizer();
        }

        [Fact]
        public async void CreatePostCommandHandler_ValidPost_ReturnSuccessResult()
        {
            //Arrange
            var command = new Faker<CreatePostCommand>()
                .RuleFor(x => x.Title, e => randomizer.String2(8))
                .Generate();
            var entity = new Faker<Post>().Generate();

            _databaseMock.Setup(x => x.Find(It.IsAny<Expression<Func<Post, bool>>>()));
            _databaseMock.Setup(x => x.Create(It.IsAny<Post>()));
            _databaseMock.Setup(x => x.SaveChanges());

            //Act
            var result = await _sut.Handle(command, default);

            //Assert
            Assert.Null(result.Errors);
            Assert.NotNull(result.Result);
            Assert.NotNull(result.Message);
            Assert.True(result.StatusCode == HttpStatusCode.OK);

            _databaseMock.Verify(x => x.Find(It.IsAny<Expression<Func<Post, bool>>>()), Times.Once);
            _databaseMock.Verify(x => x.Create(It.IsAny<Post>()), Times.Once);
            _databaseMock.Verify(x => x.SaveChanges(), Times.Once);
             
        }

        [Fact]
        public async void CreatePostCommandHandler_NullPost_ReturnFailResult()
        {
            //Arrange
            _databaseMock.Setup(x => x.Create(It.IsAny<Post>()));

            //Act
            var result = await _sut.Handle(null, default);

            //Assert
            Assert.NotNull(result.Errors);
            Assert.Null(result.Result);
            Assert.Equal("Error", result.Message);
            Assert.True(result.StatusCode == HttpStatusCode.BadRequest);
            _databaseMock.Verify(x => x.Create(It.IsAny<Post>()), Times.Never);
            
        }

        [Fact]
        public async void ChangePostCommandHandler_ValidPost_ReturnSuccessResult()
        {
            //Arrange
            var command = new Faker<ChangePostCommand>()
                .RuleFor(x => x.Id, g => randomizer.Guid())
                .RuleFor(x => x.Title, g => randomizer.String2(2))
                .Generate();
            var entity = new Faker<Post>().Generate();

            _databaseMock.SetupSequence(x => x.Find(It.IsAny<Expression<Func<Post, bool>>>()))
                .ReturnsAsync(entity)
                .ReturnsAsync((Post)null);
            _databaseMock.Setup(x => x.SaveChanges());

            //Act
            var result = await _changeSut.Handle(command, default);

            //Assert
            Assert.Null(result.Errors);
            Assert.NotNull(result.Result);
            Assert.Equal("Success", result.Message);
            Assert.True(HttpStatusCode.OK == result.StatusCode);
           
             
        }

        [Fact]
        public async void ChangePostCommandHandler_PostNotFound_ReturnFailResult()
        {
            //Arange
            var command = new Faker<ChangePostCommand>()
                .RuleFor(x => x.Id, g => randomizer.Guid())
                .Generate();

            _databaseMock.Setup(x => x.Find(It.IsAny<Expression<Func<Post, bool>>>()));

            //Asert
     
            await Assert.ThrowsAsync<RecordNotFoundErrorException>(async () => await _changeSut.Handle(command, default));
             
            _databaseMock.Verify(x => x.Find(It.IsAny<Expression<Func<Post, bool>>>()), Times.Once);
            _databaseMock.Verify(x => x.SaveChanges(), Times.Never);
           
        }

        [Fact]
        public async void DeletePostCommandHandler_PostFound_ReturnSuccessResult()
        {
            //Arange
            var command = new Faker<DeletePostCommand>()
                .RuleFor(x => x.Id, g => randomizer.Guid())
                .Generate();
            var entity = new Faker<Post>().Generate();
            _databaseMock.Setup(x => x.Find(It.IsAny<Expression<Func<Post, bool>>>())).ReturnsAsync(entity);
            
            _databaseMock.Setup(x => x.SaveChanges());

            //Act
            var result = await _deleteSut.Handle(command, default);

            //Asert
            Assert.Null(result.Errors);
            Assert.NotNull(result.Result);
            Assert.Equal("Success", result.Message);
            Assert.True(HttpStatusCode.OK == result.StatusCode);

            _databaseMock.Verify(x => x.Find(It.IsAny<Expression<Func<Post, bool>>>()), Times.Once);
            _databaseMock.Verify(x => x.SaveChanges(), Times.Once);
            
        }

        [Fact]
        public async void DeletePostCommandHandler_PosttNotFound_ReturnFailResult()
        {
            //Arange
            var command = new Faker<DeletePostCommand>()
                .RuleFor(x => x.Id, g => randomizer.Guid())
                .Generate();

            _databaseMock.Setup(x => x.Find(It.IsAny<Expression<Func<Post, bool>>>()));

             
            //Asert 
            await Assert.ThrowsAsync<RecordNotFoundErrorException>(async () => await _deleteSut.Handle(command, default));
             
            _databaseMock.Verify(x => x.Find(It.IsAny<Expression<Func<Post, bool>>>()), Times.Once);
            _databaseMock.Verify(x => x.SaveChanges(), Times.Never);
            
        }

         
    }
}
