﻿using Bogus;
using Dureforce.BlogSystem.PostService.Core.Application.Queries.Post;
using Dureforce.BlogSystem.PostService.Core.Application.ReadModels.Post;
using Dureforce.BlogSystem.PostService.Core.Domain.Aggregates.PostAggregate.Commands;
using Dureforce.Cqrs.Shared.Errors;
using Dureforce.Cqrs.Shared.Exception;
using Get.Caa.IntegrationSetting.Test.Integration.Clients;
using Get.Caa.IntegrationSetting.Test.Integration.Fixtures;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Refit;
using System;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace Get.Caa.IntegrationSetting.Test.Integration
{
    [Collection(WebCollection.Collection)]
    [Trait("Category", "Integration")]
    public class PostTests
    {
        private readonly IPostClient _httpClient;
        private readonly Randomizer randomizer;

        public PostTests(WebFixture fixture)
        {
            _httpClient = RestService.For<IPostClient>(fixture.Client, new RefitSettings
            {
                ContentSerializer = new NewtonsoftJsonContentSerializer(
                    new JsonSerializerSettings
                    {
                        ContractResolver = new CamelCasePropertyNamesContractResolver()
                    })
            });
            randomizer = new Randomizer();
        }

        [Fact]
        public async Task GetAsync_ExistData_ReturnsOk()
        { //Arrange
            var command = CreatePostCommand(randomizer.String2(7), randomizer.String2(100), Guid.NewGuid());
            //Act
            await _httpClient.Post(command); 
             var apiResponse = await _httpClient.Get();
            var result = apiResponse.Result;

            //Assert
            Assert.Equal(HttpStatusCode.OK, apiResponse.StatusCode);
            Assert.NotNull(result);
            Assert.Null(apiResponse.Errors);
        }

        [Fact]
        public async Task PostAsync_ValidPost_ReturnsOk()
        {
            //Arrange
            var command = CreatePostCommand(randomizer.String2(7), randomizer.String2(100), Guid.NewGuid());

            //Act
            var apiResponse = await _httpClient.Post(command);

            string serializerResponse = JsonConvert.SerializeObject(apiResponse.Result);
            var objectResponse = JsonConvert.DeserializeObject<PostReadModel>(serializerResponse);

            var deleteResponse = await _httpClient.Delete(DeletePostCommand(objectResponse.Id));

            //Assert
            Assert.Equal(HttpStatusCode.OK, apiResponse.StatusCode);
            Assert.Equal(HttpStatusCode.OK, deleteResponse.StatusCode);
            Assert.Null(apiResponse.Errors);
            Assert.NotNull(objectResponse);
            Assert.Equal(command.Title, objectResponse.Title);
        }


        [Fact]
        public async Task PostAsync_PostExist_ReturnsConflict()
        {
            //Arrange
            var command = CreatePostCommand(randomizer.String2(7), randomizer.String2(100), Guid.NewGuid());

            //Act
            var apiResponse = await _httpClient.Post(command);
            //Assert
            await Assert.ThrowsAsync<RecordExistErrorException>(async () => await  _httpClient.Post(command));
            
        }

        [Fact]
        public async Task UpdateAsync_ValidPost_ReturnsOk()
        {
            //Arrange
            var command = CreatePostCommand(randomizer.String2(7), randomizer.String2(100), Guid.NewGuid());
            var name = randomizer.String2(10);

            //Act
            var apiResponse = await _httpClient.Post(command);

            string serializerResponse = JsonConvert.SerializeObject(apiResponse.Result);
            var objectResponse = JsonConvert.DeserializeObject<PostReadModel>(serializerResponse);

            var updateResponse = await _httpClient.Put(ChangePostCommand(objectResponse.Id, name, "test_body", Guid.NewGuid()));
            var deleteResponse = await _httpClient.Delete(DeletePostCommand(objectResponse.Id));

            //Assert
            Assert.Equal(HttpStatusCode.OK, apiResponse.StatusCode);
            Assert.Equal(HttpStatusCode.OK, updateResponse.StatusCode);
            Assert.Equal(HttpStatusCode.OK, deleteResponse.StatusCode);
            Assert.Null(apiResponse.Errors);
            Assert.NotNull(objectResponse);
            Assert.Equal(command.Title, objectResponse.Title);
        }

        [Fact]
        public async Task UpdateAsync_PostNotFound_ReturnsNotFound()
        {
            //Arrange
            var command = CreatePostCommand(randomizer.String2(7), randomizer.String2(100), Guid.NewGuid());
            var id = randomizer.Guid();

            //Act
            var apiResponse = await _httpClient.Post(command);
            //Assert
            await Assert.ThrowsAsync<RecordNotFoundErrorException>(async () => await _httpClient.Put(ChangePostCommand(id, "test_title", "test_body", Guid.NewGuid())));
            Assert.Equal(HttpStatusCode.OK, apiResponse.StatusCode);
     
        } 

        [Fact]

        public async Task DeleteAsync_PostNotFound_ReturnsNotFound()
        {

            //Arrange
            var command = new DeletePostCommand { Id = randomizer.Guid() };

            //Assert
            await Assert.ThrowsAsync<RecordNotFoundErrorException > (async () => await _httpClient.Delete(command));



        }

        private CreatePostCommand CreatePostCommand(string title,string body, Guid categoryId)
        {
            return new CreatePostCommand() { Title = title, Body = body, CategoryId = categoryId }   ;
        }
        private ChangePostCommand ChangePostCommand(Guid id, string title, string body, Guid categoryId)
        {
            return new ChangePostCommand( ) { Id=id, Title = title, Body = body, CategoryId = categoryId }; ;
        }
        private DeletePostCommand DeletePostCommand(Guid id)
        {
            return new DeletePostCommand { Id = id };
        }
    }
}
