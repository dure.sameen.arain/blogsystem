﻿using Dureforce.Cache.Configuration;
using Dureforce.Cache.Distributed;
using System.Threading;
using System.Threading.Tasks;

namespace Get.Caa.IntegrationSetting.Test.Integration.Fake
{
    public class FakeRedisCache : IDistributedCache 
    {
        public byte[] Get(string key)
        {
            return new byte[3] ;
        }

        public Task<byte[]> GetAsync(string key, CancellationToken token = default)
        {
            return   Task.FromResult(new byte[3]);
        }

        public void Refresh(string key)
        {
             
        }

        public Task RefreshAsync(string key, CancellationToken token = default)
        {
            return Task.FromResult("");
        }

        public void Remove(string key)
        {
             
        }

        public Task RemoveAsync(string key, CancellationToken token = default)
        {
            return Task.FromResult("");

        }

        public void Set(string key, byte[] value, DefaultCachePolicyOptions options)
        {
             
        } 

        public async Task  SetAsync(string key, byte[] value, DefaultCachePolicyOptions options, CancellationToken token = default)
        {
              await Task.CompletedTask;
        }
    }
}
