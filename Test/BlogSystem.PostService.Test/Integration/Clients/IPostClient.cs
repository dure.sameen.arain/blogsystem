﻿using Dureforce.BlogSystem.PostService.Core.Application.Queries.Post;
using Dureforce.BlogSystem.PostService.Core.Application.ReadModels.Post;
using Dureforce.BlogSystem.PostService.Core.Domain.Aggregates.PostAggregate.Commands;
using Dureforce.Cqrs.Shared.Application;
using Dureforce.Cqrs.Shared.Models;
using Refit;
using System.Threading.Tasks;

namespace Get.Caa.IntegrationSetting.Test.Integration.Clients
{
    public interface IPostClient
    {
        [Get("/api/v1/post/list")]
        Task<ResponseModel<PostReadModelList>> Get();

        [Get("/api/v1/post/by-id")]
        Task<ResponseModel<PostReadModel>> GetById(GetPostByIdQuery query);

        [Post("/api/v1/post/")]
        Task<CommandExecutionResult> Post(CreatePostCommand command);

        [Put("/api/v1/post/")]
        Task<CommandExecutionResult> Put(ChangePostCommand command);

        [Delete("/api/v1/post/")]
        Task<CommandExecutionResult> Delete(DeletePostCommand command);
    }
}
