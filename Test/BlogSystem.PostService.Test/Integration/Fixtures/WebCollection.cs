﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace Get.Caa.IntegrationSetting.Test.Integration.Fixtures
{
    [CollectionDefinition(Collection)]
    public class WebCollection : ICollectionFixture<WebFixture>
    {
        public const string Collection = "InMemory Web collection";
    }
    public class WebFixture : IAsyncLifetime
    {
        internal IHost Host;
        internal IServiceProvider ServiceProvider;
        internal HttpClient Client;
        static WebFixture()
        {
            Configuration = GetConfiguration();

        }

        private static IConfiguration GetConfiguration()
            => new ConfigurationBuilder()
            .AddJsonFile("appsettings.json")
            .Build();


        protected static IConfiguration Configuration { get; }
        public async Task InitializeAsync()
        {
            Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "Test");
            Host = Microsoft.Extensions.Hosting.Host.CreateDefaultBuilder()
                   .ConfigureWebHostDefaults(x =>
                   {
                       x.UseTestServer();
                       x.UseStartup<Internal.Startup>();
                   }).Build();

            await Host.StartAsync();
            ServiceProvider = Host.Services;
            Client = Host.GetTestClient();

        }

        public async Task DisposeAsync()
        {
            Host.Dispose();
            Client.Dispose ();
            GC.SuppressFinalize(this);
            await Task.CompletedTask;
        }
    }
}
