﻿namespace Dureforce.Cqrs.Shared.Configuration
{
	public interface IEnvironmentConfiguration
	{
		public string ActiveEnvironment { get; }
	}
}
