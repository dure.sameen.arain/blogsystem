﻿namespace Dureforce.Cqrs.Shared.Errors
{
	public static class NotFoundError
	{
		public static string Message(string message)
		{
			return $"{message} not found";
		}
	}
}
