﻿namespace Dureforce.Cqrs.Shared.Errors
{
	public static class BadRequestError
	{
		public static string Message(string message)
		{
			return $"BadRequest :{message}";
		}
	}
}
