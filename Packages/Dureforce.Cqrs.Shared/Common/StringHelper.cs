﻿namespace Dureforce.Cqrs.Shared.Common
{
	public static class StringHelper
	{
		public static int GetDeterministicHashCode(string value)
		{
			unchecked
			{
				int hash1 = (5381 << 16) + 5381;
				int hash2 = hash1;
				value = string.IsNullOrEmpty(value) ? string.Empty : value;

				for (int i = 0; i < value.Length; i += 2)
				{
					hash1 = ((hash1 << 5) + hash1) ^ value[i];

					if (i == value.Length - 1)
					{
						break;
					}

					hash2 = ((hash2 << 5) + hash2) ^ value[i + 1];
				}

				return hash1 + (hash2 * 1566083941);
			}
		}

		public static string ToCommaSeparated(IList<string> list)
		{
			return String.Join(",", list);
		}

		/// <summary>
		/// Converts comma separated string values to list.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static List<string> ToList(string value)
		{
			List<string> data = value.Split(",").ToList();

			return data;
		}
	}
}
