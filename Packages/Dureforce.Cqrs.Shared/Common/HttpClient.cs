﻿using Microsoft.AspNetCore.Mvc;


namespace Dureforce.Cqrs.Shared.Common
{
    public class HttpClientHelper
    {
        private HttpClient _httpClient;

        public HttpClientHelper(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<FileStreamResult> GetAsync(string url, StringContent stringContent)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url);
            request.Headers.Add("accept", "*/*");
            request.Content = stringContent;
        
            var response = await _httpClient.SendAsync(request);
            var bytes = await response.Content.ReadAsStreamAsync();
                      
            var contentType = response.Content.Headers.ContentType.MediaType.ToString();
            var filename = response.Content.Headers.ContentDisposition.FileName.ToString();

            FileStreamResult result =  new FileStreamResult(bytes, contentType );
            result.FileDownloadName = filename;

            return result;
        }
    }
}