﻿using SystemException = System.Exception;

namespace Dureforce.Cqrs.Shared.Common
{
	public class GuidHelper
	{
		public static bool ValidGuid(Guid unValidatedGuid)
		{
			try
			{
				if (unValidatedGuid != Guid.Empty)
				{
					if (Guid.TryParse(unValidatedGuid.ToString(), out Guid validatedGuid))
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}

			}
			catch (SystemException)
			{
				throw;
			}
		}
	}
}
