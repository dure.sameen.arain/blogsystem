﻿using RestSharp;

namespace Dureforce.Cqrs.Shared.Common
{
    public class RestHelper
    {
        public static async Task<IRestResponse<T>> PostAsync<T>(string url, string data)
        {
            var client = new RestClient(url);
            var header = "x-api-key";
            client.AddDefaultHeader(header, Environment.GetEnvironmentVariable(header));
            var request = new RestRequest(Method.POST)
            {
                RequestFormat = DataFormat.Json
            };
            request.AddJsonBody(data);
            var responseBody = await client.ExecuteAsync<T>(request);
            return responseBody;
        }

        public static async Task<IRestResponse<T>> GetAsync<T>(string url)
        {
            var client = new RestClient(url);
            var header = "x-api-key";
            client.AddDefaultHeader(header, Environment.GetEnvironmentVariable(header));
            var request = new RestRequest(Method.GET)
            {
                RequestFormat = DataFormat.Json
            };


            var responseBody = await client.ExecuteAsync<T>(request);
            return responseBody;
        }
    }
}
