﻿namespace Dureforce.Cqrs.Shared.Common
{
	public static class Utility
	{
		public static List<int> GeneratePageSizes(List<int> allowedPageSizes, int totalRecords)
		{
			List<int> pageSizes = new List<int>();
			int totalItems = allowedPageSizes.Count - 1;

			for (int itemCounter = 0; itemCounter <= totalItems; itemCounter++)
			{
				int arrayItem = allowedPageSizes[itemCounter];
				int temp = totalRecords;

				while (arrayItem <= temp)
				{
					temp -= arrayItem;

					pageSizes.Add(arrayItem);
				}
			}

			List<int> result = pageSizes.Distinct().ToList();

			result.Add(totalRecords);

			// Lower number condition
			result = result.Distinct().ToList();

			// Upper number condition
			int max = result.Max();

			if (max > 500)
			{
				result.Remove(max);
			}

			return result;
		}
	}
}
