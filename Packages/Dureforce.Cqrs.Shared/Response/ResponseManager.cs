﻿using Dureforce.Cqrs.Shared.Enums;
using Dureforce.Cqrs.Shared.Exception;
using Dureforce.Cqrs.Shared.Models;
using System.Net;

namespace Dureforce.Cqrs.Shared.Response
{
    public static class ResponseManager
    {
        public static ResponseModel<T> GenerateResponseModel<T>(HttpStatusCode httpStatusCode, MessageType messageType, string messageKey, IList<string> sqls, T result, string exceptionMessage = null, int totalRecords = 0)
        {

            ResponseModel<T> responseModel = new ResponseModel<T>
            {
                StatusCode = httpStatusCode,
                MessageType = string.IsNullOrEmpty(exceptionMessage) ? (int)messageType : (int)MessageType.Error,
                Message = string.IsNullOrEmpty(exceptionMessage) ? messageKey : exceptionMessage,
                Result = result,
                SQLs = sqls,
                TotalRecords = totalRecords

            };

            int httpStatus = (int)httpStatusCode;


            if (httpStatus >= 500 && httpStatus < 600)
            {
                throw new ErrorException(httpStatus, exceptionMessage);
            }

            return responseModel;
        }

    }
}
