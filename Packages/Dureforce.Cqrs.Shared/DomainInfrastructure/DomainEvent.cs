﻿namespace Dureforce.Cqrs.Shared.DomainInfrastructure
{
	public abstract class DomainEvent : IDomainEvent
	{
		public Guid Id { get; set; }
		public Guid EventId { get; set; } = Guid.NewGuid();
		public object AggregateRootId { get; set; }
		public DateTime CreatedOn { get; set; }
		public DateTime LastChangedOn { get; set; }
		public DateTime OccuredOn { get; set; }
		public string EventType { get; set; }
	}
}
