﻿using MediatR;

namespace Dureforce.Cqrs.Shared.DomainInfrastructure
{
	public interface IDomainEvent : INotification
	{
	}
}
