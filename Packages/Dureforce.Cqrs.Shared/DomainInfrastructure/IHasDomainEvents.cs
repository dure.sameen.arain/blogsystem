﻿namespace Dureforce.Cqrs.Shared.DomainInfrastructure
{
	public interface IHasDomainEvents
	{
		IReadOnlyList<DomainEvent> UncommittedChanges();
		void MarkChangesAsCommitted();
		void Raise(DomainEvent @event);
	}
}
