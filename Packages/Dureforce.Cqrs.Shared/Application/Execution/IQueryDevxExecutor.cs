﻿namespace Dureforce.Cqrs.Shared.Application.Execution
{
	public interface IQueryDevxExecutor
	{
		Task<TResult> Execute<TResult>(IQuery<TResult> query, CancellationToken cancellationToken);
	}
}
