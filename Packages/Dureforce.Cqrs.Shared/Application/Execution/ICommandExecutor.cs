﻿using Dureforce.Cqrs.Shared.Domain;

namespace Dureforce.Cqrs.Shared.Application.Execution
{
	public interface ICommandExecutor
	{
		Task<CommandExecutionResult> Execute(ICommand<CommandExecutionResult> command, CancellationToken cancellationToken);
	}
}
