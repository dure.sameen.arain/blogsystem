﻿using MediatR;

namespace Dureforce.Cqrs.Shared.Application.Execution
{
	public class QueryDevxExecutor : IQueryDevxExecutor
	{
		private readonly IMediator _mediator;

		public QueryDevxExecutor(IMediator mediator)
		{
			_mediator = mediator;
		}

		public async Task<TResult> Execute<TResult>(IQuery<TResult> query, CancellationToken cancellationToken)
		{
			//try
			//{
			return await _mediator.Send(query, cancellationToken).ConfigureAwait(false);

			//}
			//catch (DomainException exception)
			//{
			//   // return new TResult { Message = exception.Message, MessageType = (int)MessageType.Error, StatusCode = HttpStatusCode.Conflict };
			//}
			//catch (Exception exception)
			//{
			//   // return new ResponseModel<TResult> { Message = exception.Message, MessageType = (int)MessageType.Error, StatusCode = HttpStatusCode.InternalServerError };
			//}
		}
	}
}
