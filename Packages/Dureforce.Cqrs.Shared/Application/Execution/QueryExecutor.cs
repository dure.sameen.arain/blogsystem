﻿using Dureforce.Cqrs.Shared.Models;
using MediatR;

namespace Dureforce.Cqrs.Shared.Application.Execution
{
	public class QueryExecutor : IQueryExecutor
	{
		private readonly IMediator _mediator;

		public QueryExecutor(IMediator mediator)
		{
			_mediator = mediator;
		}

		public async Task<ResponseModel<TResult>> Execute<TResult>(IQuery<ResponseModel<TResult>> query, CancellationToken cancellationToken)
		{
			return await _mediator.Send(query, cancellationToken).ConfigureAwait(false);
		}
	}
}
