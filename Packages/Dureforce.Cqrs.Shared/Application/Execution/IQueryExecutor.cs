﻿
using Dureforce.Cqrs.Shared.Models;

namespace Dureforce.Cqrs.Shared.Application.Execution
{
	public interface IQueryExecutor
	{
		Task<ResponseModel<TResult>> Execute<TResult>(IQuery<ResponseModel<TResult>> query, CancellationToken cancellationToken);
	}
}
