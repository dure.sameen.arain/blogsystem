﻿using Dureforce.Cqrs.Shared.Domain;
using MediatR;

namespace Dureforce.Cqrs.Shared.Application.Execution
{
	public class CommandExecutor : ICommandExecutor
	{
		private readonly IMediator _mediator;

		public CommandExecutor(IMediator mediator)
		{
			_mediator = mediator;
		}

		public async Task<CommandExecutionResult> Execute(ICommand<CommandExecutionResult> command, CancellationToken cancellationToken)
		{
			return await _mediator.Send(command, cancellationToken).ConfigureAwait(false);
		}
	}
}
