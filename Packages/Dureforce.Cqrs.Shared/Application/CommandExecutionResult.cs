﻿using System.Net;

namespace Dureforce.Cqrs.Shared.Application
{
	public class CommandExecutionResult<T>
	{
		public Guid EventId { get; set; } = Guid.NewGuid();
		public HttpStatusCode StatusCode { get; set; }
		public int MessageType { get; set; }
		public string Message { get; set; }
		public IList<string> SQLs { get; set; }
		public T Result { get; set; }
		public IEnumerable<Error> Errors { get; set; }

		public CommandExecutionResult()
		{
			//
		}

		public static CommandExecutionResult<T> Default()
		{
			return new CommandExecutionResult<T>();
		}
	}

	public class CommandExecutionResult : CommandExecutionResult<object>
	{
		public new static CommandExecutionResult Default()
		{
			return new CommandExecutionResult();
		}

		public static CommandExecutionResult WithError(params KeyValuePair<int, string>[] errors)
		{
			return new CommandExecutionResult
			{
                MessageType = (int)Enums.MessageType.Error,
				Errors = errors.ToList().Select(x => new Error { Code = x.Key, Message = x.Value })
			};
		}
	}
}
