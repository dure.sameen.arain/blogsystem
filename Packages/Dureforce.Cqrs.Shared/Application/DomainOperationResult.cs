﻿ 
namespace Dureforce.Cqrs.Shared.Application
{
	public class DomainOperationResult
	{
		public object Metadata { get; }

		public DomainOperationResult()
		{
			//
		}

		//[JsonConstructor]
		public DomainOperationResult(object metadata)
		{
			Metadata = metadata;
		}

		public static DomainOperationResult Create(object model)
		{
			return new DomainOperationResult(model);
		}

		public static DomainOperationResult CreateEmpty()
		{
			return new DomainOperationResult();
		}

		public static DomainOperationResult Update(object id)
		{
			return new DomainOperationResult(id);
		}

		public static DomainOperationResult UpdateEmpty()
		{
			return new DomainOperationResult();
		}

		public static DomainOperationResult Success(object id)
		{
			return new DomainOperationResult(id);
		}

		public static DomainOperationResult SuccessEmpty()
		{
			return new DomainOperationResult();
		}

		public static DomainOperationResult Fail(object id)
		{
			return new DomainOperationResult(id);
		}

		public static DomainOperationResult FailEmpty()
		{
			return new DomainOperationResult();
		}
	}
}
