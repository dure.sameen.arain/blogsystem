﻿using MediatR;

namespace Dureforce.Cqrs.Shared.Application
{
	public interface IQuery<T> : IRequest<T>
	{
		//
	}
}
