﻿using MediatR;
using System.Net;

namespace Dureforce.Cqrs.Shared.Application.Handlers
{
	public abstract class CommandHandler<TRequest> : Handler, IRequestHandler<TRequest, CommandExecutionResult> where TRequest : IRequest<CommandExecutionResult>
	{
		public abstract Task<CommandExecutionResult> Handle(TRequest command, CancellationToken cancellationToken);

		protected Task<CommandExecutionResult> Ok(DomainOperationResult data)
		{
			CommandExecutionResult result;
			if (data != null)
			{
				result = new() { Message = "Success", Result = data.Metadata, StatusCode = HttpStatusCode.OK };
			}
			else
			{
				result = new() { Message = "Error", StatusCode = HttpStatusCode.InternalServerError };
			}
			return Task.FromResult(result);
		}

		protected Task<CommandExecutionResult> Fail(params string[] errorMessages)
		{
			CommandExecutionResult result = new()
			{
				Message = "Error",
				Errors = errorMessages.ToList().Select(x => new Error { Code = 404, Message = x }),
				StatusCode = HttpStatusCode.NotFound
			};

			return Task.FromResult(result);
		}

		protected Task<CommandExecutionResult> BadRequest(params string[] errorMessages)
		{
			CommandExecutionResult result = new()
			{
				Message = "Error",
				Errors = errorMessages.ToList().Select(x => new Error { Code = 400, Message = x }),
				StatusCode = HttpStatusCode.BadRequest
			};

			return Task.FromResult(result);
		}
		protected Task<CommandExecutionResult> RecordExist(params string[] errorMessages)
		{
			CommandExecutionResult result = new()
			{
				Message = "Error",
				Errors = errorMessages.ToList().Select(x => new Error { Code = 409, Message = x }),
				StatusCode = HttpStatusCode.Conflict
			};

			return Task.FromResult(result);
		}

        protected Task<CommandExecutionResult> RecordNotFound (params string[] errorMessages)
        {
            CommandExecutionResult result = new()
            {
                Message = "Error",
                Errors = errorMessages.ToList().Select(x => new Error { Code = 404, Message = x }),
                StatusCode = HttpStatusCode.NotFound
            };

            return Task.FromResult(result);
        }
    }
}
