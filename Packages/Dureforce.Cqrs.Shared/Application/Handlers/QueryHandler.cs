﻿using Dureforce.Cqrs.Shared.Enums;
using Dureforce.Cqrs.Shared.Models;
using MediatR;
using System.Net;

namespace Dureforce.Cqrs.Shared.Application.Handlers
{
	public abstract class QueryHandler<TRequest, TResponse> : Handler, IRequestHandler<TRequest, ResponseModel<TResponse>> where TRequest : IRequest<ResponseModel<TResponse>>
	{
		public QueryHandler()
		{
			//
		}

		public abstract Task<ResponseModel<TResponse>> Handle(TRequest request, CancellationToken cancellationToken);

		protected Task<ResponseModel<TResponse>> Ok(TResponse data)
		{
			ResponseModel<TResponse> result;
			if (data != null)
			{
				result = new() { Message = "Success", Result = data, StatusCode = HttpStatusCode.OK };
			}
			else
			{
				result = new() { Message = "Info", MessageType = (int)MessageType.Info, StatusCode = HttpStatusCode.NotFound };
			}
			return Task.FromResult(result);
		}

		protected Task<QueryExecutionResult<IEnumerable<TResponse>>> Ok(IList<TResponse> data)
		{
			QueryExecutionResult<IEnumerable<TResponse>> result = new() { Message = "Success", Result = data };

			return Task.FromResult(result);
		}

		protected Task<ResponseModel<TResponse>> NotFound(string errorMessages)
		{
			ResponseModel<TResponse> result = new ResponseModel<TResponse>
			{
				Message = "Info",
				MessageType = (int)MessageType.Info,
				StatusCode = HttpStatusCode.NotFound,
				Errors = new[] { errorMessages },
			};

			return Task.FromResult(result);
		}

		protected Task<ResponseModel<TResponse>> BadRequest(string errorMessages)
		{
			ResponseModel<TResponse> result = new ResponseModel<TResponse>
			{
				Message = "Info",
				MessageType = (int)MessageType.Info,
				StatusCode = HttpStatusCode.BadRequest,
				Errors = new[] { errorMessages },
			};

			return Task.FromResult(result);
		}
	}
}
