﻿using MediatR;

namespace Dureforce.Cqrs.Shared.Application.Handlers
{
	public abstract class QueryDevxHandler<TRequest, TResponse> : Handler, IRequestHandler<TRequest, TResponse> where TRequest : IRequest<TResponse>
	{
		public QueryDevxHandler()
		{
			//
		}

		public abstract Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken);

		protected Task<QueryExecutionResult<IEnumerable<TResponse>>> Ok(IList<TResponse> data)
		{
			QueryExecutionResult<IEnumerable<TResponse>> result = new() { Message = "Success", Result = data };

			return Task.FromResult(result);
		}
	}
}
