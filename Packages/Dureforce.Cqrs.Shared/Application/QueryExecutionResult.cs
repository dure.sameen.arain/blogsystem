﻿using System.Net;

namespace Dureforce.Cqrs.Shared.Application
{
	public class QueryExecutionResult<T>
	{
		public Guid EventId { get; set; } = Guid.NewGuid();
		public HttpStatusCode StatusCode { get; set; }
		public int MessageType { get; set; }
		public string Message { get; set; }
		public IList<string> SQLs { get; set; }
		public T Result { get; set; }
		public IList<string> Errors;

		public QueryExecutionResult()
		{
			//
		}
	}
}
