﻿namespace Dureforce.Cqrs.Shared.Application
{
	public class Error
	{
		public int Code { get; set; }
		public string Message { get; set; }
	}
}
