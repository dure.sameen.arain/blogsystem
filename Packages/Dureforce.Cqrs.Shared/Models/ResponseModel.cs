﻿using System.Net;

namespace Dureforce.Cqrs.Shared.Models
{
    public class ResponseModel<T>
    {
        public IList<string> Errors;

        public ResponseModel() { }

        public Guid EventId { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public int TotalRecords { get; set; }
        public int MessageType { get; set; }
        public string Message { get; set; }
        public IList<string> SQLs { get; set; }
        public T Result { get; set; }
    }
}
