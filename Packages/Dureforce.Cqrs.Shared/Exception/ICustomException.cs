﻿namespace Dureforce.Cqrs.Shared.Exception
{
    public interface ICustomException
    {
        int StatusCode { get; set; }
        string Message { get; set; }
    }
}
