﻿namespace Dureforce.Cqrs.Shared.Exception
{
	public class InvalidDatabaseSchemaErrorException : SystemException,ICustomException
	{
		public int StatusCode { get; set; }
		public new string Message { get; set; }

		public InvalidDatabaseSchemaErrorException(int statusCode, string message)
			: base(message)
		{
			StatusCode = statusCode;
			Message = message;
		}
	}
}
