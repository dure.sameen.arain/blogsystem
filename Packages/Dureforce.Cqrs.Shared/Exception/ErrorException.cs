﻿using SystemException = System.Exception;

namespace Dureforce.Cqrs.Shared.Exception
{
	public class ErrorException : SystemException, ICustomException
	{
		public int StatusCode { get; set; }
		public new string Message { get; set; }

		public ErrorException(int statusCode, string message)
			: base(message)
		{
			StatusCode = statusCode;
			Message = message;
		}
	}
}
