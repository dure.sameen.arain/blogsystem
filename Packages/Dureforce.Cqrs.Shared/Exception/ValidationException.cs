﻿using SystemException = System.Exception;

namespace Dureforce.Cqrs.Shared.Exception
{
	public class ValidationException : SystemException, IValidationException
	{
		public int Status { get; set; }
		public string Title { get; set; }
		public Dictionary<string, List<string>> Errors { get; set; }
		public ValidationException(int status, string title, Dictionary<string, List<string>> errors) : base(title)
		{
			Status = status;
			Title = title;
			Errors = errors;
		}
	}
}
