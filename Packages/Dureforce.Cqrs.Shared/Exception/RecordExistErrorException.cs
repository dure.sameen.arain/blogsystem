﻿namespace Dureforce.Cqrs.Shared.Exception
{
	public class RecordExistErrorException : SystemException,ICustomException
	{
		public int StatusCode { get; set; }
		public new string Message { get; set; }

		public RecordExistErrorException(int statusCode, string message)
			: base(message)
		{
			StatusCode = statusCode;
			Message = message;
		}
	}
}
