﻿namespace Dureforce.Cqrs.Shared.Exception
{
    public interface IValidationException
    {
        int Status { get; set; }
        string Title { get; set; }
        Dictionary<string, List<string>> Errors { get; set; }
    }
}
