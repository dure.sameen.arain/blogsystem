﻿using SystemException = System.Exception;
namespace Dureforce.Cqrs.Shared.Exception
{
	public class DeleteErrorException : SystemException,ICustomException
	{
		public int StatusCode { get; set; }
		public new string Message { get; set; }

		public DeleteErrorException(int statusCode, string message)
			: base(message)
		{
			StatusCode = statusCode;
			Message = message;
		}
	}
}
