﻿namespace Dureforce.Cqrs.Shared.Exception
{
	public class RecordNotFoundErrorException : SystemException, ICustomException
	{
		public int StatusCode { get; set; }
		public new string Message { get; set; }

		public RecordNotFoundErrorException(int statusCode, string message)
		   : base(message)
		{
			StatusCode = statusCode;
			Message = message;
		}
	}
}
