﻿using System.Text.Json.Serialization;

namespace Dureforce.Cqrs.Shared.Command
{
    public class BaseCommand
    {
        [JsonIgnore]
        public string UserClaimsId { get; set; }

        public void SetUserClaimsId(string email)
        {
            UserClaimsId = email;
        }

        public string GetUserClaimsId()
        {
            return UserClaimsId;
        }
    }
}
