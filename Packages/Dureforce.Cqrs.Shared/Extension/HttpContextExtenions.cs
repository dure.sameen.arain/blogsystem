﻿using System.Security.Claims;

namespace Dureforce.Cqrs.Shared.Extension
{/// <summary>
/// common used extensions
/// </summary>
    public static class HttpContextExtensions
    {
        /// <summary>
        /// get name claim
        /// </summary>
        /// <param name="principal"></param>
        /// <returns></returns>
        public static string UserName(this ClaimsPrincipal principal)
        {
            var name = principal.Claims
                .Where(x => x.Type == "name")
                .Select(x => x.Value).FirstOrDefault();
            return name;
        }
    }
}
