﻿namespace Dureforce.Cqrs.Shared.Extension
{
	public static class EnumExtension
	{
		public static string GetName<T>(this T value)
		{
			return Enum.GetName(typeof(T), value);
		}
	}
}
