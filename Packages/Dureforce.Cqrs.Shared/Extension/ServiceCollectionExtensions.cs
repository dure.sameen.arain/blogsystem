﻿using Microsoft.Extensions.DependencyInjection;

namespace Dureforce.Cqrs.Shared.Extension
{
	/// <summary>
	/// The <see cref="T:Microsoft.Extensions.DependecnyInjection.IServiceCollection"/> Extensions.
	/// </summary>
	public static class ServiceCollectionExtensions
	{
		/// <summary>
		/// Adds new module to the <see cref="T:Microsoft.Extensions.DependecnyInjection.IServiceCollection"/>.
		/// </summary>
		/// <param name="services">The <see cref="T:Microsoft.Extensions.DependecnyInjection.IServiceCollection" /></param>
		/// <param name="module">The instance of <inherit cref="IModule" /></param>
		/// <returns></returns>
		public static IServiceCollection AddModule(this IServiceCollection services, IModule module)
		{
			if (services == null)
				throw new ArgumentNullException(nameof(services));
			if (module == null)
				throw new ArgumentNullException(nameof(module));
			module.Load(services);
			return services;
		}
	}
}
