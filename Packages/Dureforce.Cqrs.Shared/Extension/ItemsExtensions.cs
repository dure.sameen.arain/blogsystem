﻿using Dureforce.Cqrs.Shared.Domain;

namespace Dureforce.Cqrs.Shared.Extension
{
	public static class Extensions
	{
		public static T GetItem<T>(this IList<T> items, Guid key) where T : BaseEntity<Guid>
		{
			string name = string.Empty;

			if (items != null && items.Any())
			{
				T type = items.SingleOrDefault(x => x.Id == key && !x.Deleted);

				if (type != null)
				{
					return type;
				}
			}

			return default!;
		}

		public static T GetItem<T>(this IList<T> items, int key) where T : BaseEntity<int>
		{
			string name = string.Empty;

			if (items != null && items.Any())
			{
				T type = items.SingleOrDefault(x => x.Id == key && !x.Deleted);

				if (type != null)
				{
					return type;
				}
			}

			return default!;
		}
	}
}
