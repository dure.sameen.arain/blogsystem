﻿using Microsoft.Extensions.DependencyInjection;

namespace Dureforce.Cqrs.Shared.Extension
{
	/// <summary>
	/// Dependency Injection Module.
	/// </summary>
	public interface IModule
	{
		/// <summary>
		/// Dependency Injection Registration.
		/// </summary>
		/// <param name="services">The service parameter</param>
		void Load(IServiceCollection services);
	}
}
