﻿namespace Dureforce.Cqrs.Shared.Domain
{
	public interface IBaseEntity<TKey>
	{
		TKey Id { get; set; }
	}
}
