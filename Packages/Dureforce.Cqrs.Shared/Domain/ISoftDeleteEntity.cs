﻿namespace Dureforce.Cqrs.Shared.Domain
{
	public interface ISoftDeleteEntity
	{
		bool Deleted { get; set; }
	}
}
