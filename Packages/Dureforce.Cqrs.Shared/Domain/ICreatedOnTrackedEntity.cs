﻿namespace Dureforce.Cqrs.Shared.Domain
{
	public interface ICreatedOnTrackedEntity
	{
		DateTime CreatedOn { get; set; }
	}
}
