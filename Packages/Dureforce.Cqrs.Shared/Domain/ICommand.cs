﻿using MediatR;

namespace Dureforce.Cqrs.Shared.Domain
{
	public interface ICommand<T> : IRequest<T>
	{
		//
	}
}
