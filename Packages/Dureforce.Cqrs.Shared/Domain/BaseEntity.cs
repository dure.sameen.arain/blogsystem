﻿using Dureforce.Cqrs.Shared.DomainInfrastructure;
using Dureforce.Database.Attributes;

namespace Dureforce.Cqrs.Shared.Domain
{
	public abstract class BaseEntity<TKey> : IBaseEntity<TKey>, ISoftDeleteEntity
	{
		[PrimaryId]
		public TKey Id { get; set; }
		public DateTime? CreatedOn { get; set; }
		public string CreatedBy { get; set; }
		public DateTime? ModifiedOn { get; set; }
		public string ModifiedBy { get; set; } 
        public bool Deleted { get; set; }

        public void Delete(string userClaimsId)
        {
            Deleted = true;
            ModifiedOn = DateTime.Now;
            ModifiedBy = userClaimsId;
        }

        public void Restore(string userClaimsId)
        {
            Deleted = false;
            ModifiedOn = DateTime.Now;
            ModifiedBy = userClaimsId;
        }

        protected void Raise(DomainEvent @event)
		{
			@event.AggregateRootId = Id;
			@event.Id = Guid.NewGuid();
			@event.EventId = @event.EventId;
			@event.OccuredOn = DateTime.UtcNow;
			@event.EventType = @event.GetType().Name;
			@event.CreatedOn = DateTime.UtcNow;
			@event.LastChangedOn = DateTime.UtcNow;

			(this as IHasDomainEvents).Raise(@event);
		}
	}
}
