﻿namespace Dureforce.Cqrs.Shared.Domain
{
	public interface IModifiedOnTrackedEntity
	{
		DateTime ModifiedOn { get; set; }
	}
}
