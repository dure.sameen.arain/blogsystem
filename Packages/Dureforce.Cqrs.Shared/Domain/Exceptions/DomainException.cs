﻿using Dureforce.Cqrs.Shared.Exception;
using SystemException = System.Exception;

namespace Dureforce.Cqrs.Shared.Domain.Exceptions
{
	public class DomainException : SystemException, ICustomException
	{
		//public readonly int ErrorCode;

		public int StatusCode { get; set; }
		public new string Message { get; set; }

		//public DomainException(int statusCode, string message)
		//    : base(message)
		//{
		//    //
		//}

		public DomainException(int statusCode, string message)
			: base(message)
		{
			StatusCode = statusCode;
			Message = message;
		}

	}
}
