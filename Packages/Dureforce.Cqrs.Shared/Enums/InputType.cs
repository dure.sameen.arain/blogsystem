﻿namespace Dureforce.Cqrs.Shared.Enums
{
	public enum InputType
	{
		Text,
		TextArea,
		LookUp
	}
}
