﻿namespace Dureforce.Cqrs.Shared.Enums
{
	public enum ExportType
	{
		Excel = 1,
		CSV
	}
}
