﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dureforce.Cqrs.Shared.Enums
{
    public enum ProjectType
    {
        UnitTest,
        Api
    }
}
