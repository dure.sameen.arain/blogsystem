﻿
namespace Dureforce.Swagger.B2C.ViewModels
{
    public class ApiDetails
    {
        public ApiDetails()
        {
        }
        public string IdpUrl { get { return $"{Instance}{TenantId}"; } }
        /// <summary>
        /// Instance 
        /// </summary>
        public string Instance { get; set; }

        /// <summary>
        /// ClientId 
        /// </summary>
        public string ClientId { get; set; }
        /// <summary>
        /// ClientId 
        /// </summary>
        public string TenantId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Scope { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Version { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AuthorizationFLow { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Secret { get; set; }

    }
}
