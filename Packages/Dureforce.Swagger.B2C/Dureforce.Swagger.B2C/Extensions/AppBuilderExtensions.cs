﻿using Microsoft.AspNetCore.Builder;

namespace Dureforce.Swagger.B2C.Extensions
{
    public static class AppBuilderExtensions
    {

        public static IApplicationBuilder UseSwaggerEndpoint(this IApplicationBuilder app, string title,string appname, string clientId)
        {
            app.UseSwagger(); 
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", title);
                c.OAuthAppName(appname);
                c.OAuthClientId(clientId);
            });
            return app;
        }
        public static IApplicationBuilder UseSwaggerEndpoint(this IApplicationBuilder app, string title)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", title);
              
            });
            return app;
        }
    }
}
