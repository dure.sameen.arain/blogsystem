﻿using Dureforce.Swagger.B2C.Authorization;
using Dureforce.Swagger.B2C.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;

namespace Dureforce.Swagger.B2C.Extensions
{
    public static class SwaggerExtension
    {
        public static IServiceCollection AddSwagger(
            this IServiceCollection services,
            ApiDetails api,
            string xmlPath)
        {


            if (string.IsNullOrEmpty(api.IdpUrl))
                throw new Exception("Authorization Url cannot be null or empty.");
            if (string.IsNullOrEmpty(api.Title))
                throw new Exception("Api title Url cannot be null or empty.");

            if (string.IsNullOrEmpty(api.Version))
                throw new Exception("Version cannot be null or empty.");

            if (string.IsNullOrEmpty(xmlPath))

                throw new Exception("File path Url cannot be null or empty.");


            var scopes = api.Scope.Split(',');
            var scopeDictionary = new Dictionary<string, string>();
            foreach (string scope in scopes)
                scopeDictionary.Add(scope, scope);
            var flows = new OpenApiOAuthFlows()
            {
                Implicit = new OpenApiOAuthFlow()
                {
                    AuthorizationUrl = new Uri($"{api.IdpUrl}/oauth2/v2.0/authorize"),
                    TokenUrl = new Uri($"{api.IdpUrl}/oauth2/v2.0/token"),
                    Scopes = scopeDictionary
                }
            };
            services.AddSwaggerGen(c =>
            {
                c.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
                {
                    Type = SecuritySchemeType.OAuth2,
                    Flows = flows,
                    In = ParameterLocation.Header,
                    Scheme = "oauth2",
                    BearerFormat = "JWT",


                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement{
                {
                    new OpenApiSecurityScheme{
                        Reference = new OpenApiReference{
                            Id = "Bearer", //The name of the previously defined security scheme.
                            Type = ReferenceType.SecurityScheme

                        }
                    },new List<string>()
                }  });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement{
    {
        new OpenApiSecurityScheme{
            Reference = new OpenApiReference{
                Id = "oauth2", //The name of the previously defined security scheme.
                Type = ReferenceType.SecurityScheme

            }
        },new List<string>()
    }  });


                c.CustomSchemaIds(x => x.FullName);
                c.SwaggerDoc(api.Version, new OpenApiInfo { Title = api.Title, Version = api.Version });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "",
                });
                c.OperationFilter<AssignOAuth2SecurityRequirements>();
                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
      {
        {
          new OpenApiSecurityScheme
          {
            Reference = new OpenApiReference
              {
                Type = ReferenceType.SecurityScheme,
                Id = "Bearer"
              },
              Scheme = "oauth2",
              Name = "Bearer",
              In = ParameterLocation.Header,

            },
            new List<string>()
          }
        });

                c.IncludeXmlComments(xmlPath);


            });

            return services;
        }
    }
}
