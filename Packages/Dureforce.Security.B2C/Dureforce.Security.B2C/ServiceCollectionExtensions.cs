﻿using Dureforce.Security.B2C.AuthorizationPolicies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Identity.Web;
using System.Collections.Generic;

namespace Dureforce.Security.B2C
{
    public static class ServiceCollectionExtensions
    {
        //B2C Authentication
        public static IServiceCollection AddAuthentication(this IServiceCollection services, IConfiguration config)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
             .AddMicrosoftIdentityWebApi(config);


            return services;
        }

        //B2C Authorization
        public static IServiceCollection AddCustomAuthorization(this IServiceCollection services, Dictionary<string, string> policies)
        {

            services.AddAuthorization(options =>
            {
                foreach (var policyItem in policies)
                    options.AddPolicy(policyItem.Key,
                        policy => policy.Requirements.Add(new ScopesRequirement(policyItem.Value)));
            });
            return services;
        }
    }
}
