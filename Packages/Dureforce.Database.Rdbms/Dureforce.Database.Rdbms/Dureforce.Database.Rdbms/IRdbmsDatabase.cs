﻿namespace Dureforce.Database.Rdbms
{
    public interface IRdbmsDatabase : IDatabase
    {
        string Schema { get; set; }
        Task SaveChanges();
    }
}
