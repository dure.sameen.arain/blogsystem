﻿using Dureforce.Cache.Application;
using Dureforce.Cache.Configuration;
using Dureforce.Cache.Group;
using Dureforce.Cache.Handler;
using Microsoft.Extensions.DependencyInjection;

namespace Dureforce.Cache
{
    public static class CachingExtensions
    { 

        public static void AddCaching(this IServiceCollection services, CacheOptions cacheOptions )
        {
            services.AddOptions();
            services.AddSingleton<ICacheHandler, CacheHandler>();
            services.AddSingleton<ICacheGroup, CacheGroup>();
            services.AddSingleton<ICacheManager, ApplicationCacheManager>();
            services.Configure<CacheOptions>(opts =>
            {
                opts.Enabled = cacheOptions.Enabled;
                opts.DefaultCacheDurationSeconds = cacheOptions.DefaultCacheDurationSeconds;
            });
        }
    }
}
