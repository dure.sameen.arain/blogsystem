﻿
using Dureforce.Cache.Configuration;
using System.Threading;
using System.Threading.Tasks;

namespace Dureforce.Cache.Distributed
{
    public interface IDistributedCache
    {
        //
        // Summary:
        //     Gets a value with the given key.
        //
        // Parameters:
        //   key:
        //     A string identifying the requested value.
        //
        // Returns:
        //     The located value or null.
        byte[] Get(string key);
        //
        // Summary:
        //     Gets a value with the given key.
        //
        // Parameters:
        //   key:
        //     A string identifying the requested value.
        //
        //   token:
        //     Optional. The System.Threading.CancellationToken used to propagate notifications
        //     that the operation should be canceled.
        //
        // Returns:
        //     The System.Threading.Tasks.Task that represents the asynchronous operation, containing
        //     the located value or null.
        Task<byte[]> GetAsync(string key, CancellationToken token = default);
        //
        // Summary:
        //     Refreshes a value in the cache based on its key, resetting its sliding expiration
        //     timeout (if any).
        //
        // Parameters:
        //   key:
        //     A string identifying the requested value.
        void Refresh(string key);
        //
        // Summary:
        //     Refreshes a value in the cache based on its key, resetting its sliding expiration
        //     timeout (if any).
        //
        // Parameters:
        //   key:
        //     A string identifying the requested value.
        //
        //   token:
        //     Optional. The System.Threading.CancellationToken used to propagate notifications
        //     that the operation should be canceled.
        //
        // Returns:
        //     The System.Threading.Tasks.Task that represents the asynchronous operation.
        Task RefreshAsync(string key, CancellationToken token = default);
        //
        // Summary:
        //     Removes the value with the given key.
        //
        // Parameters:
        //   key:
        //     A string identifying the requested value.
        void Remove(string key);
        //
        // Summary:
        //     Removes the value with the given key.
        //
        // Parameters:
        //   key:
        //     A string identifying the requested value.
        //
        //   token:
        //     Optional. The System.Threading.CancellationToken used to propagate notifications
        //     that the operation should be canceled.
        //
        // Returns:
        //     The System.Threading.Tasks.Task that represents the asynchronous operation.
        Task RemoveAsync(string key, CancellationToken token = default);
        //
        // Summary:
        //     Sets a value with the given key.
        //
        // Parameters:
        //   key:
        //     A string identifying the requested value.
        //
        //   value:
        //     The value to set in the cache.
        //
        //   options:
        //     The cache options for the value.
        void Set(string key, byte[] value, DefaultCachePolicyOptions options);
        //
        // Summary:
        //     Sets the value with the given key.
        //
        // Parameters:
        //   key:
        //     A string identifying the requested value.
        //
        //   value:
        //     The value to set in the cache.
        //
        //   options:
        //     The cache options for the value.
        //
        //   token:
        //     Optional. The System.Threading.CancellationToken used to propagate notifications
        //     that the operation should be canceled.
        //
        // Returns:
        //     The System.Threading.Tasks.Task that represents the asynchronous operation.
        Task SetAsync(string key, byte[] value, DefaultCachePolicyOptions options, CancellationToken token = default);
    }
}
