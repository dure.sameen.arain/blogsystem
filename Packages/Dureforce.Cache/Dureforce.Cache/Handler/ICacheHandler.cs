﻿
namespace Dureforce.Cache.Handler
{
    public interface ICacheHandler
    {
        Task<bool> CacheExist<T>(string prefix, string key) where T : class;
        Task<T> GetCache<T>(string prefix, string key) where T : class;
        Task RefreshCache(string prefix, string key);
        Task RemoveCache(string prefix, string key);
        Task RemoveCache<TClass>(string keyToRemove) where TClass : class;
        Task SetCache<T, TClass>(string prefix, string key, T value)
            where T : class
            where TClass : class;
        void SetCacheExpiry(DateTime expiryDate);

        //custom methods
        Task<T> GetSetCache<T>(string prefix, string key, Func<Task<T>> func) where T : class, new();
    }
}
