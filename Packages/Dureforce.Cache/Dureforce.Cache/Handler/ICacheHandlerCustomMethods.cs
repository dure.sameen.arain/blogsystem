﻿
namespace Dureforce.Cache.Handler
{
    public interface ICacheHandlerCustomMethods
    {
        void FlushDb();
        bool ClearCache(string cacheKey);
    }
}
