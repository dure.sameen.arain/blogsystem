﻿using Dureforce.Cache.Configuration;
using Dureforce.Cache.Group;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Dureforce.Cache.Handler
{
    public class CacheHandler : ICacheHandler
    {
        private readonly ICacheManager _cacheStorage;
        private readonly ICacheGroup _cacheGroup;
        private readonly CacheOptions _options;
        public bool HasCacheExpiry { get; set; }
        private DateTimeOffset? _cacheExpiry;
        /// <summary>
        /// Initializes an instance of <see cref="CacheHandler"/> class.
        /// </summary>
        /// <param name="cacheStorage">The cache storage manager</param>
        /// <param name="options">The cache options</param> 
        public CacheHandler(ICacheManager cacheStorage, IOptions<CacheOptions> options)
        {
            _cacheStorage = cacheStorage;
            _cacheGroup = new CacheGroup(_cacheStorage);
            _options = options.Value;
        }

        /// <summary>
        /// Gets the data from cache.
        /// </summary>
        /// <typeparam name="T">The return object model</typeparam>
        /// <param name="prefix">The prefix key</param>
        /// <param name="key">The unique key</param>
        /// <returns></returns>
        public async Task<T> GetCache<T>(string prefix, string key) where T : class
        {
            if (!_options.Enabled)
                return default!;
            return await Task.FromResult(_cacheStorage.GetItem<T>(string.Concat(prefix, key)).CacheItem);
        }

        /// <summary>
        /// Sets or stores the data to cache.
        /// </summary>
        /// <typeparam name="T">The object type</typeparam>
        /// <typeparam name="TClass">The TClass</typeparam>
        /// <param name="prefix">The prefix key</param>
        /// <param name="key">The unique key</param>
        /// <param name="value">The value that need to store in cache</param>
        /// <returns></returns>
        public async Task SetCache<T, TClass>(string prefix, string key, T value) where T : class where TClass : class
        {
            if (!_options.Enabled)
                return;
            await Task.Run(() =>
            {

                if (_options.DefaultCacheDurationSeconds != 0)
                {
                    _cacheStorage.SetItem(string.Concat(prefix, key), value,
                                new DefaultCachePolicyOptions()
                                    .SetAbsoluteExpiration(TimeSpan.FromSeconds(_options.DefaultCacheDurationSeconds)));

                }
                else
                    _cacheStorage.SetItem(string.Concat(prefix, key), value);
            });
            if (!IsGetById(string.Concat(prefix, key)))
                _cacheGroup.AddToGroup(typeof(TClass).FullName, string.Concat(prefix, key));

        }

        private static bool IsGetById(string key)
        {
            return key.ToLower().Contains("byid");
        }

        /// <summary>
        /// Checks the cache exits for the provided pair of key or not.
        /// </summary>
        /// <typeparam name="T">The object type</typeparam>
        /// <param name="prefix">The prefix key</param>
        /// <param name="key">The unique key</param>
        /// <returns></returns>
        public async Task<bool> CacheExist<T>(string prefix, string key) where T : class
        {
            return await Task.FromResult(_cacheStorage.GetItem<T>(string.Concat(prefix, key)).Exists);
        }

        /// <summary>
        /// Removes the data from the cache.
        /// </summary>
        /// <param name="prefix">The prefix key</param>
        /// <param name="key">The unique key</param>
        /// <returns></returns>
        public async Task RemoveCache(string prefix, string key)
        {

            await Task.Run(() => _cacheStorage.RemoveItem(string.Concat(prefix, key)));
        }
        /// <summary>
        /// Removes the data from the cache group.and also the given key
        /// </summary> 
        /// <returns></returns>
        public async Task RemoveCache<TClass>(string keyToRemove) where TClass : class
        {

            await Task.Run(() => _cacheGroup.RemoveGroup(typeof(TClass).FullName, keyToRemove));
        }

        /// <summary>
        /// Refreshes the cache data.
        /// </summary>
        /// <param name="prefix">The prefix key</param>
        /// <param name="key">The unique key</param>
        /// <returns></returns>
        public async Task RefreshCache(string prefix, string key)
        {
            await Task.Run(() => _cacheStorage.Refresh(string.Concat(prefix, key)));
        }

        /// <summary>
        /// Sets the cache expiry date & time.
        /// </summary>
        /// <param name="expiryDate">The expiry date & time</param>
        public void SetCacheExpiry(DateTime expiryDate)
        {
            _cacheExpiry = new DateTimeOffset(expiryDate);
        }

        //Custom methods
        public async Task<T> GetSetCache<T>(string prefix, string key, Func<Task<T>> func) where T : class, new()
        {
            T results;
            if (!_options.Enabled)
            {
                results = await func.Invoke();
                return results;
            }
                        
            var cacheValue = await Task.FromResult(_cacheStorage.GetItem<T>(string.Concat(prefix, key)).CacheItem);
            if (cacheValue != null)
            {
                //results = Serializer.Deserialize<T>(cacheValue);
                results = cacheValue;
            }
            else
            {
                results = await func.Invoke();
                //cacheValue = Serializer.Serialize(results);
                await Task.Run(() =>
                {

                    if (_options.DefaultCacheDurationSeconds != 0)
                    {
                        _cacheStorage.SetItem(string.Concat(prefix, key), results,
                                    new DefaultCachePolicyOptions()
                                        .SetAbsoluteExpiration(TimeSpan.FromSeconds(_options.DefaultCacheDurationSeconds)));

                    }
                    else
                        _cacheStorage.SetItem(string.Concat(prefix, key), results);
                }); 
                
                _cacheGroup.AddToGroup(prefix, string.Concat(prefix, key));
            }

            return results;
        }
    }
}
