﻿using Dureforce.Cache.Configuration;
using Dureforce.Cache.Distributed;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Dureforce.Cache.Application
{

    /// <inheritdoc />
    /// <summary>
    /// Implementation of a cache manager that stores items
    /// </summary>
    public class ApplicationCacheManager : CacheManagerBase
    {
        /// <summary>
        /// Initializes a new instance of the ApplicationCacheManager
        /// </summary>
        /// <param name="cache">The IDistributedCache implementation to use</param>
        /// <param name="logger">The logger to use to write log messages</param>
        /// <param name="defaultPolicy">The default policy</param>
        /// <param name="options">The caching options to use</param>
        public ApplicationCacheManager(IDistributedCache cache, ILogger<ApplicationCacheManager> logger, IOptions<DefaultCachePolicyOptions> defaultPolicy, IOptions<CacheOptions> options) :
            base(cache, logger, defaultPolicy?.Value, options?.Value)
        {

        }
    }
}
