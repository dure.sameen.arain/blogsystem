﻿using Dureforce.Cache.Distributed;
using System;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Dureforce.Cache.Models;
using Dureforce.Cache.Configuration;

namespace Dureforce.Cache
{
    public interface ICacheManager
    {
        /// <summary>
        /// Get item from cache
        /// </summary>
        /// <typeparam name="T">Type of item to get</typeparam>
        /// <param name="key">Key of the item to get</param>
        /// <returns>CacheResult which contains indication if the items has been previously cached</returns>
        CacheResult<T> GetItem<T>(string key) where T : class;

        /// <summary>
        /// Get item from cache
        /// </summary>
        /// <typeparam name="T">Type of item to get</typeparam>
        /// <param name="key">Key of the item to get</param>
        /// <param name="converters">Converters to use while deserializing</param>
        /// <returns>CacheResult which contains indication if the items has been previously cached</returns>
        CacheResult<T> GetItem<T>(string key, params JsonConverter[] converters) where T : class;

        /// <summary>
        /// Get item from cache and store in cache if not in cache yet
        /// </summary>
        /// <typeparam name="T">Type of item to get</typeparam>
        /// <param name="key">Key of the item to get from the cache</param>
        /// <param name="delegateAction">Delegate function to invoke when item is not found in the cache. 
        /// The retrieved item will then be stored in the cache.</param>
        /// <returns>CacheResult which contains indication if the items has been previously cached</returns>
        T AddOrGetExisting<T>(string key, Func<T> delegateAction) where T : class;

        /// <summary>
        /// Get item from cache and store in cache if not in cache yet
        /// </summary>
        /// <typeparam name="T">Type of item to get</typeparam>
        /// <param name="key">Key of the item to get from the cache</param>
        /// <param name="delegateAction">Delegate function to invoke when item is not found in the cache. 
        ///     The retrieved item will then be stored in the cache.</param>
        /// <param name="converters">Converters to use while deserializing</param>
        /// <returns>CacheResult which contains indication if the items has been previously cached</returns>
        T AddOrGetExisting<T>(string key, Func<T> delegateAction, params JsonConverter[] converters) where T : class;

        /// <summary>
        /// Get item from cache sync
        /// </summary>
        /// <typeparam name="T">Type of item to get</typeparam>
        /// <param name="key">Key of the item to get</param>
        /// <param name="delegateAction">Delegate function to invoke when item is not found in the cache.
        ///     The retrieved item will then be stored in the cache.</param>
        /// <param name="options">retention options in the cache</param>
        /// <param name="converters">Converters to use while deserializing</param>
        /// <returns>CacheResult which contains indication if the items has been previously cached</returns>
        T AddOrGetExisting<T>(string key, Func<T> delegateAction, DefaultCachePolicyOptions options, params JsonConverter[] converters) where T : class;

        /// <summary>
        /// Get item from cache sync 
        /// </summary>
        /// <typeparam name="T">Type of item to get</typeparam>
        /// <param name="key">Key of the item to get</param>
        /// <param name="delegateAction">Delegate function to invoke when item is not found in the cache.
        /// The retrieved item will then be stored in the cache.</param>
        /// <param name="options">retention options in the cache</param>
        /// <returns>CacheResult which contains indication if the items has been previously cached</returns>
        T AddOrGetExisting<T>(string key, Func<T> delegateAction, DefaultCachePolicyOptions options) where T : class;

        /// <summary>
        /// Get item from cache Async
        /// </summary>
        /// <typeparam name="T">Type of item to get</typeparam>
        /// <param name="key">Key of the item to get</param>
        /// <returns>CacheResult which contains indication if the items has been previously cached</returns>
        Task<CacheResult<T>> GetItemAsync<T>(string key) where T : class;

        /// <summary>
        /// Get item from cache Async
        /// </summary>
        /// <typeparam name="T">Type of item to get</typeparam>
        /// <param name="key">Key of the item to get</param>
        /// <param name="converters">Converters to use while deserializing</param>
        /// <returns>CacheResult which contains indication if the items has been previously cached</returns>
        Task<CacheResult<T>> GetItemAsync<T>(string key, params JsonConverter[] converters) where T : class;

        /// <summary>
        /// Get item from cache and store in cache if not in cache yet Async
        /// </summary>
        /// <typeparam name="T">Type of item to get</typeparam>
        /// <param name="key">Key of the item to get from the cache</param>
        /// <param name="delegateAction">Delegate function to invoke when item is not found in the cache. 
        /// The retrieved item will then be stored in the cache.</param>
        /// <returns>CacheResult which contains indication if the items has been previously cached</returns>
        Task<T> AddOrGetExistingAsync<T>(string key, Func<Task<T>> delegateAction) where T : class;

        /// <summary>
        /// Get item from cache and store in cache if not in cache yet Async
        /// </summary>
        /// <typeparam name="T">Type of item to get</typeparam>
        /// <param name="key">Key of the item to get from the cache</param>
        /// <param name="delegateAction">Delegate function to invoke when item is not found in the cache. 
        ///     The retrieved item will then be stored in the cache.</param>
        /// <param name="converters">Converters to use while deserializing</param>
        /// <returns>CacheResult which contains indication if the items has been previously cached</returns>
        Task<T> AddOrGetExistingAsync<T>(string key, Func<Task<T>> delegateAction, params JsonConverter[] converters) where T : class;

        /// <summary>
        /// Get item from cache Async
        /// </summary>
        /// <typeparam name="T">Type of item to get</typeparam>
        /// <param name="key">Key of the item to get</param>
        /// <param name="delegateAction">Delegate function to invoke when item is not found in the cache. 
        /// The retrieved item will then be stored in the cache.</param>
        /// <param name="options">retention options in the cache</param>
        /// <returns>CacheResult which contains indication if the items has been previously cached</returns>
        Task<T> AddOrGetExistingAsync<T>(string key, Func<Task<T>> delegateAction, DefaultCachePolicyOptions options) where T : class;

        /// <summary>
        /// Get item from cache Async
        /// </summary>
        /// <typeparam name="T">Type of item to get</typeparam>
        /// <param name="key">Key of the item to get</param>
        /// <param name="delegateAction">Delegate function to invoke when item is not found in the cache. 
        ///     The retrieved item will then be stored in the cache.</param>
        /// <param name="options">retention options in the cache</param>
        /// <param name="converters">Converters to use while deserializing</param>
        /// <returns>CacheResult which contains indication if the items has been previously cached</returns>
        Task<T> AddOrGetExistingAsync<T>(string key, Func<Task<T>> delegateAction, DefaultCachePolicyOptions options, params JsonConverter[] converters) where T : class;

        /// <summary>
        /// Set and item in the cache
        /// </summary>
        /// <param name="key">Key of the item to store</param>
        /// <param name="item">Item to store</param>
        void SetItem<T>(string key, T item) where T : class;

        /// <summary>
        /// Set item in the cache async.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="item"></param>
        Task SetItemAsync<T>(string key, T item) where T : class;

        /// <summary>        
        /// Sets a item in the cache
        /// </summary>
        /// <param name="key">Key of the item to store</param>
        /// <param name="item">Item to store</param>        
        /// <param name="options">retention options in the cache</param>
        void SetItem<T>(string key, T item, DefaultCachePolicyOptions options) where T : class;

        /// <summary>
        /// Sets a item in the cache async.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="item"></param>
        /// <param name="options"></param>
        Task SetItemAsync<T>(string key, T item, DefaultCachePolicyOptions options) where T : class;

        /// <summary>
        /// Delete an item from the cache. Does nothing when item is not found
        /// </summary>
        /// <param name="key">Key of the item to delete</param>
        void RemoveItem(string key);

        /// <summary>
        /// deletes item from the cache async
        /// </summary>
        /// <param name="key"></param>
        /// <returns>task</returns>
        Task RemoveItemAsync(string key);

        /// <summary>
        /// extend the cache duration of a cached item.
        /// </summary>
        /// <param name="key"></param>
        void Refresh(string key);

        /// <summary>
        /// refereshes item from the cache Async
        /// </summary>
        /// <param name="key"></param>
        /// <returns>task</returns>
        Task RefreshAsync(string key);
    }
}
