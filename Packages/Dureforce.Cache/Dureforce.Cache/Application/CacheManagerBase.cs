﻿using Dureforce.Cache.Configuration;
using Dureforce.Cache.Distributed;
using Dureforce.Cache.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;


namespace Dureforce.Cache
{
    /// <inheritdoc />
    /// <summary>
    /// Base class for cachemenagers
    /// </summary>
    public abstract class CacheManagerBase : ICacheManager
    {
        protected readonly IDistributedCache _cache;
        private readonly ILogger _logger;
        private readonly CacheOptions _options;
        protected readonly DefaultCachePolicyOptions _defaultPolicy;
        protected JsonSerializerSettings DefaultSerializerSettings => new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            TypeNameHandling = TypeNameHandling.Auto,
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        };


        /// <summary>
        /// .ctor for CacheManagerBase.
        /// </summary>
        /// <param name="cache">A class instance that implements IDistributedCache interface.</param>
        /// <param name="logger">Logger instance that implements ILogger interface.</param>
        /// <param name="policy">Default cache policy or any class that inherits from DefaultCachePolicy</param>
        public CacheManagerBase(IDistributedCache cache, ILogger logger, DefaultCachePolicyOptions policy, CacheOptions options)
        {
            _cache = cache ?? throw new ArgumentNullException(nameof(cache));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _defaultPolicy = policy == null ? throw new ArgumentNullException() :
                 new DefaultCachePolicyOptions
                 {
                     SlidingExpiration = policy.SlidingExpiration,
                     AbsoluteExpirationRelativeToNow = policy.AbsoluteExpirationRelativeToNow
                 };
            _options = options ?? new CacheOptions();
            var cacheEnabled = _options.Enabled ? "enabled" : "disabled";
            if (!_options.Enabled)
            {
                _logger.LogInformation($"!------ Caching has been {cacheEnabled}. ---------!");
            }
        }

        /// <summary>
        /// Creates / Modifies the key that is used for caching.
        /// </summary>
        /// <param name="key">empty or a base key</param>
        /// <returns>a string</returns>
        protected virtual string CreateKey(string key)
        {
            return key;
        }

        public CacheResult<T> GetItem<T>(string key) where T : class  
        {
            return GetItem<T>(key, null);
        }


        /// <summary>
        /// Gets the CacheItem by key, additionally you can add converters for specific conversions
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="converters"></param>
        /// <returns></returns>
        public CacheResult<T> GetItem<T>(string key, params JsonConverter[] converters) where T : class
        {

            if (null == key)
            {
                throw new ArgumentNullException(nameof(key));
            }

            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentException("Value cannot be empty or a white space", nameof(key));
            }

            var cacheResult = _cache.GetString(CreateKey(key));

            if (string.IsNullOrEmpty(cacheResult))
            {
                return new CacheResult<T>(null, false);
            }


            if (null == converters || converters.Length == 0)
            {
                return Newtonsoft.Json.JsonConvert.DeserializeObject<CacheResult<T>>(cacheResult, DefaultSerializerSettings);
            }

            return Newtonsoft.Json.JsonConvert.DeserializeObject<CacheResult<T>>(cacheResult, converters);
        }

        /// <summary>
        /// Adds a item to cache  if it does not exists, otherwise executes  the delegateAction and stores the result then returning the result.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="delegateAction"></param>
        /// <param name="converters"></param>
        /// <returns></returns>
        public T AddOrGetExisting<T>(string key, Func<T> delegateAction, params JsonConverter[] converters) where T : class
        {

            if (null == key)
            {
                throw new ArgumentNullException(nameof(key));
            }

            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentException("Value cannot be empty or a white space", nameof(key));
            }

            if (!_options.Enabled)
            {
                return delegateAction();
            }

            var result = GetItem<T>(key, converters);
            if (result.Exists)
            {
                return result.CacheItem;
            }

            var value = delegateAction();
            SetItem(key, value);
            return value;
        }

        public T AddOrGetExisting<T>(string key, DefaultCachePolicyOptions options, Func<T> delegateAction) where T : class
        {
            return AddOrGetExisting(key, delegateAction, options, null);
        }

        public T AddOrGetExisting<T>(string key, Func<T> delegateAction) where T : class
        {
            return AddOrGetExisting(key, delegateAction, (JsonConverter[])null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="delegateAction"></param>
        /// <param name="options"></param>
        /// <param name="converters"></param>
        /// <returns></returns>
        public T AddOrGetExisting<T>(string key, Func<T> delegateAction, DefaultCachePolicyOptions options, params JsonConverter[] converters) where T : class
        {


            if (null == key)
            {
                throw new ArgumentNullException(nameof(key));
            }

            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentException("Value cannot be empty or a white space", nameof(key));
            }

            if (null == options)
            {
                throw new ArgumentNullException(nameof(options));
            }

            if (!_options.Enabled)
            {
                return delegateAction();
            }

            var result = GetItem<T>(key, converters);

            if (result.Exists)
            {
                return result.CacheItem;
            }

            var value = delegateAction();
            SetItem(key, value, options);

            return value;
        }

        public T AddOrGetExisting<T>(string key, Func<T> delegateAction, DefaultCachePolicyOptions options) where T : class
        {
            return AddOrGetExisting(key, delegateAction, options, null);
        }

        public async Task<CacheResult<T>> GetItemAsync<T>(string key) where T : class
        {
            return await GetItemAsync<T>(key, null);
        }

        public async Task<CacheResult<T>> GetItemAsync<T>(string key, params JsonConverter[] converters) where T : class
        {
            if (null == key)
            {
                throw new ArgumentNullException(nameof(key));
            }

            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentException("Value cannot be empty or a white space", nameof(key));
            }

            var cacheResult = await _cache.GetStringAsync(CreateKey(key));

            if (string.IsNullOrEmpty(cacheResult))
                return new CacheResult<T>(null, false);

            if (null == converters || converters.Length == 0)
            {
                return Newtonsoft.Json.JsonConvert.DeserializeObject<CacheResult<T>>(cacheResult);
            }
            return Newtonsoft.Json.JsonConvert.DeserializeObject<CacheResult<T>>(cacheResult, converters);
        }

        public async Task<T> AddOrGetExistingAsync<T>(string key, Func<Task<T>> delegateAction) where T : class
        {
            return await AddOrGetExistingAsync(key, delegateAction, (JsonConverter[])null);
        }

        public async Task<T> AddOrGetExistingAsync<T>(string key, Func<Task<T>> delegateAction, params JsonConverter[] converters) where T : class
        {
            if (null == key)
            {
                throw new ArgumentNullException(nameof(key));
            }

            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentException("Value cannot be empty or a white space", nameof(key));
            }

            if (!_options.Enabled)
            {
                return await delegateAction();
            }

            var result = await GetItemAsync<T>(key, converters);

            if (result.Exists)
                return result.CacheItem;

            var value = await delegateAction();
            await SetItemAsync(key, value);

            return value;
        }

        public async Task<T> AddOrGetExistingAsync<T>(string key, Func<Task<T>> delegateAction, DefaultCachePolicyOptions options) where T : class
        {
            return await AddOrGetExistingAsync(key, delegateAction, options, null);
        }

        /// <summary>
        /// Gets a item from cache and if it does not exists it adds it.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="delegateAction"></param>
        /// <param name="options"></param>
        /// <param name="converters"></param>
        /// <returns></returns>
        public async Task<T> AddOrGetExistingAsync<T>(string key, Func<Task<T>> delegateAction, DefaultCachePolicyOptions options, params JsonConverter[] converters) where T : class
        {
            if (null == key)
            {
                throw new ArgumentNullException(nameof(key));
            }

            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentException("Value cannot be empty or a white space", nameof(key));
            }

            if (!_options.Enabled)
            {
                return await delegateAction();
            }

            if (null == options)
            {
                throw new ArgumentNullException(nameof(options));
            }

            var result = await GetItemAsync<T>(key, converters);
            if (result.Exists)
            {
                return result.CacheItem;
            }
            var value = await delegateAction();
            await SetItemAsync(key, value, options);

            return value;
        }

        public void SetItem<T>(string key, T item) where T : class
        {
            if (null == key)
            {
                throw new ArgumentNullException(nameof(key));
            }

            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentException("Value cannot be empty or a white space", nameof(key));
            }

            var result = JsonConvert.SerializeObject(new CacheResult<T>(item, true), DefaultSerializerSettings);

            if (null == _defaultPolicy)
                throw new NullReferenceException($"{nameof(_defaultPolicy)} cannot be null");
            _defaultPolicy.SetAbsoluteExpiration(TimeSpan.FromSeconds(_options.DefaultCacheDurationSeconds));

            _cache.SetString(CreateKey(key), result, _defaultPolicy);

        }

        public void SetItem<T>(string key, T item, DefaultCachePolicyOptions options) where T : class
        {

            if (null == key)
            {
                throw new ArgumentNullException(nameof(key));
            }

            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentException("Value cannot be empty or a white space", nameof(key));
            }

            if (null == options)
            {
                throw new ArgumentNullException(nameof(options));
            }

            var result = JsonConvert.SerializeObject(new CacheResult<T>(item, true), DefaultSerializerSettings);

            _cache.SetString(CreateKey(key), result, options);
        }

        public void Refresh(string key)
        {
            if (null == key)
            {
                throw new ArgumentNullException(nameof(key));
            }

            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentException("Value cannot be empty or a white space", nameof(key));
            }

            _cache.Refresh(CreateKey(key));
        }

        public async Task RefreshAsync(string key)
        {
            if (null == key)
            {
                throw new ArgumentNullException(nameof(key));
            }

            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentException("Value cannot be empty or a white space", nameof(key));
            }

            await _cache.RefreshAsync(CreateKey(key));
        }

        public async Task SetItemAsync<T>(string key, T item) where T : class
        {
            if (null == key)
            {
                throw new ArgumentNullException(nameof(key));
            }

            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentException("Value cannot be empty or a white space", nameof(key));
            } 

            string result = JsonConvert.SerializeObject(new CacheResult<T>(item, true), DefaultSerializerSettings);

            if (null == _defaultPolicy)
                throw new NullReferenceException($"{nameof(_defaultPolicy)} cannot be null");
            _defaultPolicy.SetAbsoluteExpiration(TimeSpan.FromSeconds(_options.DefaultCacheDurationSeconds));

            await _cache.SetStringAsync(CreateKey(key), result, _defaultPolicy);
        }

        public async Task SetItemAsync<T>(string key, T item, DefaultCachePolicyOptions options) where T : class
        {
            if (null == key)
            {
                throw new ArgumentNullException(nameof(key));
            }

            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentException("Value cannot be empty or a white space", nameof(key));
            }

            if (null == options)
                throw new ArgumentNullException(nameof(options));

            var result = JsonConvert.SerializeObject(new CacheResult<T>(item, true), DefaultSerializerSettings);

            await _cache.SetStringAsync(CreateKey(key), result, options);
        }

        public void RemoveItem(string key)
        {
            if (null == key)
            {
                throw new ArgumentNullException(nameof(key));
            }

            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentException("Value cannot be empty or a white space", nameof(key));
            }

            _cache.Remove(CreateKey(key));
        }

        public async Task RemoveItemAsync(string key)
        {
            if (null == key)
            {
                throw new ArgumentNullException(nameof(key));
            }

            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentException("Value cannot be empty or a white space", nameof(key));
            }

            await _cache.RemoveAsync(CreateKey(key));
        }

    }
}
