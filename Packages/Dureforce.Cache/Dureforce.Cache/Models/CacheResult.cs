﻿using System;

namespace Dureforce.Cache.Models
{
    /// <summary>
    /// Root object in a cache entry to indicate if the item exists and if it does it provides the value. 
    /// </summary>
    /// <typeparam name="T">The type of the actual value to cache</typeparam>
    [Serializable]
    public class CacheResult<T> where T : class
    {
        public bool Exists { get; set; }

        public T CacheItem { get; set; }

        /// <summary>
        /// Initializes a new instance of CacheResult
        /// </summary>
        /// <param name="cacheItem">The value cached value</param>
        /// <param name="exists">Boolean indicating whether the item exists in cache</param>
        public CacheResult(T cacheItem, bool exists)
        {
            Exists = exists;
            CacheItem = cacheItem;
        }
    }
}
