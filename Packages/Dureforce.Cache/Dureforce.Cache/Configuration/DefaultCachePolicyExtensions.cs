﻿using System;

namespace Dureforce.Cache.Configuration
{
    public static class DefaultCachePolicyExtensions
    {
        /// <summary>
        /// Sets an absolute expiration time, relative to now.
        /// </summary>
        /// <param name="options">The options to be operated on.</param>
        /// <param name="relative">The expiration time, relative to now.</param>
        public static DefaultCachePolicyOptions SetAbsoluteExpiration(
            this DefaultCachePolicyOptions options,
            TimeSpan relative)
        {
            options.AbsoluteExpirationRelativeToNow = relative;
            return options;
        }

        /// <summary>
        /// Sets an absolute expiration date for the cache entry.
        /// </summary>
        /// <param name="options">The options to be operated on.</param>
        /// <param name="absolute">The expiration time, in absolute terms.</param>
        public static DefaultCachePolicyOptions SetAbsoluteExpiration(
            this DefaultCachePolicyOptions options,
            DateTimeOffset absolute)
        {
            options.AbsoluteExpiration = absolute;
            return options;
        }

        /// <summary>
        /// Sets how long the cache entry can be inactive (e.g. not accessed) before it will be removed.
        /// This will not extend the entry lifetime beyond the absolute expiration (if set).
        /// </summary>
        /// <param name="options">The options to be operated on.</param>
        /// <param name="offset">The sliding expiration time.</param>
        public static DefaultCachePolicyOptions SetSlidingExpiration(
            this DefaultCachePolicyOptions options,
            TimeSpan offset)
        {
            options.SlidingExpiration = offset;
            return options;
        }
    }
}
