﻿using System;

namespace Dureforce.Cache.Configuration
{
    public class DefaultCachePolicyOptions
    {
        public DateTimeOffset? AbsoluteExpiration
        {
            get; set;
        }
        public TimeSpan? AbsoluteExpirationRelativeToNow { get; set; }

        public TimeSpan? SlidingExpiration { get; set; }
    }
}
