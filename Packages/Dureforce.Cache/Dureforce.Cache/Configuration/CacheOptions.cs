﻿
namespace Dureforce.Cache.Configuration
{
    public class CacheOptions
    {
        public bool Enabled { get; set; } = false;
        public int DefaultCacheDurationSeconds { get; set; } = 1800;
    }
}
