﻿namespace Dureforce.Cache.Group
{
    public interface ICacheGroup
    {
        void AddToGroup(string group, string key);
        void RemoveGroup(string group, string keyToRemove=default!);
    }
}
