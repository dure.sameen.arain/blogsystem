﻿using Dureforce.Cache.Application;
using Dureforce.Cache.Models;
using System.Collections.Generic;

namespace Dureforce.Cache.Group
{
    public class CacheGroup : ICacheGroup
    {
        private readonly ICacheManager _cacheStorage;

        public CacheGroup(ICacheManager cacheStorage)
        {
            _cacheStorage = cacheStorage;

        }

        public void AddToGroup(string group, string key)
        {
            if (!GetGroup(group, out var cacheItem))
            {
                cacheItem.CacheItem.Add(key);
                _cacheStorage.SetItem(group, cacheItem.CacheItem);
            }
            else
            {
                var list = new List<string>() { key };
                _cacheStorage.SetItem(group, list);
            }

        }

        public void RemoveFromGroup(string @group, string key)
        {
            if (GetGroup(@group, out var cacheItem)) return;
            cacheItem.CacheItem.Remove(key);
            _cacheStorage.SetItem(@group, cacheItem.CacheItem);
        }

        public bool GetGroup(string group, out CacheResult<List<string>> cacheItem)
         { 
            var cacheItem1  = _cacheStorage.GetItem<List<string>>(group);

            if (cacheItem1==null || !cacheItem1.Exists)
            {
                cacheItem = default!;
                return true;
            }
            cacheItem = cacheItem1;
            return false;
        }

        public void RemoveGroup(string group, string keyToRemove=default!)
        {
            if (!string.IsNullOrEmpty(keyToRemove))
                _cacheStorage.RemoveItem(keyToRemove);

            if (!GetGroup(group, out var cacheItem))
            {
                cacheItem.CacheItem?.ForEach(item => _cacheStorage.RemoveItem(item));
            }
            _cacheStorage.RemoveItem(group);

        }
    }
}
