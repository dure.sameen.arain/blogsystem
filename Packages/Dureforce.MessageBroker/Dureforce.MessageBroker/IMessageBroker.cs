﻿

using Newtonsoft.Json;

namespace Dureforce.MessageBroker
{
    public interface IMessageBroker
    {
        Task PublishObject<T>(IDistributedMessage<T> distributedMessage, CancellationToken cancellationToken) where T : class;
        Task PublishJson(IDistributedMessage distributedMessage, CancellationToken cancellationToken);
        Task PublishString(IDistributedMessage distributedMessage, CancellationToken cancellationToken);
        Task SendObject<T>(IDistributedMessage<T> distributedMessage, CancellationToken cancellationToken) where T : class;
        Task SendJson(IDistributedMessage distributedMessage, CancellationToken cancellationToken);
        Task SendString(IDistributedMessage distributedMessage, CancellationToken cancellationToken);
        Task StartConsuming(IConsumer consumer);
        Task StopConsuming();
        Task<T> SpecificReceiver<T>(ISingleMessageConsumer consumer);
        Task CreateSubscriber(string topic, string name, string ruleSql);
        Task DeleteSubscriber(string topic, string name);
        void Disconnect();
        void SetMessageBrokerConnectionString(string connectionString);
        void StartTemporaryConfiguration(string connectionstring);
        void StopTemporaryConfiguration();
    }
}
