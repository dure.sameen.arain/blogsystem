﻿ 

namespace Dureforce.MessageBroker.Models
{
    public class GeneralQueueMessage : QueueMessage, IDistributedMessage
    {
        public TimeSpan TimeSpan { get; set; }
        public string Body { get; set; }
    }

    public class GeneralQueueMessage<T> : QueueMessage, IDistributedMessage<T> where T : class
    {
        public TimeSpan TimeSpan { get; set; }
        public T Body { get; set; }
    }
}
