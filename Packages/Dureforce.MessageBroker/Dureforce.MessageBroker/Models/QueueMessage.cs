﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dureforce.MessageBroker.Models
{
    public abstract class QueueMessage
    {
        public GeneralQueue Queue { get; set; }
    }
}
