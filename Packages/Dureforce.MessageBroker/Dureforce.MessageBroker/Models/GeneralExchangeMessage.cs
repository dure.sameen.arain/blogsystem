﻿

namespace Dureforce.MessageBroker.Models
{
    public class GeneralExchangeMessage : ExchangeMessage, IDistributedMessage
    {
        public TimeSpan TimeSpan { get; set; }
        public string Body { get; set; }
    }

    public class GeneralExchangeMessage<T> : ExchangeMessage, IDistributedMessage<T> where T : class
    {
        public TimeSpan TimeSpan { get; set; }
        public T Body { get; set; }
    }
}
