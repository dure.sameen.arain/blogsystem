﻿ 

namespace Dureforce.MessageBroker.Models
{
    public enum ReceiveMode
    {  
         
        PeekLock,
        
        ReceiveAndDelete
    }
}
