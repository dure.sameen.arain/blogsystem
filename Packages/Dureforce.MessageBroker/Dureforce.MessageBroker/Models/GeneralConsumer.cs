﻿using Newtonsoft.Json;

namespace Dureforce.MessageBroker.Models
{
    public class GeneralConsumer : IConsumer
    {
        public IList<string> Queues { get; set; } = new List<string>();
        public Action<string>? Action { get; set; }
    }

    public class GeneralSingleMessageConsumer : ISingleMessageConsumer
    {
        public GeneralSingleMessageConsumer(string entityPath, ReceiveMode receiveMode,   TimeSpan timeout, JsonConverter converter)
        {
            EntityPath = entityPath;
            ReceiveMode = receiveMode; 
            Timeout = timeout;
            Converter = converter;
        }

        public string EntityPath { get; set; } = String.Empty; 
        public ReceiveMode ReceiveMode { get; set; } = ReceiveMode.ReceiveAndDelete; 
        public TimeSpan Timeout { get ; set ; }
        public JsonConverter Converter { get ; set ; }
    }
}
