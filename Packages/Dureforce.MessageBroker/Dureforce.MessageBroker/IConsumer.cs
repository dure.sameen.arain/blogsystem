﻿
using Dureforce.MessageBroker.Models;
using Newtonsoft.Json;

namespace Dureforce.MessageBroker
{
    public interface IConsumer
    {
        IList<string> Queues { get; set; }
        Action<string> Action { get; set; }
    }

    public interface ISingleMessageConsumer
    {
         string EntityPath { get; set; }
        ReceiveMode  ReceiveMode { get; set; } 
        TimeSpan     Timeout { get; set; }
        JsonConverter Converter { get; set; }
    }
}
