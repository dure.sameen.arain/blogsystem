﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dureforce.MessageBroker.Exceptions
{
    public class ConsumingChannelIsNullException : Exception
    {
        public ConsumingChannelIsNullException(string message)
            : base(message)
        {
            //
        }
    }
}
