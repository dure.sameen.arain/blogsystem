﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dureforce.MessageBroker.Exceptions
{
    public class ConsumerActionIsNullException : Exception
    {
        public ConsumerActionIsNullException(string message)
            : base(message)
        {
            //
        }
    }
}
