﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dureforce.MessageBroker.Exceptions
{
    public class QueueIsNullException : Exception
    {
        public QueueIsNullException(string message)
            : base(message)
        {
            //
        }
    }
}
