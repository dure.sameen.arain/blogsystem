﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dureforce.MessageBroker.Exceptions
{
    public class ConsumerMessageException : Exception
    {
        public ConsumerMessageException(string message)
            : base(message)
        {
            //
        }
    }
}
