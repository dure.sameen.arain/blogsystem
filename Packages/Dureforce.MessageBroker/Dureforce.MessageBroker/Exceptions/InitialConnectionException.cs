﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dureforce.MessageBroker.Exceptions
{
    public class InitialConnectionException : Exception
    {
        public int NumberOfRetries { get; set; }

        public InitialConnectionException(string message, Exception innerException)
            : base(message, innerException)
        {
            //
        }
    }
}
