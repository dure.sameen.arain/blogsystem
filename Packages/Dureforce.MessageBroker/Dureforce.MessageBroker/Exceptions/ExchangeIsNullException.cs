﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dureforce.MessageBroker.Exceptions
{
    public class ExchangeIsNullException : Exception
    {
        public ExchangeIsNullException(string message)
            : base(message)
        {
            //
        }
    }
}
