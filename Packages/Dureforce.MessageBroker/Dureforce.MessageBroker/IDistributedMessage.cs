﻿ 
namespace Dureforce.MessageBroker
{
    public interface IDistributedMessage
    {
        string Body { get; set; }
        TimeSpan TimeSpan { get; set; }
    }

    public interface IDistributedMessage<T> where T : class
    {
        T Body { get; set; }
        TimeSpan TimeSpan { get; set; }
    }
}
