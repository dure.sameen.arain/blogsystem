﻿using Dureforce.Database.Rdbms.SqlServer.Extensions;
using Dureforce.Database.Rdbms.SqlServer.Interfaces;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer.Infrastructure.Internal;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Dureforce.Database.Rdbms.SqlServer
{
    public class Database : IRdbmsDatabase
    {//comment
        public string Schema { get; set; }
        public string ConnectionString { get; set; }
        private DbContext _dbContext;
        private readonly IDynamicContextBuilder _dynamicContextBuilder;
        private string _connectionString = string.Empty;
        private SqlConnection _transactionalConnection = null;
        DbContextOptions _options;

        public Database(IDynamicContextBuilder dynamicContextBuilder, DbContextOptions options, Type type)
        {
            _dynamicContextBuilder = dynamicContextBuilder;
            _options = options;
            var dynamicDbContextType = _dynamicContextBuilder.BuildDynamicDbContextType(type);
            _dbContext = _dynamicContextBuilder.CreateDynamicContext(dynamicDbContextType, options);
            ConnectionString = _dbContext.Database.GetConnectionString();
            Schema = string.Empty;
        }

        public Database(IDynamicContextBuilder dynamicContextBuilder, DbContextOptions options, Type type,IDbInitializer dbInitializer, string schema)
        {
            _dynamicContextBuilder = dynamicContextBuilder;
            _options = options;
            var dynamicDbContextType = _dynamicContextBuilder.BuildDynamicDbContextType(type);
            _dbContext = _dynamicContextBuilder.CreateDynamicContext(dynamicDbContextType, options);
            dbInitializer?.Initialize(_dbContext);
            ConnectionString = _dbContext.Database.GetConnectionString();
            Schema = schema;
        }

        public Database(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<T> Query<T>(Expression<Func<T, bool>> expression = null) where T : class
        {
            IQueryable<T> baseQuery = _dbContext.Set<T>().AsNoTracking();

            if (expression == null)
            {
                return baseQuery.AsQueryable();
            }

            return baseQuery.Where(expression).AsQueryable();
        }

        public async Task<T> Find<T>(Expression<Func<T, bool>> predicate) where T : class
        {
            IQueryable<T> query = _dbContext.Set<T>();

            var entity = await query.FirstOrDefaultAsync(predicate);
            return entity;
        }

        public async Task<T> GetById<T>(string id) where T : class
        {
            throw new NotImplementedException();
        }

        public async Task Create<T>(T entity) where T : class
        {
            await _dbContext.Set<T>().AddAsync(entity);
        }

        public async Task Update<T>(T entity) where T : class
        {
            await Task.CompletedTask;

            _dbContext.Set<T>().Update(entity);
        }

        public async Task Delete<T>(T entity) where T : class
        {
            await Task.CompletedTask;

            _dbContext.Set<T>().Remove(entity);
        }

        public async Task SaveChanges()
        {
            await _dbContext.SaveChangesAsync();
        }

        public DataSet RunSavedQuery(string storedProcedure, object[] parameters, IDbTransaction transaction = null)
        {
            CommandType type;
            string query = SetQuerySchema(storedProcedure, out type);
            IList<SqlParameter> filters = GetFilters(parameters);

            var tran = transaction as SqlTransaction;

            return (transaction == null) ? Query(query, filters, type) : QueryWithTransaction(query, filters, tran);
        }

        public async Task<DataSet> RunSavedQueryAsync(string storedProcedure, object[] parameters, IDbTransaction transaction = null)
        {
            CommandType type;
            string query = SetQuerySchema(storedProcedure, out type);
            IList<SqlParameter> filters = GetFilters(parameters);

            var tran = transaction as SqlTransaction;

            return (transaction == null) ? await QueryAsync(query, filters, type) : await QueryWithTransactionAsync(query, filters, tran);
        }

        #region private methods
        private DataSet Query(string query, IList<SqlParameter> parameters, CommandType type)
        {
            DataSet dataSet = new DataSet();
            var sqlServerOptionsExtension =
                   _options.FindExtension<SqlServerOptionsExtension>();
            if (sqlServerOptionsExtension != null)
            {
                _connectionString = sqlServerOptionsExtension.ConnectionString;
            }

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand(query, connection);
                cmd.CommandType = type;
                if (parameters !=null)
                    cmd.Parameters.AddRange(parameters.ToArray());
                using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                {
                    adapter.Fill(dataSet);
                }
            }

            return dataSet;
        }

        private DataSet QueryWithTransaction(string query, IList<SqlParameter> parameters, SqlTransaction transaction)
        {
            DataSet dataSet = new DataSet();

            _transactionalConnection = new SqlConnection(_connectionString);

            _transactionalConnection.Open();

            _transactionalConnection.BeginTransaction();

            using (SqlDataAdapter adapter = new SqlDataAdapter(query, _transactionalConnection))
            {
                adapter.SelectCommand.Transaction = transaction;

                if (parameters != null && parameters.Any())
                {
                    adapter.SelectCommand.Parameters.AddRange(parameters.ToArray());
                }

                adapter.Fill(dataSet);
                transaction.Commit();
            }

            return dataSet;
        }

        #region Query async method
        private async Task<DataSet> QueryAsync(string query, IList<SqlParameter> parameters, CommandType type)
        {
            DataSet dataSet = new DataSet();
            var sqlServerOptionsExtension =
                   _options.FindExtension<SqlServerOptionsExtension>();
            if (sqlServerOptionsExtension != null)
            {
                _connectionString = sqlServerOptionsExtension.ConnectionString;
            }

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand(query, connection);
                cmd.CommandType = type;
                if (parameters != null)
                    cmd.Parameters.AddRange(parameters.ToArray());

                using (var reader = await cmd.ExecuteReaderAsync())
                {
                    while (!reader.IsClosed)
                    {
                        DataTable dataTable = new DataTable();
                        dataTable.Load(reader);
                        dataSet.Tables.Add(dataTable);
                    }
                }
            }

            return dataSet;
        }

        private async Task<DataSet> QueryWithTransactionAsync(string query, IList<SqlParameter> parameters, SqlTransaction transaction)
        {
            DataSet dataSet = new DataSet();

            _transactionalConnection = new SqlConnection(_connectionString);

            _transactionalConnection.Open();

            _transactionalConnection.BeginTransaction();

            SqlCommand cmd = new SqlCommand(query, _transactionalConnection);
            //cmd.CommandType = type;
            if (parameters != null)
                cmd.Parameters.AddRange(parameters.ToArray());

            cmd.Transaction = transaction;

            using (var reader = await cmd.ExecuteReaderAsync())
            {
                while (!reader.IsClosed)
                {
                    DataTable dataTable = new DataTable();
                    dataTable.Load(reader);
                    dataSet.Tables.Add(dataTable);
                }
                transaction.Commit();
            }

            return dataSet;
        }

        #endregion

        private string SetQuerySchema(string storedProcedure, out CommandType type)
        {
            type = CommandType.Text;
            string schemaPhrase = string.IsNullOrEmpty(Schema) ? "" : $"[{Schema}].";
            string query = String.Empty;
            if (storedProcedure.ToLower().StartWithAny(new List<string>() { "select", "insert", "update", "delete" }))
            {
                query = storedProcedure.Replace("dbo.", schemaPhrase);
            }
            else
            {
                var tmp = storedProcedure.Contains('.') ? "" : $"{schemaPhrase}.";
                query = $"{tmp}{storedProcedure}";
                type = CommandType.StoredProcedure;
            }
            return query;
        }

        private IList<SqlParameter> GetFilters(object[] parameters)
        {
            IList<SqlParameter> filters = null;

            if (parameters != null && parameters.Any())
            {
                filters = new List<SqlParameter>();
                bool firstParameter = true;

                for (int counter = 0; counter < parameters.Length; counter += 2)
                {
                    string parameterName = parameters[counter] as string;
                    object parameterValue = parameters[counter + 1];

                    filters.Add(new SqlParameter(parameterName, parameterValue));
                    //if (type != CommandType.StoredProcedure)
                    //{
                    //    query += (firstParameter ? " " : ", ") + parameterName;
                    //}
                    if (firstParameter)
                    {
                        firstParameter = false;
                    }
                }
            }
            return filters;
        }
        #endregion
    }
}
