﻿using Dureforce.Database.Rdbms.SqlServer.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;

namespace Dureforce.Database.Rdbms.SqlServer.Extensions
{
    public class ContextFactory : IContextFactory
    {
        public TContext Create<TContext>(DbContextOptions<TContext> options) where TContext : DbContext
        {
            return (TContext)Activator.CreateInstance(typeof(TContext), options);
        }

        public TContext Create<TContext>(string connectionString) where TContext : DbContext
        {
            var optionsBuilder = new DbContextOptionsBuilder<TContext>();
            optionsBuilder.UseSqlServer(connectionString);
            return (TContext)Activator.CreateInstance(typeof(TContext), optionsBuilder.Options);
        }
    }
}
