﻿using Microsoft.EntityFrameworkCore.Diagnostics;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;

namespace Dureforce.Database.Rdbms.SqlServer.Extensions
{
    public class QueryCommandInterceptor : DbCommandInterceptor
    {
        private string _schema { get; set; }

        public QueryCommandInterceptor(string schema)
        {
            _schema = schema;
        }

        public override InterceptionResult<int> NonQueryExecuting(
           DbCommand command,
           CommandEventData eventData,
           InterceptionResult<int> result)
        {
            // TODO: Add your business logic
            // var dbName = connection.Database;
            // var commandText = command.CommandText;
            return result;
        }

        public override ValueTask<InterceptionResult<int>> NonQueryExecutingAsync(
            DbCommand command,
            CommandEventData eventData,
            InterceptionResult<int> result,
            CancellationToken cancellationToken = default)
        {
            // TODO: Add your business logic
            // var dbName = connection.Database;
            // var commandText = command.CommandText;
            return new ValueTask<InterceptionResult<int>>(result);
        }

        public override InterceptionResult<DbDataReader> ReaderExecuting(
            DbCommand command,
            CommandEventData eventData,
            InterceptionResult<DbDataReader> result)
        {
            ReplaceSchema(command, eventData);
            return result;
        }

        public override ValueTask<InterceptionResult<DbDataReader>> ReaderExecutingAsync(
            DbCommand command,
            CommandEventData eventData,
            InterceptionResult<DbDataReader> result,
            CancellationToken cancellationToken = default)
        {
            ReplaceSchema(command, eventData);
            return new ValueTask<InterceptionResult<DbDataReader>>(result);
        }

        public override InterceptionResult<object> ScalarExecuting(
           DbCommand command,
           CommandEventData eventData,
           InterceptionResult<object> result)
        {
            ReplaceSchema(command, eventData);
            return result;
        }

        public override ValueTask<InterceptionResult<object>> ScalarExecutingAsync(
            DbCommand command,
            CommandEventData eventData,
            InterceptionResult<object> result,
            CancellationToken cancellationToken = default)
        {
            ReplaceSchema(command, eventData);
            return new ValueTask<InterceptionResult<object>>(result);
        }

        #region Private Method
        private void ReplaceSchema(DbCommand command, CommandEventData eventData)
        {
            if (eventData.CommandSource == CommandSource.LinqQuery)
            {
                command.CommandText = command.CommandText.Replace("FROM [", $"FROM [{_schema}].[");
            }
            else if (eventData.CommandSource == CommandSource.SaveChanges && command.CommandText.Contains("INTO"))
            {
                command.CommandText = command.CommandText.Replace("INTO ", $"INTO [{_schema}].");

                //Case: Primary key INT
                //SELECT [Id] FROM [TableName]
                if (command.CommandText.Contains("FROM"))
                {
                    command.CommandText = command.CommandText.Replace("FROM ", $"FROM [{_schema}].");
                }
            }
            else if (eventData.CommandSource == CommandSource.SaveChanges && command.CommandText.Contains("UPDATE"))
            {
                command.CommandText = command.CommandText.Replace("UPDATE ", $"UPDATE [{_schema}].");
            }
            else if (eventData.CommandSource == CommandSource.SaveChanges && command.CommandText.Contains("DELETE FROM"))
            {
                command.CommandText = command.CommandText.Replace("FROM ", $"FROM [{_schema}].");
            }
        }
        #endregion
    }
}
