﻿using Dureforce.Database.Rdbms.SqlServer.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Dureforce.Database.Rdbms.SqlServer.Extensions
{
    public static class DependencyResolverDatabaseExtensions
    {
        public static IServiceCollection UseSqlServerDatabase(this IServiceCollection services, string connectionString, Type type, string schema, IDbInitializer dbInitializer)
        {
            var builder = new DbContextOptionsBuilder();

            if (!string.IsNullOrEmpty(schema) && !schema.Equals("dbo"))
            {
                builder.UseSqlServer(connectionString)
                    .AddInterceptors(new QueryCommandInterceptor(schema));
            }
            else
            {
                builder.UseSqlServer(connectionString);
            }

            AddServices(services, type, schema, builder,dbInitializer);
            return services;
        }
        public static IServiceCollection UseSqlLiteDatabase(this IServiceCollection services, string connectionString, Type type, IDbInitializer dbInitializer)
        {
            var builder = new DbContextOptionsBuilder(); 
                builder.UseSqlite(connectionString); 
            AddServices(services, type, "dbo", builder, dbInitializer);

            return services;
        }

        private static void AddServices(IServiceCollection services, Type type, string schema, DbContextOptionsBuilder builder, IDbInitializer dbInitializer)
        {
            services.AddScoped<IDynamicContextBuilder, DynamicContextBuilder>();
            services.AddScoped<IRdbmsDatabase>(ctx => new Database(ctx.GetService<IDynamicContextBuilder>(), builder.Options, type,dbInitializer, schema));
        }
    }
}
