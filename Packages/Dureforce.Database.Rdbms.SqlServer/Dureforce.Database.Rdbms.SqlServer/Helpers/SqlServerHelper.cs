﻿using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Dureforce.Database.Rdbms.SqlServer.Helpers;

public class SqlServerHelper
{
    private readonly string _connectionString = string.Empty;

    public SqlServerHelper()
    {
    }

    public SqlServerHelper(string connectionString)
    {
        _connectionString = connectionString;
    }

    public DataTable ExecuteQuery(string query, object[] parameters = null)
    {
        DataTable dataTable = new DataTable();

        using (SqlConnection connection = new SqlConnection(_connectionString))
        {
            connection.Open();

            using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
            {
                if (parameters != null && parameters.Any())
                {
                    adapter.SelectCommand.Parameters.AddRange(BuildParameters(parameters).ToArray());
                }

                adapter.Fill(dataTable);
            }
        }

        return dataTable;
    }

    public int ExecuteNonQuery(string query, object[] parameters = null)
    {
        int affectedRows = 0;

        using (SqlConnection connection = new SqlConnection(_connectionString))
        {
            connection.Open();

            using (SqlCommand command = new SqlCommand())
            {
                command.Connection = connection;
                command.CommandText = query;

                if (parameters != null && parameters.Any())
                {
                    command.Parameters.AddRange(BuildParameters(parameters).ToArray());
                }

                affectedRows = command.ExecuteNonQuery();
            }

            connection.Close();
        }

        return affectedRows;
    }

    public DataTable ExecuteQuery(string query, SqlConnection connection, SqlTransaction transaction, object[] parameters = null)
    {
        var dataTable = new DataTable();

        using (SqlDataAdapter adapter = new SqlDataAdapter(query, connection))
        {
            adapter.SelectCommand.Transaction = transaction;

            if (parameters != null && parameters.Any())
            {
                adapter.SelectCommand.Parameters.AddRange(BuildParameters(parameters).ToArray());
            }

            adapter.Fill(dataTable);
        }

        return dataTable;
    }

    public int ExecuteNonQuery(string query, SqlConnection connection, SqlTransaction transaction, object[] parameters = null)
    {
        var command = new SqlCommand(query, connection, transaction);

        if (parameters != null && parameters.Any())
        {
            command.Parameters.AddRange(BuildParameters(parameters).ToArray());
        }

        var affectedRows = command.ExecuteNonQuery();

        return affectedRows;
    }

    private IList<SqlParameter> BuildParameters(object[] parameters)
    {
        IList<SqlParameter> filters = null;

        if (parameters != null && parameters.Any())
        {
            filters = new List<SqlParameter>();

            for (int counter = 0; counter < parameters.Length; counter += 2)
            {
                string parameterName = parameters[counter] as string;
                object parameterValue = parameters[counter + 1];

                filters.Add(new SqlParameter(parameterName, parameterValue));
            }
        }

        return filters;
    }


}
