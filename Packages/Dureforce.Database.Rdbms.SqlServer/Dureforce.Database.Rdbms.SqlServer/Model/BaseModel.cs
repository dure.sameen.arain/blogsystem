﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Dureforce.Database.Rdbms.SqlServer.Model
{
    public class BaseModel
    {
        [Key]
        public int Id { get; set; }
    }
}