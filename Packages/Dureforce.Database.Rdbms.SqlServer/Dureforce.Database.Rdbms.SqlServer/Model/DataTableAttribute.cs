﻿using System;

namespace Dureforce.Database.Rdbms.SqlServer.Model
{
    [AttributeUsage(AttributeTargets.Class)]
    public class DataTableAttribute : Attribute
    {
    }
}
