﻿using Microsoft.EntityFrameworkCore;

namespace Dureforce.Database.Rdbms.SqlServer.Interfaces
{
    public interface IContextFactory
    {
        TContext Create<TContext>(string connectionString) where TContext : DbContext;
        TContext Create<TContext>(DbContextOptions<TContext> options) where TContext : DbContext;
    }
}
