﻿using Microsoft.EntityFrameworkCore;
 

namespace Dureforce.Database.Rdbms.SqlServer.Interfaces
{
    public interface IDbInitializer
    {
        string Schema { get; set; }
        void Initialize(DbContext dbContext);
    }
}
