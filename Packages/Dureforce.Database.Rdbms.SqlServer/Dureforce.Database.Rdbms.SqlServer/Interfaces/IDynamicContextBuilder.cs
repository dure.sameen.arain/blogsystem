﻿using Microsoft.EntityFrameworkCore;
using System;

namespace Dureforce.Database.Rdbms.SqlServer.Interfaces
{
    public interface IDynamicContextBuilder
    {
        Type BuildDynamicDbContextType(Type type);
        DbContext CreateDynamicContext(Type dynDbContextType, DbContextOptions options);
    }
}