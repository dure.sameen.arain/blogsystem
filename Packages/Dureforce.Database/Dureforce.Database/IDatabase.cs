﻿using System.Data;
using System.Linq.Expressions;

namespace Dureforce.Database
{
    public interface IDatabase
    {
        IQueryable<T> Query<T>(Expression<Func<T, bool>> expression = default!) where T : class;
        Task<T> Find<T>(Expression<Func<T, bool>> predicate) where T : class;
        Task<T> GetById<T>(string id) where T : class;
        Task Create<T>(T entity) where T : class;
        Task Update<T>(T entity) where T : class;
        Task Delete<T>(T entity) where T : class;
        DataSet RunSavedQuery(string queryname, object[] parameters, IDbTransaction transaction = default!);
        Task<DataSet> RunSavedQueryAsync(string queryname, object[] parameters, IDbTransaction transaction = default!);
        string ConnectionString { get; set; }
    }
}
