﻿namespace Dureforce.Database.Models
{
    public class QueryFilter
    {
        public string PropertyName { get; set; }
        public string PropertyValue { get; set; }
        public string PropertyValues { get; set; }
        public QueryFilterType FilterType { get; set; } = QueryFilterType.Text;
        public QueryFilterOperator FilterOperator { get; set; } = QueryFilterOperator.Equals;
        public QueryFilterMode FilterMode { get; set; } = QueryFilterMode.And;
    }
}
