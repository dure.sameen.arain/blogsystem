﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dureforce.Database.Models
{
    public interface IDataModel<T>
    {
        T Id { get; set; }
    }
}
