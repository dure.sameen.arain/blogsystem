﻿namespace Dureforce.Database.Models
{
    public class SortSettings
    {
        public string Key { get; private set; }
        public SortMode SortMode { get; private set; }

        public SortSettings(string key, SortMode sortMode)
        {
            Key = key;
            SortMode = sortMode;
        }
    }
}
