﻿namespace Dureforce.Database.Models
{
    public enum QueryFilterType
    {
        Text,
        Number,
        Date,
        Boolean
    }
}
