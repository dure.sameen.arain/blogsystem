﻿namespace Dureforce.Database.Models
{
    public enum QueryFilterOperator
    {
        Equals,
        NotEquals,
        GreaterThan,
        GreaterThanEqualsTo,
        LessThan,
        LessThanEqualsTo,
        StartsWith,
        EndsWith,
        Contains,
        NotContains,
        In
    }
}
