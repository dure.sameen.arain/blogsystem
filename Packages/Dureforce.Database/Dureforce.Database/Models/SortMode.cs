﻿namespace Dureforce.Database.Models
{
    public enum SortMode
    {
        Ascending = 1,
        Descending = -1
    }
}
