﻿namespace Dureforce.Database.Models
{
    public class DatabaseSettings
    {
        public bool SupportSavedQuery { get; set; }
        public bool SupportPartitionKey { get; set; }

    }
}
