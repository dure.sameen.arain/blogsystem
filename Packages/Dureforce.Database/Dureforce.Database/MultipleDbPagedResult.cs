﻿namespace Dureforce.Database
{
    public class MultipleDbPagedResult<TDocument> where TDocument : class
    {
        public int CurrentPage { get; private set; }
        public int PageSize { get; private set; }
        public long DocumentsCount { get; private set; }
        public int TotalPages { get; private set; }
        public Dictionary<string, List<TDocument>> Result { get; private set; }
        public string Query { get; private set; }

        public MultipleDbPagedResult(int currentPage, int pageSize, long documentsCount, int totalPages, Dictionary<string, List<TDocument>> documents, string query)
        {
            CurrentPage = currentPage;
            PageSize = pageSize;
            DocumentsCount = documentsCount;
            TotalPages = totalPages;
            Result = documents;
            Query = query;
        }
    }
}

