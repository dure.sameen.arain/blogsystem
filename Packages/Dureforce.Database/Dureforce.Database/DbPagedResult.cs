﻿namespace Dureforce.Database
{
    public class DbPagedResult<TDocument> where TDocument : class
    {
        public int CurrentPage { get; private set; }
        public int PageSize { get; private set; }
        public long DocumentsCount { get; private set; }
        public int TotalPages { get; private set; }
        public IEnumerable<TDocument> Result { get; private set; }
        public string Query { get; private set; }

        public DbPagedResult(int currentPage, int pageSize, long documentsCount, int totalPages, IEnumerable<TDocument> documents, string  query)
        {
            CurrentPage = currentPage;
            PageSize = pageSize;
            DocumentsCount = documentsCount;
            TotalPages = totalPages;
            Result = documents;
            Query = query; 
        }
    }
}
