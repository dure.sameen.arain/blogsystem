﻿using System;
using System.Linq;
using System.Reflection;
using Dureforce.Database.Attributes;

namespace Dureforce.Database.Helpers
{
    public static class ReflectionExtensions
    {
        public static object GetPrimaryId<T>(this T obj)
        {
            var prop = GetPropertyInfo<T>(typeof(PrimaryId));

            object ret = prop != null ? prop?.GetValue(obj, null) : null;

            return ret;
        }

        public static string GetPropertyNameByAttribute<T>(this Type attributeType)
        {
            PropertyInfo propertyInfo = GetPropertyInfo<T>(attributeType);

            string propertyName = (propertyInfo != null) ? propertyInfo.Name : string.Empty;

            return propertyName;
        }

        public static object GetPropertyValueByAttribute<T>(this T @object, Type attributeType)
        {
            PropertyInfo propertyInfo = GetPropertyInfo<T>(attributeType);

            object @value = (propertyInfo != null) ? propertyInfo?.GetValue(@object, null) : null;

            return @value;
        }

        public static string GetId<T>(this T obj)
        {
            var prop = GetPropertyInfo(obj, typeof(PrimaryId));

            object ret = prop != null ? prop?.GetValue(obj, null) : null;

            return ret.ToString();
        }

        public static string GetPartitionKeyValue<T>(this T obj, Type attribute)
        {
            var prop = GetPropertyInfo(obj, attribute);

            object ret = prop != null ? prop?.GetValue(obj, null) : null;

            return ret.ToString();
        }

        public static string GetPartitionKey<T>(this T obj, Type attribute)
        {
            var prop = GetPropertyInfo(obj, attribute);

            return "/" + LowerCaseFirst(prop.Name);
        }

        public static string LowerCaseFirst(this string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            char[] a = s.ToCharArray();

            a[0] = char.ToLower(a[0]);

            return new string(a);
        }

        private static PropertyInfo GetPropertyInfo<T>(Type attributeType)
        {
            return typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .FirstOrDefault(p => p.GetCustomAttributes(attributeType, false).Count() == 1);
        }

        private static PropertyInfo GetPropertyInfo(object obj, Type attributeType)
        {
            return obj.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance)
                       .FirstOrDefault(p => p.GetCustomAttributes(attributeType, false).Count() == 1);
        }
    }
}
