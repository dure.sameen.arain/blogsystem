﻿using System;

namespace Dureforce.Database.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class PrimaryId : System.Attribute
    {
        public PrimaryId()
        {
            //
        }
    }
}
