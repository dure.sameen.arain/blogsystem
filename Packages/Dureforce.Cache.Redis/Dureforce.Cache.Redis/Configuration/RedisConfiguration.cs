﻿namespace Dureforce.Cache.Redis.Configuration
{
    /// <summary>
    /// configuration for use with reddis
    /// </summary>
    public class RedisConfiguration
    {
        /// <summary>
        /// Azure Cache for redis connection string
        /// </summary>
        public string ConnectionString { get; set; } 
        /// <summary>
        /// The name of the host the redis server is running on.
        /// </summary>
        public string HostName { get; set; }

        /// <summary>
        /// The portnumber to use for accessing the redis server
        /// </summary>
        public string PortNr { get; set; }

        /// <summary>
        /// Name of the instance to use
        /// </summary>
        public string InstanceName { get; set; }

        /// <summary>
        /// (Optional) the username for authentication purpose
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// The password for authentication purpose
        /// </summary>
        public string Password { get; set; }

        public bool EnableAuthentication { get; set; }
    }
}
