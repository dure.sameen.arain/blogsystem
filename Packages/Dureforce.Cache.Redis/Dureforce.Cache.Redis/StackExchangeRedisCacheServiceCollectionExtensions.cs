// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.

using Dureforce.Cache.Configuration;
using Dureforce.Cache.Distributed;
using Dureforce.Cache.Handler;
using Dureforce.Cache.Redis.Configuration;
using Dureforce.Cache.Redis.Distributed;
using Microsoft.Extensions.DependencyInjection;
using StackExchange.Redis;
using System;

namespace Dureforce.Cache.Redis
{
    /// <summary>
    /// Extension methods for setting up Redis distributed cache related services in an <see cref="IServiceCollection" />.
    /// </summary>
    public static class StackExchangeRedisCacheServiceCollectionExtensions
    {
        /// <summary>
        /// Adds Redis distributed caching services to the specified <see cref="IServiceCollection" />.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection" /> to add services to.</param>
        /// <param name="setupAction">An <see cref="Action{RedisCacheOptions}"/> to configure the provided
        /// <see cref="RedisCacheOptions"/>.</param>
        /// <returns>The <see cref="IServiceCollection"/> so that additional calls can be chained.</returns>
        public static IServiceCollection AddStackExchangeRedisCache(this IServiceCollection services, Action<RedisCacheOptions> setupAction)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            if (setupAction == null)
            {
                throw new ArgumentNullException(nameof(setupAction));
            }

            services.AddOptions();
            services.Configure(setupAction);
            services.AddSingleton<IDistributedCache, RedisCache>();
            
            return services;
        }

        /// <summary>
        /// Adds <see cref="RedisConfiguration"/> and the <see cref="RedisConfiguration"/> that is under the Cache
        /// </summary>
        /// <param name="services"></param>
        /// <param name="redisConfiguration"></param>
        /// <param name="cacheOptions"></param>
        public static void AddRedisCaching(this IServiceCollection services, RedisConfiguration redisConfiguration, CacheOptions cacheOptions)
        {
            services.AddStackExchangeRedisCache(options =>
            {
                if (redisConfiguration.EnableAuthentication)
                {
                    options.ConfigurationOptions.Password =
                        redisConfiguration.Password;
                }

                options.InstanceName = redisConfiguration.InstanceName;
                options.Configuration = (!string.IsNullOrEmpty(redisConfiguration.ConnectionString)) ? redisConfiguration.ConnectionString : $"{redisConfiguration.HostName}:{redisConfiguration.PortNr}";
            });
            
            services.AddCaching(cacheOptions);

            services.AddSingleton(x => ConnectionMultiplexer.Connect(redisConfiguration.ConnectionString).GetDatabase());
            services.AddSingleton(x => {
                var connection = ConnectionMultiplexer.Connect(redisConfiguration.ConnectionString + ",allowadmin=true");

                var endpoint = connection.GetEndPoints().FirstOrDefault();
                return connection.GetServer(endpoint);

            });
            services.AddSingleton<ICacheHandlerCustomMethods, RedisCacheCustomMethods>();

        }
    }
}