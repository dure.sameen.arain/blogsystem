﻿using Dureforce.Cache.Handler;
using StackExchange.Redis;

namespace Dureforce.Cache.Redis.Distributed
{
    public class RedisCacheCustomMethods : ICacheHandlerCustomMethods
    {
        private readonly IDatabase _cacheDatabase;
        private readonly IServer _server;
        public RedisCacheCustomMethods(IDatabase cacheDatabase, IServer server)
        {
            _cacheDatabase = cacheDatabase;
            _server = server;
        }

        public void FlushDb()
        {
            var databaseId = _cacheDatabase.Database;
            _server.FlushDatabase(databaseId);
        }

        public bool ClearCache(string cacheKey)
        {

            var key = new RedisKey();
            key.Append(cacheKey);
            return _cacheDatabase.KeyDelete(key);


        }
    }
}
