﻿using Dureforce.MessageBroker.AzureServiceBus.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Dureforce.MessageBroker.AzureServiceBus.Extensions
{
    public static class DependencyResolverAzureServiceBusExtensions
    {
        public static IServiceCollection AddAzureServiceBusMessaging(this IServiceCollection services, AzureServiceBusSettings settings)
        {
            services.AddScoped<IMessageBroker>(s => new AzureServiceBusMessageBroker(settings));

            return services;
        }
    }
}
