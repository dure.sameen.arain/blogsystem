﻿using Azure.Messaging.ServiceBus;
using Azure.Messaging.ServiceBus.Administration;
using Dureforce.MessageBroker.AzureServiceBus.Configuration;
using Dureforce.MessageBroker.Exceptions;
using Dureforce.MessageBroker.Models;
using Newtonsoft.Json;
using System.Text;

namespace Dureforce.MessageBroker.AzureServiceBus
{
    public sealed class AzureServiceBusMessageBroker : IMessageBroker, IDisposable, IAsyncDisposable
    {
        private readonly AzureServiceBusSettings _azureServiceBusSettings;
        private ServiceBusClient _serviceBusClient;
        private IEnumerable<ServiceBusProcessor> _consumers = new List<ServiceBusProcessor>();
        private bool _consuming = false;
        private Action<string>? _action;
        private bool _isDisposed = false;
        private string _connectionString;

        public AzureServiceBusMessageBroker(AzureServiceBusSettings settings)
        {
            _azureServiceBusSettings = settings;
            _connectionString = _azureServiceBusSettings.ConnectionString;
            _serviceBusClient = new ServiceBusClient(_azureServiceBusSettings.ConnectionString);
        }

        public void SetMessageBrokerConnectionString(string connectionString)
        {
            AzureServiceBusSettings settings = new AzureServiceBusSettings
            {
                ConnectionString = connectionString
            };

            _serviceBusClient = new ServiceBusClient(settings.ConnectionString);
        }

        public async Task PublishObject<T>(IDistributedMessage<T> distributedMessage, CancellationToken cancellationToken) where T : class
        {
            await SendObjectToTopicOrQueue<T>(distributedMessage, cancellationToken).ConfigureAwait(false);
        }

        public async Task PublishJson(IDistributedMessage distributedMessage, CancellationToken cancellationToken)
        {
            await SendStringToTopicOrQueue(distributedMessage, cancellationToken).ConfigureAwait(false);
        }

        public async Task PublishString(IDistributedMessage distributedMessage, CancellationToken cancellationToken)
        {
            await SendStringToTopicOrQueue(distributedMessage, cancellationToken).ConfigureAwait(false);
        }

        public async Task SendObject<T>(IDistributedMessage<T> distributedMessage, CancellationToken cancellationToken) where T : class
        {
            await SendObjectToTopicOrQueue<T>(distributedMessage, cancellationToken).ConfigureAwait(false);
        }

        public async Task SendJson(IDistributedMessage distributedMessage, CancellationToken cancellationToken)
        {
            await SendStringToTopicOrQueue(distributedMessage, cancellationToken).ConfigureAwait(false);
        }

        public async Task SendString(IDistributedMessage distributedMessage, CancellationToken cancellationToken)
        {
            await SendStringToTopicOrQueue(distributedMessage, cancellationToken).ConfigureAwait(false);
        }

        public async Task StartConsuming(IConsumer consumer)
        {
            ValidateConsumer(consumer);

            if (_consuming)
            {
                return;
            }

            _consuming = true;
            _action = consumer.Action;
            IList<ServiceBusProcessor> tags = new List<ServiceBusProcessor>();

            foreach (string channel in consumer.Queues)
            {
                ServiceBusProcessor serviceBusProcessor = _serviceBusClient.CreateProcessor(channel);
                serviceBusProcessor.ProcessMessageAsync += ConsumerOnReceived;
                serviceBusProcessor.ProcessErrorAsync += ConsumerOnReceivedError;

                await serviceBusProcessor.StartProcessingAsync();

                tags.Add(serviceBusProcessor);
            }

            _consumers = tags;
        }

        public async Task StopConsuming()
        {
            if (!_consuming)
            {
                return;
            }

            if (_consumers.Any())
            {
                foreach (ServiceBusProcessor serviceBusProcessor in _consumers)
                {
                    await serviceBusProcessor.StopProcessingAsync();

                    serviceBusProcessor.ProcessMessageAsync -= ConsumerOnReceived;
                    serviceBusProcessor.ProcessErrorAsync -= ConsumerOnReceivedError;

                    await serviceBusProcessor.DisposeAsync();
                }
            }

            _consuming = false;
        }

        public void Disconnect()
        {
            Dispose();
        }

        public void Dispose()
        {
            Task.Run(async () => await DisposeAsync());
        }

        public async ValueTask DisposeAsync()
        {
            if (!_isDisposed)
            {
                _isDisposed = true;

                await _serviceBusClient.DisposeAsync();
            }
        }

        private async Task SendObjectToTopicOrQueue<T>(IDistributedMessage<T> distributedMessage, CancellationToken cancellationToken) where T : class
        {
            GeneralQueueMessage<T> message = distributedMessage as GeneralQueueMessage<T>;

            ValidateProducingChannel(message);

            string channel = (!string.IsNullOrEmpty(message.Queue.Exchange)) ? message.Queue.Exchange : message.Queue.Name;
            string body = JsonConvert.SerializeObject (message.Body );

            await Send(channel, body, message.Queue.Label, message.Queue.Arguments, message.TimeSpan, cancellationToken).ConfigureAwait(false);
        }

        private async Task SendStringToTopicOrQueue(IDistributedMessage distributedMessage, CancellationToken cancellationToken)
        {
            GeneralQueueMessage message = distributedMessage as GeneralQueueMessage;

            ValidateProducingChannel(message);

            string channel = (!string.IsNullOrEmpty(message.Queue.Exchange)) ? message.Queue.Exchange : message.Queue.Name;

            await Send(channel, message.Body, message.Queue.Label, message.Queue.Arguments, message.TimeSpan , cancellationToken).ConfigureAwait(false);
        }

        private async Task Send(string channel, string body, string subject, IDictionary<string, object> customProperties,TimeSpan timeSpan, CancellationToken cancellationToken)
        {
            ServiceBusSender serviceBusSender = _serviceBusClient.CreateSender(channel);
            ServiceBusMessage serviceBusMessage = new(body)
            {
                Subject = subject
            };
            if (customProperties != null && customProperties.Any())
            {
                foreach (string key in customProperties.Keys)
                {
                    serviceBusMessage.ApplicationProperties.Add(key, customProperties[key]);
                }
            }
            if (timeSpan.Ticks !=0)
            {
                serviceBusMessage.ScheduledEnqueueTime = DateTime.UtcNow.AddTicks(timeSpan.Ticks);
            }
            await serviceBusSender.SendMessageAsync(serviceBusMessage, cancellationToken).ConfigureAwait(false);
            await serviceBusSender.DisposeAsync();
        }

        private async Task ConsumerOnReceived(ProcessMessageEventArgs eventArgs)
        {
            string messageFromQueue = eventArgs.Message.Body.ToString();

            _action(messageFromQueue);
            await eventArgs.CompleteMessageAsync(eventArgs.Message);
        }

        private async Task ConsumerOnReceivedError(ProcessErrorEventArgs eventArgs)
        {
            string exceptionMessage = eventArgs.Exception.Message;
            string innerExceptionMessage = (eventArgs.Exception.InnerException != null) ? eventArgs.Exception.InnerException.Message : string.Empty;

            throw new ConsumerMessageException(string.Format("{0} {1}", exceptionMessage, innerExceptionMessage));
        }

        private void ValidateProducingChannel(GeneralQueueMessage message)
        {
            if (message is null)
            {
                throw new ProducingChannelIsNullException("Producing channel is null.");
            }

            ValidateTopicAndQueue(message.Queue);
        }

        private void ValidateProducingChannel<T>(GeneralQueueMessage<T> message) where T : class
        {
            if (message is null)
            {
                throw new ProducingChannelIsNullException("Producing channel is null.");
            }

            ValidateTopicAndQueue(message.Queue);
        }

        private void ValidateTopicAndQueue(GeneralQueue queue)
        {
            if (queue is null)
            {
                throw new ProducingChannelIsNullException("Producing channel is null.");
            }

            if (string.IsNullOrEmpty(queue.Exchange) && string.IsNullOrEmpty(queue.Name))
            {
                throw new ProducingChannelIsNullException("Topic or Queue name is missing.");
            }
        }

        private void ValidateConsumer(IConsumer consumer)
        {
            if (consumer is null)
            {
                throw new ConsumerIsNullException("Consumer is null.");
            }

            if (consumer.Queues.Count == 0)
            {
                throw new QueueIsNullException("Consumer queues are empty.");
            }

            if (consumer.Action is null)
            {
                throw new ConsumerActionIsNullException("Consumer action is null.");
            }
        }

        public void StartTemporaryConfiguration(string connectionString)
        {
            _serviceBusClient = new ServiceBusClient(connectionString);
        }

        public void StopTemporaryConfiguration()
        {
            _serviceBusClient = new ServiceBusClient(_connectionString);
        }
        public async Task CreateSubscriber (string topic, string name , string ruleSql )
        {
            var adminClient = new ServiceBusAdministrationClient(_connectionString);
            await adminClient.CreateSubscriptionAsync(new CreateSubscriptionOptions(topic, name)  ,  new CreateRuleOptions($"{name}_rl",  new SqlRuleFilter(ruleSql)) );
            
        } 
        public async Task  DeleteSubscriber (string topic, string name )
        {
            var adminClient = new ServiceBusAdministrationClient(_connectionString);
            await adminClient.DeleteSubscriptionAsync(topic,name);

        }
        public async Task<T> SpecificReceiver<T>(ISingleMessageConsumer consumer)
        {
            _serviceBusClient = new ServiceBusClient(_connectionString);

            var receiver = _serviceBusClient.CreateReceiver(consumer.EntityPath,  new ServiceBusReceiverOptions {  ReceiveMode=(ServiceBusReceiveMode ) consumer.ReceiveMode}); 

            ServiceBusReceivedMessage message = await receiver.ReceiveMessageAsync(consumer.Timeout);

            if (message != null)
            {
                T item;
                string reply = Encoding.UTF8.GetString(message.Body);
                if (consumer.Converter != null)
                {  item = JsonConvert.DeserializeObject<T>(reply, consumer.Converter); }
                else
                { item = JsonConvert.DeserializeObject<T>(reply); }
                
                return item;
            }
            return default!;

        }

         
    }
}
