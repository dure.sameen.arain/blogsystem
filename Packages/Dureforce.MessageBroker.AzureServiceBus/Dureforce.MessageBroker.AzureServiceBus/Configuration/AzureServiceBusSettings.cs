﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dureforce.MessageBroker.AzureServiceBus.Configuration
{
    public class AzureServiceBusSettings
    {
        public string ConnectionString { get; set; }
    }
}
